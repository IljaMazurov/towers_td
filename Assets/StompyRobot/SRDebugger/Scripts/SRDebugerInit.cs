﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SRDebugerInit : MonoBehaviour
{
    private void Awake()
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        SRDebug.Init();
#endif
    }
}
