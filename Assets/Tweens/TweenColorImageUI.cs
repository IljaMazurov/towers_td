//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2016 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using TMPro;
using UnityEngine.UI;
/// <summary>
/// Tween the object's alpha. Works with both UI widgets as well as renderers.
/// </summary>

[AddComponentMenu("NGUI/Tween/Tween Color Image UI")]
public class TweenColorImageUI : UITweener {
	bool mCached = false;

	[SerializeField] private Color  _fromColor;
	[SerializeField] private Color  _toColor;
	[SerializeField] private bool   _includeChildren = false;

	 private Image      _thisImage;
	 private Image[]    _thisImages;
	 private float      _progress;


    void Cache() {
        _thisImage = GetComponent<Image>();
        if (_includeChildren)
            _thisImages = GetComponentsInChildren<Image>();

		mCached = true;
	}

	private void SetColor (float progress) {
        if (!mCached) 
            Cache();

        _progress           = progress;
        var color           = Color.Lerp(_fromColor, _toColor, progress);
        _thisImage.color    = color;
        if (_includeChildren) {
            for (int i = 0; i < _thisImages.Length; i++) {
                _thisImages[i].color = color;
            }
        }

	}


	public float value {
		get {
			return _progress;
		}
		set {
            SetColor(value);
		}
	}

    /// <summary>
    /// Tween the value.
    /// </summary>

    protected override void OnUpdate(float factor, bool isFinished) {
        value = factor;
    }

    /// <summary>
    /// Start the tweening operation.
    /// </summary>



    static public TweenAlpha Begin (GameObject go, float duration, float alpha) {
		TweenAlpha comp = UITweener.Begin<TweenAlpha>(go, duration);
		comp.from = comp.value;
		comp.to = alpha;

		if (duration <= 0f) {
			comp.Sample(1f, true);
			comp.enabled = false;
		}

		return comp;
	}

}
