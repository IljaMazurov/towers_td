﻿Shader "Editor/EditorPreviewItem" {
    Properties {
        _Color ("Color", Color) = (1, 1, 1, 1)
    }

    SubShader {
        Tags { 
            "RenderType"	= "Transparent"
			"Queue"			= "Transparent"
        }

        Pass {
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM

            #pragma vertex      vert
            #pragma fragment    frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex   : POSITION;
                float3 normal   : NORMAL;
            };

            struct v2f {
                float4  vertex  : SV_POSITION;
                fixed	color   : COLOR;
            };

            fixed4 _Color;

            v2f vert (appdata v) {
                v2f data;

                float4 wpos     = mul(unity_ObjectToWorld, v.vertex);
                fixed3 normal	= UnityObjectToWorldNormal(v.normal);
				fixed  dotVec	= dot(_WorldSpaceLightPos0.xyz, normal);
				fixed3 view		= normalize(_WorldSpaceCameraPos.xyz - wpos);

				fixed  albedo	= (1.5 - dot(view, normal) * .75) * 1.5;

				data.vertex		= mul(UNITY_MATRIX_VP, wpos);
				data.color		= clamp(dotVec, .25, 1) * albedo;

                return data;
            }

            fixed4 frag (v2f i) : SV_Target {
                return _Color * i.color;
            }

            ENDCG
        }
    }
}
