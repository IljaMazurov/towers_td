﻿Shader "Editor/EditorPreviewText" {
    Properties {
        [NoScaleOffset]
        _MainTex        ("Texture",     2D)             = "white" {}
        _Color          ("Color",       Color)          = (1, 1, 1, 1)

        _X_Offset       ("X Offset",    Range(0,1))    = 0
        _X_Scale        ("X Scale",     Range(0,2))    = 1
        _Y_Offset       ("Y Offset",    Range(0,1))    = 0
        _Y_Scale        ("Y Scale",     Range(0,2))    = 1
    }

    SubShader {
        Tags { 
            "RenderType"	= "Transparent"
			"Queue"			= "Transparent"
        }

        Pass {
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM

            #pragma vertex      vert
            #pragma fragment    frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex   : POSITION;
                fixed2 uv       : TEXCOORD;
            };

            struct v2f {
                float4  vertex  : SV_POSITION;
                fixed2	uv      : TEXCOORD;
            };

            sampler2D   _MainTex;
            fixed4      _Color;

            fixed       _X_Offset;
            fixed       _X_Scale;
            fixed       _Y_Offset;
            fixed       _Y_Scale;

            v2f vert (appdata v) {
                v2f data;

                float4 wpos     = mul(unity_ObjectToWorld, v.vertex);
				data.vertex		= mul(UNITY_MATRIX_VP, wpos);
				data.uv		    = v.uv * fixed2(_X_Scale, _Y_Scale) + fixed2(_X_Offset, _Y_Offset);

                return data;
            }

            fixed4 frag(v2f i) : SV_Target{
                fixed4 tex = tex2D(_MainTex, i.uv);
                return tex;
            }

            ENDCG
        }
    }
}
