﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEditor;
using UnityEngine;
using Constants.Map;


[CustomEditor(typeof(BattleMapBuilder), true)]
public class Battle_MapBuilder_Inspector : Editor {
    private const string    SHADER_ITEM = "Editor/EditorPreviewItem";
    private const string    SHADER_TEXT = "Editor/EditorPreviewText";

    private const int MAP_SIZE_MIN  = 5;
    private const int MAP_SIZE_MAX  = 25;

    private Mesh        sphereMesh;
    private Mesh        cubeMesh;
    private Mesh        capsuleMesh;

    private Texture2D   texture_Text;

    private Material    material_Grey;

    private Material    material_Translucent;
    private Material    material_Orange;
    private Material    material_Yellow;
    private Material    material_Red;
    private Material    material_Green;

    private Material    material_Selection;

    private Vector3                         cellCubeOffset  = new Vector3(0, 0, 0);
    private Dictionary<CellType, Material>  text_materials  = new Dictionary<CellType, Material>();

    private Dictionary<CellType, Material>  mapViewMaerials = new Dictionary<CellType, Material>();
    private Dictionary<CellType, LocaleCfg> cellLocaleCfg   = new Dictionary<CellType, LocaleCfg> {
        { CellType.Empty,       new LocaleCfg {  x_offset = 0.015f, x_scale = 0.335f, y_scale = 0.145f, y_offset = 0.358f } },
        { CellType.Ground,      new LocaleCfg {  x_offset = 0.015f, x_scale = 0.335f, y_scale = 0.120f, y_offset = 0.870f } },
        { CellType.Road_Start,  new LocaleCfg {  x_offset = 0.015f, x_scale = 0.335f, y_scale = 0.120f, y_offset = 0.749f } },
        { CellType.Road,        new LocaleCfg {  x_offset = 0.015f, x_scale = 0.335f, y_scale = 0.120f, y_offset = 0.626f } },
        { CellType.Road_End,    new LocaleCfg {  x_offset = 0.015f, x_scale = 0.335f, y_scale = 0.130f, y_offset = 0.498f } },
    };

    private int             edit_Current_Pen = (int)CellType.Empty;


    #region Inspector
    public override void    OnInspectorGUI          () {
        serializedObject.Update();

        DrawDefaultInspector();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        Map_DrawMapConfig();
        EditorGUILayout.EndVertical();

        serializedObject.ApplyModifiedProperties();
    }

    public void             Map_DrawMapConfig       () {
        var mapProperty = serializedObject.FindProperty("mbuilder_CurrentMap");

        if (mapProperty?.objectReferenceValue == null)
            return;

        var mapObject   = new SerializedObject(mapProperty.objectReferenceValue);
        mapObject.Update();

        var mapSize = mapObject.FindProperty("_Size");

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Map Size", GUILayout.Width(100f));
        EditorGUILayout.IntSlider(mapSize, MAP_SIZE_MIN, MAP_SIZE_MAX, new GUIContent(""), GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();


        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Reset Map")) {
            ClearMapCells(mapObject);
        }

        EditorGUILayout.EndHorizontal();

        mapObject.ApplyModifiedProperties();
    }

    private void            SetNeighborIndex        (SerializedProperty prop, int index, int width, int size, int x, int y) {
        prop.InsertArrayElementAtIndex(index);
        var val = prop.GetArrayElementAtIndex(index);

        var cellIdx     = x + y * width;
        if (x < 0 || y < 0 || cellIdx < 0 || cellIdx >= size)
            cellIdx = -1;

        val.intValue = cellIdx;
    }
    private void            ClearMapCells           (SerializedObject mapObject) {
        var map_Size    = mapObject.FindProperty("_Size");
        var map_Cells   = mapObject.FindProperty("_Cells");

        map_Cells.ClearArray();

        var cell_x      = 0;
        var cell_y      = 0;

        var size = map_Size.intValue * map_Size.intValue;
        for (int i = 0; i < size; i++) {
            map_Cells.InsertArrayElementAtIndex(i);
            var cellInfo = map_Cells.GetArrayElementAtIndex(i);

            var cellPos     = new Vector3(cell_x, 0, cell_y);

            cellInfo.FindPropertyRelative("_CellType").intValue                 = (int)CellType.Ground;
            cellInfo.FindPropertyRelative("_WorldPosition").vector3Value        = cellPos;
            var cellNeighbors = cellInfo.FindPropertyRelative("_Neighbors");

            cellNeighbors.ClearArray();
            SetNeighborIndex(cellNeighbors, 0, map_Size.intValue, size, cell_x, cell_y + 1);
            SetNeighborIndex(cellNeighbors, 1, map_Size.intValue, size, cell_x + 1, cell_y);
            SetNeighborIndex(cellNeighbors, 2, map_Size.intValue, size, cell_x, cell_y - 1);
            SetNeighborIndex(cellNeighbors, 3, map_Size.intValue, size, cell_x - 1, cell_y);

            cell_x++;
            if (cell_x >= map_Size.intValue) {
                cell_x = 0; cell_y++;
            }
        }
    }
    
    #endregion


    private Material        GetTextMaterial         (CellType type, LocaleCfg cfg) {
        if (text_materials.ContainsKey(type))
            return text_materials[type];

        var shader_comp = Shader.Find(SHADER_TEXT);
        var material    = new Material(shader_comp);

        material.SetTexture("_MainTex",    texture_Text);
        material.SetFloat("_X_Offset",     cfg.x_offset);
        material.SetFloat("_X_Scale",      cfg.x_scale);
        material.SetFloat("_Y_Offset",     cfg.y_offset);
        material.SetFloat("_Y_Scale",      cfg.y_scale);

        text_materials.Add(type, material);

        return material;
    }
    private void            GetMaterial             (ref Material mat, string shader, Color color) {
        if (mat != null)
            return;

        var shader_comp     = Shader.Find(shader);
        var material        = new Material(shader_comp);
        material.SetColor("_Color", color);

        mat = material;
    }
    private void            InitBaseMaterial        () {
        if (texture_Text == null) {
            texture_Text = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/_Game/Editor/Textures/BlockType_Locales.png", typeof(Texture2D));
        }

        GetMaterial(ref material_Selection,     SHADER_ITEM, new Color(1f, .5f, .0f, .35f));

        GetMaterial(ref material_Translucent,   SHADER_ITEM, new Color(.45f, .45f, .45f, .25f));

        GetMaterial(ref material_Grey,          SHADER_ITEM, new Color(.45f, .45f, .45f, 1f));
        GetMaterial(ref material_Orange,        SHADER_ITEM, new Color(1.0f, .45f, .0f, 1f));
        GetMaterial(ref material_Yellow,        SHADER_ITEM, new Color(1.0f, 1.0f, .0f, 1f));
        GetMaterial(ref material_Red,           SHADER_ITEM, new Color(.75f, .0f, .0f, 1f));
        GetMaterial(ref material_Green,         SHADER_ITEM, new Color(.25f, .5f, .25f, 1f));


        if (mapViewMaerials.Count == 0) {
            mapViewMaerials = new Dictionary<CellType, Material> {
                { CellType.Empty,       material_Translucent        },
                { CellType.Ground,      material_Green              },
                { CellType.Road_Start,  material_Yellow             },
                { CellType.Road,        material_Orange             },
                { CellType.Road_End,    material_Red                },
            };
        }
    }
    private void            InitBaseMeshes          () {
        if (sphereMesh == null) {
            var sphere  = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphereMesh  = sphere.GetComponent<MeshFilter>().sharedMesh;
            DestroyImmediate(sphere);
        }

        if (cubeMesh == null) {
            var sphere  = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cubeMesh = sphere.GetComponent<MeshFilter>().sharedMesh;
            DestroyImmediate(sphere);
        }

        if (capsuleMesh == null) {
            var sphere  = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            capsuleMesh = sphere.GetComponent<MeshFilter>().sharedMesh;
            DestroyImmediate(sphere);
        }
    }


    private void            OnSceneGUI              () {
        serializedObject.Update();
        var mapProperty = serializedObject.FindProperty("mbuilder_CurrentMap");

        if (mapProperty?.objectReferenceValue == null)
            return;

        InitBaseMeshes  ();
        InitBaseMaterial();

        var mousePosition   = MousePointOnGround().exToInt();
        var penIndex        = DrawPenPreview    (mapProperty.objectReferenceValue as ABattleMap, mousePosition);
        var selected        = DrawCellsPreview  (mapProperty.objectReferenceValue as ABattleMap, mousePosition);

        ProcessEvents(selected, penIndex);

        SceneView.RepaintAll();
    }

    private void            ProcessEvents           (int blockID, int penID) {
        switch (Event.current.type) {
            case EventType.MouseDown:   ProcessMouseDown(blockID, penID); break;
            case EventType.MouseDrag:   ProcessMouseDown(blockID, penID); break;
            //case EventType.MouseUp:   ProcessMouseUp(Event.current);      break;
            //case EventType.MouseMove: ProcessMouseMove(Event.current);    break;
            //case EventType.Repaint:     ProcessRepaint(Event.current); break;

            default:
                break;
        }
        
    }

    private void            ProcessMouseDown        (int blockID, int penID) {
        if (Event.current.button != 0)
            return;

        StopEvents();
        if (blockID != -1) {

            var mapProperty = serializedObject.FindProperty("mbuilder_CurrentMap");

            if (mapProperty?.objectReferenceValue == null)
                return;

            var mapObject = new SerializedObject(mapProperty.objectReferenceValue);
            mapObject.Update();

            var cells       = mapObject.FindProperty("_Cells");
            var cellData    = cells.GetArrayElementAtIndex(blockID);

            Undo.RecordObject(mapProperty.objectReferenceValue, "Update Cells Params");

            cellData.FindPropertyRelative("_CellType").intValue = edit_Current_Pen;
            
            PrefabUtility.RecordPrefabInstancePropertyModifications(mapProperty.objectReferenceValue);

            mapObject.ApplyModifiedProperties();

        } else if (penID != -1) {
            edit_Current_Pen = penID;
        }
    }
    private void            StopEvents             () {
        GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
        Event.current.Use();
    }


    private int             DrawPenPreview          (ABattleMap map, Vector3Int mousePos) {
        var result      = -1;

        var typeIndex   = cellLocaleCfg.Count;
        var selectSpot  = new Vector3();

        material_Translucent.SetPass(0);
        foreach (var cfg in cellLocaleCfg) {
            var spot = new Vector3(map.map_Size + 3.5f, 0, typeIndex--);
            Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(spot + cellCubeOffset, Quaternion.identity, new Vector3(6f, 0.001f, .95f)));

            //var mDist = (spot + new Vector3(0, 0, .5f)) - mousePos;
            if (mousePos.z == spot.z && (mousePos.x - spot.x).Abs() < 3f) {
                result = (int)cfg.Key;
                Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(spot + cellCubeOffset, Quaternion.identity, new Vector3(6f, 0.001f, .95f)));
                Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(spot + cellCubeOffset, Quaternion.identity, new Vector3(6f, 0.001f, .95f)));
            }

            if ((int)cfg.Key == edit_Current_Pen) {
                selectSpot = spot;
            }
        }


        typeIndex = cellLocaleCfg.Count;
        foreach (var cfg in cellLocaleCfg) {
            var cellType        = cfg.Key;
            var shaderData      = cfg.Value;
            
            GetTextMaterial(cellType, shaderData).SetPass(0);

            var spot = new Vector3(map.map_Size + 4f, 0, typeIndex--);
            Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(spot + cellCubeOffset, Quaternion.identity, new Vector3(-5f, 0.001f, -.95f)));
        }

        typeIndex = cellLocaleCfg.Count;
        foreach (var cfg in cellLocaleCfg) {
            var cellType    = cfg.Key;
            var material    = mapViewMaerials[cellType];

            material.SetPass(0);
            var spot = new Vector3(map.map_Size + 1f, 0, typeIndex--);
            Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(spot + cellCubeOffset, Quaternion.identity, new Vector3(.85f, 0.15f, .85f)));
        }

        if (edit_Current_Pen != -1) {
            var cellType = (CellType)edit_Current_Pen;
            var material = mapViewMaerials[cellType];

            material.SetPass(0);

            Graphics.DrawMeshNow(sphereMesh, Matrix4x4.TRS(selectSpot + new Vector3(-3.5f, 0, 0), Quaternion.identity, new Vector3(.5f, .5f, .5f)));
            Graphics.DrawMeshNow(sphereMesh, Matrix4x4.TRS(selectSpot + new Vector3(+3.5f, 0, 0), Quaternion.identity, new Vector3(.5f, .5f, .5f)));
        }

        return result;
    }
    private int             DrawCellsPreview        (ABattleMap map, Vector3Int mousePos) {
        var drawMap     = new Dictionary<CellType, List<Vector3>>();

        var result      = -1;
        var selectexVec = new Vector3Int(-1, -1, -1);

        for (int i = 0; i < map.map_Cells.Count; i++) {
            var cellInfo    = map.map_Cells[i];
            var cellType    = cellInfo.cell_CellType;
            var cellPos     = cellInfo.cell_WorldPosition;

            if (!drawMap.ContainsKey(cellType))
                drawMap.Add(cellType, new List<Vector3>());


            if (mousePos == cellPos) {
                result      = i;
                selectexVec = cellPos.exToInt();
            }

            drawMap[cellType].Add(cellPos);
        }

        foreach (var typeCells in drawMap) {
            var material = material_Grey;
            if (mapViewMaerials.ContainsKey(typeCells.Key))
                material = mapViewMaerials[typeCells.Key];
            
            material.SetPass(0);
            foreach (var cell in typeCells.Value) {
                Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(cell + cellCubeOffset, Quaternion.identity, new Vector3(.95f, 0.15f, .95f)));
            }
        }

        if (result != -1) {
            material_Selection.SetPass(0);
            Graphics.DrawMeshNow(cubeMesh, Matrix4x4.TRS(selectexVec + cellCubeOffset, Quaternion.identity, new Vector3(1f, 0.25f, 1f)));
        }

        return result;
    }


    private Vector3         MousePointOnGround      () {
        var mouseCameraRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        var mousePosition = mouseCameraRay.origin;
        var ground_below = mousePosition.exZero(false, true, false);

        var ground_dist = Vector3.Distance(mouseCameraRay.origin, ground_below);

        var angl_top = Vector3.Angle(Vector3.down, mouseCameraRay.direction);
        var height_angle = 90f - angl_top;

        var hLength = ground_dist / Mathf.Sin(height_angle * Mathf.Deg2Rad);

        return (mouseCameraRay.origin + mouseCameraRay.direction * hLength).exZero(false, true, false) - cellCubeOffset;
    }

    private class LocaleCfg {
        public float x_scale;
        public float y_scale;
        public float x_offset;
        public float y_offset;
    }
}

