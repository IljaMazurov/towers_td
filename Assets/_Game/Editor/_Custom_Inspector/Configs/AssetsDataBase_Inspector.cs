﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AssetsDataBase))]
public class AssetsDataBase_Inspector : Editor {
    public override void        OnInspectorGUI              () {
        var gui_target = target as AssetsDataBase;

        serializedObject.Update();

        DrawDefaultInspector();
        if (db_Updater.items != null) {
            foreach (var item in db_Updater.items) {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                Draw_Info(item.db_Type, item.db_VarName, item.db_Path);
                EditorGUILayout.EndVertical();
            }
        }

        if (GUI.changed)
            EditorUtility.SetDirty(gui_target);

        serializedObject.ApplyModifiedProperties();
    }

    private List<string>        GetFolderTree   (string path) {
        var res     = new List<string>();
        var dirs    = Directory.GetDirectories(path);
        foreach (var dir in dirs) {
            var list = GetFolderTree(dir);
            res.AddRange(list);
        }

        res.AddRange(dirs);
        return res;
    }
    private void                PreloadFolder   (string targetFolder, System.Type type) {
        var folderPath      = Application.dataPath + "/" + targetFolder;
        var folderStruct    = GetFolderTree(folderPath);

        foreach (var folder in folderStruct) {
            var files = Directory.GetFiles(folder);
            foreach (var file in files) {
                if (!file.Contains(".asset") &&
                    !file.Contains(".prefab"))
                    continue;

                var fPath   = file.Remove(0, file.IndexOf("Assets")).Replace("\\", "/");
                var d       = AssetDatabase.LoadAssetAtPath(fPath, type);
            }
        }

        Dbg.Log($"{targetFolder} cached", Color.green);
    }


    private void                Draw_Info       (System.Type assetType, string fieldName, string assetsPath) {
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField(fieldName);

        var asset       = assetType.GetField("asset");
        var assettype   = asset.FieldType;

        var db_list = serializedObject.FindProperty(fieldName);
        if (GUILayout.Button($"Refresh [{fieldName}] List", GUILayout.ExpandWidth(true), GUILayout.Height(24f))) {
            PreloadFolder(assetsPath, assettype);
            RefreshList(db_list, assettype);
        }

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        DrawAssetsList(db_list);
        EditorGUILayout.EndVertical();

        EditorGUILayout.EndVertical();
    }
    
    private void                DrawAssetsList             (SerializedProperty configs) {
        var count = configs.arraySize;
        for (int i = 0; i < count; i++) {
            var element     = configs.GetArrayElementAtIndex(i);
            var asset       = element?.FindPropertyRelative("asset");
            var active      = element?.FindPropertyRelative("active");

            if (asset == null) {
                continue;
            }

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            EditorGUILayout.PropertyField(active, new GUIContent(""), GUILayout.Width(50));
            EditorGUILayout.LabelField(asset.objectReferenceValue.name);
            EditorGUILayout.EndHorizontal();
        }
        
    }

    private void                RefreshList                 (SerializedProperty configs, System.Type type) {
        var list = GetConfigList(type);

        List<object> isInStock = new List<object>();
        var itemsInConfig = configs.arraySize;
        for (int i = 0; i < itemsInConfig; i++) {
            var item    = configs.GetArrayElementAtIndex(i);
            var asset   = item.FindPropertyRelative("asset").objectReferenceValue;
            var active  = item.FindPropertyRelative("active").boolValue;

            if (asset != null && active)
                isInStock.Add(asset);
        }

        configs.ClearArray();
        var updateSize = list.Count;
        for (int i = 0; i < updateSize; i++) {
            var item = list[i];
            configs.InsertArrayElementAtIndex(i);
            var element = configs.GetArrayElementAtIndex(i);

            element.FindPropertyRelative("asset").objectReferenceValue = item;
            element.FindPropertyRelative("active").boolValue = isInStock.Contains(item);
        }
    }
    private List<Object>        GetConfigList               (System.Type type) {
        var assets = AssetDatabase.FindAssets($"t:{type.Name}");
        var files   = new List<Object>(assets.Length + 1);

        foreach (var asset in assets) {
            var path    = AssetDatabase.GUIDToAssetPath(asset);
            var cfg     = AssetDatabase.LoadAssetAtPath(path, type);

            files.Add(cfg);
        }

        return files;
    }

}
