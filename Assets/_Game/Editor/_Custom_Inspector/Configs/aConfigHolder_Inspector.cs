﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(aConfigsHolder))]
public class aConfigHolder_Inspector : Editor {
    public override void        OnInspectorGUI              () {
        var gui_target = target as aConfigsHolder;

        serializedObject.Update();

        var configs = serializedObject.FindProperty("configs");

        DrawDefaultInspector();
        DrawConfigInspector(configs);

        if (GUI.changed)
            EditorUtility.SetDirty(gui_target);

        serializedObject.ApplyModifiedProperties();
    }


    private void                DrawConfigInspector         (SerializedProperty configs) {
        EditorGUILayout.BeginVertical();
        DrawRefreshButton   (configs);
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        DrawConfigsList     (configs);
        EditorGUILayout.EndVertical();
    }
    private void                DrawRefreshButton           (SerializedProperty configs) {
        if (GUILayout.Button("Refresh Config List", GUILayout.ExpandWidth(true), GUILayout.Height(24f))) {
            RefreshList(configs);
        }
    }
    private void                DrawConfigsList             (SerializedProperty configs) {
        var count = configs.arraySize;
        for (int i = 0; i < count; i++) {
            var element = configs.GetArrayElementAtIndex(i);
            if (element?.objectReferenceValue?.name == null) {
                RefreshList(configs);
                break;
            }

            EditorGUILayout.HelpBox(element?.objectReferenceValue?.name, MessageType.None);
        }
        
    }

    private void                RefreshList                 (SerializedProperty configs) {
        var list = GetConfigList();
        configs.ClearArray();

        foreach (var item in list) {
            configs.InsertArrayElementAtIndex(0);
            var element = configs.GetArrayElementAtIndex(0);
            element.objectReferenceValue = item;
        }
    }
    private List<AConfigFile>   GetConfigList               () {
        var assets = AssetDatabase.FindAssets("t:AConfigFile");
        var files   = new List<AConfigFile>(assets.Length + 1);

        foreach (var asset in assets) {
            var path    = AssetDatabase.GUIDToAssetPath(asset);
            var cfg     = (AConfigFile)AssetDatabase.LoadAssetAtPath(path, typeof(AConfigFile));

            files.Add(cfg);
        }

        return files;
    }

}
