﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RenderOrderSelector))]
public class RenderOrderSelector_Inspector : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        RenderOrderSelector setter = target as RenderOrderSelector;

        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginChangeCheck();
        int newId = DrawSortingLayersPopup(setter.RenderLayer);

        if (EditorGUI.EndChangeCheck()) {
            setter.RenderLayer = newId;
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginChangeCheck();
        int order = EditorGUILayout.IntField("Sorting Order", setter.RenderOrder);

        if (EditorGUI.EndChangeCheck()) {
            setter.RenderOrder = order;
        }
        
        EditorGUILayout.EndHorizontal();

    }

    int DrawSortingLayersPopup(int layerID) {
        var layers = SortingLayer.layers;
        var names = layers.Select(l => l.name).ToArray();

        if (!SortingLayer.IsValid(layerID))  {
            layerID = layers[0].id;
        }

        var layerValue = SortingLayer.GetLayerValueFromID(layerID);
        var newLayerValue = EditorGUILayout.Popup("Sorting Layer", layerValue, names);
        return layers[newLayerValue].id;
    }

}