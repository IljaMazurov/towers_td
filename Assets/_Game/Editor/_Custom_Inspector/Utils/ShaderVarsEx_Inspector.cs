﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ShaderVarsEx))]
public class ShaderVarsEx_Inspector : Editor {
    ShaderVarsEx    gui_target;

    GUIStyle        styleRed;
    GUIStyle        styleGreen;

    public override void OnInspectorGUI() {
        styleRed                    = new GUIStyle(GUI.skin.label);
        styleRed.fontStyle          = FontStyle.Bold;
        styleRed.normal.textColor   = Color.red;

        styleGreen                  = new GUIStyle(GUI.skin.label);
        styleGreen.fontStyle        = FontStyle.Bold;
        styleGreen.normal.textColor = Color.green;

        gui_target  = target as ShaderVarsEx;
        var type    = target.GetType();
        var fields  = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

        foreach (var field in fields) {
            var attributes  = field.GetCustomAttributes(true);
            var attribute   = null as GlobalVarDescriptionAttribute;

            for (int i = 0; i < attributes.Length; i++) {
                if (attributes[i] is GlobalVarDescriptionAttribute attr) {
                    DrawFieldInfo(field, attr); break;
                }
            }

        }

        if (GUI.changed)
            EditorUtility.SetDirty(gui_target);
        serializedObject.ApplyModifiedProperties();

    }

    private void DrawFieldInfo(FieldInfo info, GlobalVarDescriptionAttribute attr) {
        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

        EditorGUILayout.BeginVertical(GUILayout.Width(20));
        var activated = (bool)info.GetValue(gui_target);
        activated = EditorGUILayout.Toggle(activated, GUILayout.Width(30), GUILayout.Height(30));

        info.SetValue(gui_target, activated);
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.TextField(attr.nameID, activated ? styleGreen : styleRed);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(attr.description);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
    }

    

}