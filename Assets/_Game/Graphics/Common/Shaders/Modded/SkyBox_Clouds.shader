﻿Shader "Custom/SkyBox/SkyBox_Clouds" {
    Properties{
        [NoScaleOffset] _SkyAtmosphereT     ("Sky Color Too",           Color)      = (1, 1, 1, 1)
        [NoScaleOffset] _SkyAtmosphereB     ("Sky Color Bot",           Color)      = (1, 1, 1, 1)
        [NoScaleOffset] _SkySeparationHeight("Sky Separation Height",   Float)      = 0
        [NoScaleOffset] _SkySeparationGaP   ("Sky Separation Gap",      Float)      = 0

        [Space]
        [NoScaleOffset] _SkyCloudsColor     ("Clouds Color",    Color)      = (1, 1, 1, 1)

        [NoScaleOffset] _CloudTex           ("Clouds Texture",  Cube)       = "white" {}

    }

    SubShader{
        Tags { 
            "Queue" = "Background"  
        }

        Pass {
            ZWrite  Off
            Cull    Off

            CGPROGRAM
            #pragma enable_d3d11_debug_symbols
            #pragma vertex      vert
            #pragma fragment    frag

            struct vertexInput {
               half4 vertex    : POSITION;
               half3 uv        : TEXCOORD0;
            };

            struct vertexOutput {
               half4 vertex    : SV_POSITION;
               half3 uv1       : TEXCOORD0;
               half3 uv2       : TEXCOORD1;
               half  sky       : COLOR;
            };

            samplerCUBE     _CloudTex;

            fixed4          _SkyAtmosphereT;
            fixed4          _SkyAtmosphereB;
            fixed4          _SkyCloudsColor;

            half            _SkySeparationHeight;
            half            _SkySeparationGaP;

            half4x4        _rotation1;
            half4x4        _rotation2;

            vertexOutput vert(vertexInput vin) {
                vertexOutput data;

                half4   skyCoord    = mul(unity_ObjectToWorld, vin.vertex);

                data.vertex         = mul(UNITY_MATRIX_VP, skyCoord);
                data.uv1            = mul(_rotation1, vin.uv);
                data.uv2            = mul(_rotation2, vin.uv);

                data.sky            = skyCoord.y;

                return data;
            }

            fixed4 frag(vertexOutput data) : COLOR {
                half    sky         = data.sky;
                half    isSkyLower  = sky < _SkySeparationHeight;
                half    isSkyInGap  = sky > (_SkySeparationHeight + _SkySeparationGaP);

                half    skyDevider  = (_SkySeparationGaP / (_SkySeparationHeight - sky));
                half    skyLerp     = smoothstep(isSkyLower, isSkyInGap, skyDevider);
                fixed4 skyAtmo      = lerp(_SkyAtmosphereT, _SkyAtmosphereB, skyLerp);



                fixed4 clouds1      =     texCUBE(_CloudTex, data.uv1);
                fixed4 clouds2      =     texCUBE(_CloudTex, data.uv2);
                //fixed4 cloudsColor  = step(clouds1, clouds2);
                fixed4 cloudsColor  = clouds1 * clouds2;

                fixed4 skyColor     = lerp(skyAtmo, _SkyCloudsColor, cloudsColor.r);

                return skyColor;
            }

            ENDCG
        }
    }
}