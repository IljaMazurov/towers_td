﻿Shader "Custom/Warped/Sprite_Warped" {

    Properties {
		[PerRendererData]
		_MainTex		("Sprite Texture",			2D)		= "white" {}

        [PerRendererData] 
		[HideInInspector]
		_Color			("Sprite color Tint",		Color)	= (1,1,1,1)

		[MaterialToggle]	PixelSnap		("Pixel snap",		Float)		= 0
		[HideInInspector]   _RendererColor	("RendererColor",	Color)		= (1,1,1,1)
		[HideInInspector]   _Flip			("Flip",			Vector)		= (1,1,1,1)
    }


	SubShader {
		Tags {
			"Queue"				= "Transparent"
			"IgnoreProjector"	= "True"
			"RenderType"		= "Transparent"
			"PreviewType"		= "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
			#pragma vertex		vert
			#pragma fragment	frag

			#pragma enable_d3d11_debug_symbols

			#pragma multi_compile_instancing
			#pragma multi_compile_local _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA

			#include "UnitySprites.cginc"



			struct INPUT {
				half4 vertex	: POSITION;
				fixed4 color    : COLOR;
				fixed2 uv		: TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct V2F {
				half4 vertex		: SV_POSITION;
				fixed4 color		: COLOR;
				half2 uv			: TEXCOORD0;

				UNITY_VERTEX_OUTPUT_STEREO
			};

			half        _ModDir;
			half        _ModPower;
			half        _ModMinDistance;
			half        _ModMaxDistance;

			V2F vert(INPUT IN) {
				V2F data;

				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

				data.vertex		= UnityFlipSprite(IN.vertex, _Flip);

				// -- warp
				half4   viewPos         = mul(UNITY_MATRIX_MV, IN.vertex);
				half    camDistance     = length(viewPos.xyz);

				half    mval            = (_ModDir * (camDistance - _ModMinDistance) / _ModMaxDistance) * _ModPower;

				half l1 = lerp(1, 0, mval);
				half l2 = lerp(0, 1, mval);

				half4x4 rotationMatrix = half4x4(
					1,  0,  0,  0,
					0,  l1,-l2, 0,
					0,  l2, l1, 0,
					0,  0,  0,  1
				);

				half4 viewRotated       = mul(rotationMatrix, viewPos);
				half4 viewFinal         = viewRotated;

				half4 rotatedPosition   = mul(viewFinal, UNITY_MATRIX_IT_MV);
				data.vertex.xyz         = rotatedPosition;
				// -- warp


				half4 wpos		= mul(unity_ObjectToWorld, data.vertex);
				data.vertex		= mul(UNITY_MATRIX_VP, wpos);

				data.color		= IN.color;
				data.uv			= IN.uv;

				#ifdef PIXELSNAP_ON
				data.vertex		= UnityPixelSnap(data.vertex);
				#endif

				return data;
			}


			fixed4 frag(V2F data) : SV_Target {
				fixed4 col		= tex2D(_MainTex, data.uv * data.color);

				return col;
			}


			ENDCG



		}

	}
}
