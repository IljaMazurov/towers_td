﻿Shader "Custom/Warped/Stardard_Warped"
{
    Properties {
        [NoScaleOffset]
        _MainTex            ("MainTexture",         2D)                 = "white" {}

        //_ModDir             ("Mod Dir",             Range(-1,1))        = 0
        //_ModPower           ("Mod Power",           Range(0, 10))       = 1
        //_ModMinDistance     ("Mod Min Distance",    Range(1,  100))     = 200
        //_ModMaxDistance     ("Mod Max Distance",    Range(10, 200))     = 200
    }

    SubShader {
        Tags { 
            "RenderType" = "Opaque" 
        }

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows vertex:vert

        /*
        struct appdata_full {
            float4 vertex    : POSITION;  // The vertex position in model space.
            float3 normal    : NORMAL;    // The vertex normal in model space.
            float4 texcoord  : TEXCOORD0; // The first UV coordinate.
            float4 texcoord1 : TEXCOORD1; // The second UV coordinate.
            float4 tangent   : TANGENT;   // The tangent vector in Model Space (used for normal mapping).
            float4 color     : COLOR;     // Per-vertex color
        };
        */
        
        struct Input {
            float2  uv_MainTex;
        };

        sampler2D   _MainTex;

        half        _ModDir;
        half        _ModPower;
        half        _ModMinDistance;
        half        _ModMaxDistance;


        void vert(inout appdata_full v) {
            half4   viewPos         = mul(UNITY_MATRIX_MV, v.vertex);
            half    camDistance     = length(viewPos.xyz);

            half    mval            = (_ModDir * (camDistance - _ModMinDistance) / _ModMaxDistance) * _ModPower;

            half l1 = lerp(1, 0, mval);
            half l2 = lerp(0, 1, mval);

            half4x4 rotationMatrix = half4x4(
                1,  0,  0,  0,
                0,  l1,-l2, 0,
                0,  l2, l1, 0,
                0,  0,  0,  1
            );

            half4 viewRotated       = mul(rotationMatrix, viewPos);
            half4 viewFinal         = viewRotated;

            half4 rotatedPosition   = mul(viewFinal, UNITY_MATRIX_IT_MV);
            v.vertex.xyz            = rotatedPosition;
        }

        void surf (Input IN, inout SurfaceOutputStandard o) {
            fixed4 c        = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo        = c.rgb * 1.25;
            o.Alpha         = c.a;
        }

        ENDCG
    }

    FallBack "Diffuse"
}
