﻿Shader "Custom/PostFX/Blur" {
    Properties {
		[PerRendererData]
        _MainTex        ("Texture",		    2D)			    = "white" {}
        _TSize          ("Offset Size",		float)		    = 1
        _AAA            ("AAA",             Range(0, .1))   = .1
    }

    SubShader {
        Pass {
            ZWrite Off
            ZTest Always
            Blend SrcAlpha OneMinusSrcAlpha

            //LOD 100
            CGPROGRAM
            #pragma vertex      vert
            #pragma fragment    frag
            #pragma enable_d3d11_debug_symbols

            #include "UnityCG.cginc"

            struct IN {
                half4 vertex	: POSITION;
				half2 uv		: TEXCOORD0;
				half2 uv1		: TEXCOORD1;
				half2 uv2		: TEXCOORD2;
            };

            struct v2f  {
				half4 vertex	: SV_POSITION;
				half2 uv		: TEXCOORD0;
				half4 color		: COLOR;
            };

			sampler2D	_MainTex;
			half4	    _MainTex_TexelSize;
			half		_TSize;
			half		_AAA;

            v2f vert (IN data) {
                v2f o;

                o.uv		= data.uv;
                o.vertex	= UnityObjectToClipPos(data.vertex);
                o.color     = half4(0, 0, 1, 0);

                fixed4 cent     = tex2Dlod(_MainTex, half4(data.uv1, 0, 0));
                fixed4 left     = tex2Dlod(_MainTex, half4(data.uv1 - half2(data.uv2.x, 0), 0, 0));
                fixed4 right    = tex2Dlod(_MainTex, half4(data.uv1 + half2(data.uv2.x, 0), 0, 0));

                fixed4 top      = tex2Dlod(_MainTex, half4(data.uv1 - half2(0, data.uv2.y), 0, 0));
                fixed4 down     = tex2Dlod(_MainTex, half4(data.uv1 + half2(0, data.uv2.y), 0, 0));

                half4 colsum    = (cent + left + right + top + down) * .2;
                half mono       = (colsum.r + colsum.g + colsum.b) * .333;
                if (mono >= _AAA) {
                    o.vertex = half4(1000, 1000, 0, 0);
                    o.color = half4(0, 0, 0, 0);
                }

                return o;
            }

            fixed4 frag (v2f data) : SV_Target {
                half4 col		= tex2D(_MainTex, data.uv);

                half4 col_r	    = tex2D(_MainTex, data.uv + half2(_MainTex_TexelSize.x * _TSize, 0));
                half4 col_l	    = tex2D(_MainTex, data.uv - half2(_MainTex_TexelSize.x * _TSize, 0));
                half4 col_t	    = tex2D(_MainTex, data.uv - half2(0, _MainTex_TexelSize.y * _TSize));
                half4 col_b	    = tex2D(_MainTex, data.uv + half2(0, _MainTex_TexelSize.y * _TSize));

                col				= (col + col_r + col_l + col_t + col_b) * .2f,
				col.a			= 1;

                return col + data.color;
            }

            ENDCG
        }
    }
}
