﻿Shader "Custom/PostFX/Vignette" {
    Properties {
        _ColorCenter    ("Color Center",		Color)		= (1, 1, 1, 1)
        _ColorBorders   ("Color Borders",		Color)		= (1, 1, 1, 1)
    }

    SubShader {
        Pass {
            CGPROGRAM
            #pragma vertex      vert
            #pragma fragment    frag

            #include "UnityCG.cginc"

            struct IN {
                half4 vertex	: POSITION;
				fixed2 uv		: TEXCOORD0;
            };

            struct v2f  {
				half4 vertex	: SV_POSITION;
				fixed2 uv		: TEXCOORD0;
            };

			fixed4		_ColorCenter;
			fixed4		_ColorBorders;

            v2f vert (IN data) {
                v2f o;
                o.vertex	= UnityObjectToClipPos(data.vertex);
                o.uv		= data.uv;
                return o;
            }

            fixed4 frag (v2f data) : SV_Target {
                fixed col_x     = sin(UNITY_PI * data.uv.x);
                fixed col_y     = sin(UNITY_PI * data.uv.y);
                fixed colLerp   = (col_x + col_y) / 2;
                fixed4 col	    = lerp(_ColorBorders, _ColorCenter, colLerp);
				col.a			= 1;

                return col;
            }

            ENDCG
        }
    }
}
