﻿Shader "Custom/PostFX/DisplayBlur"
{
    Properties {
        [PerRendererData]
        _MainTex    ("Texture",     2D)             = "white" {}
    }

    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass {
            //Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            ZTest Always
            Blend One One

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma enable_d3d11_debug_symbols

            #include "UnityCG.cginc"

            struct IN {
                half4 vertex	: POSITION;
				half4 color		: COLOR0;
				half2 uv		: TEXCOORD0;
				half2 uv1		: TEXCOORD1;
				half2 uv2		: TEXCOORD2;
            };

            struct v2f  {
				half4 vertex	: SV_POSITION;
				half2 uv		: TEXCOORD0;
				half4 color		: COLOR;
            };

			sampler2D	_MainTex;

            v2f vert (IN data) {
                v2f o;

                o.uv		= data.uv;
                o.vertex	= UnityObjectToClipPos(data.vertex);

                fixed4 cent     = tex2Dlod(_MainTex, half4(data.uv1, 0, 0));
                fixed4 left     = tex2Dlod(_MainTex, half4(data.uv1 - half2(data.uv2.x, 0), 0, 0));
                fixed4 right    = tex2Dlod(_MainTex, half4(data.uv1 + half2(data.uv2.x, 0), 0, 0));

                fixed4 top      = tex2Dlod(_MainTex, half4(data.uv1 - half2(0, data.uv2.y), 0, 0));
                fixed4 down     = tex2Dlod(_MainTex, half4(data.uv1 + half2(0, data.uv2.y), 0, 0));

                half4 colsum    = (cent + left + right + top + down) * .2;
                half mono       = (colsum.r + colsum.g + colsum.b) * .333;
                if (mono < 0.000001) {
                    o.vertex = half4(1000, 1000, 0, 0);
                }


                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                fixed4 col  = tex2D(_MainTex, i.uv);
                col *= 2; 
                return saturate(col);  
            }
            ENDCG
        }
    }
}
