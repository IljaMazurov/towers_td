﻿Shader "Custom/Enemy/Battle_HealthBarShader" {
	Properties {
		_ColorFilled	("Inner Color Filled",	Color)		= (1, 1, 1, 1)
		_ColorEmpty		("Inner Color Empty",	Color)		= (1, 1, 1, 1)
		_ColorBorders	("Border Color",		Color)		= (1, 1, 1, 1)

		_BillboardScale	("Scale",			Range(0, 1))= 1
	}

	SubShader{
		Tags {
			"LightMode"		= "ForwardBase"
			//"Queue"			= "Transparent"
			//"RenderType"	= "Transparent"
		}

		Pass {
			Cull Off
			ZWrite On

			CGPROGRAM
			#pragma	vertex		vert
			#pragma	fragment	frag
			#pragma multi_compile_instancing
			#pragma enable_d3d11_debug_symbols

			#include "UnityCG.cginc"
			#include "Assets/_cg/Customs.cginc"


			struct INPUT {
				half4	vertex			: POSITION;
				fixed4	color			: COLOR0;
				fixed2	uv				: TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				half4	pos 			: SV_POSITION;
				fixed4	color			: COLOR0;
				fixed3	uv				: TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			fixed4 _ColorBorders;
			fixed4 _ColorFilled;
			fixed4 _ColorEmpty;

			fixed _BillboardScale;

			v2f vert(INPUT v) {
				v2f data;

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(data);

				float4 mData;
				float4x4 matIn	= ParseMatrix(UNITY_MATRIX_MV, mData);

				float4 position = mul(matIn, float4(0, .5, 0, 1));
				float4 view		= mul(UNITY_MATRIX_P, position + float4(v.vertex.x * _BillboardScale, v.vertex.y * _BillboardScale * 0.75f, 0, 0));

				data.pos		= view;
				data.color		= v.color;
				data.uv			= fixed3(v.uv, mData.x);

				return data;
			}

			fixed4 frag(v2f data) : SV_Target {
				UNITY_SETUP_INSTANCE_ID(data);

				fixed	isBorder	= data.color.r > 0;
				fixed	isFilled	= data.uv.x < data.uv.z;

				fixed4	fillerColor	= lerp(_ColorEmpty, _ColorFilled, isFilled);
				fixed4	finalColor	= lerp(_ColorBorders, fillerColor, isBorder);

				return finalColor;
			}

			ENDCG
		}
	}
}
