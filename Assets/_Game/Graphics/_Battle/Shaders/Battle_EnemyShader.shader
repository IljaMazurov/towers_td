﻿Shader "Custom/Enemy/Battle_EnemyShader" {
	Properties{
		_CustomColor		("Color",		Color) = (1, 1, 1, 1)
		_CustomInnerColor	("Inner Color",	Color) = (1, 1, 1, 1)

		_Contrast			("Contrast",	Range(0.8, 2)) = 1
		_Frensel			("Frensel",		Range(0.5, 3)) = 1.5
		_FrenselInt			("FrenselInt",	Range(0.5, 2)) = 1.5

		_MinLight			("MinLight",	Range(0.01, 0.5)) = 0.25
		_MaxLight			("MaxLight",	Range(0.5, 1)) = 1
	}

		SubShader{
			Tags {
				"LightMode"		= "ForwardBase"
				"Queue"			= "Transparent"
				"RenderType"	= "Bloom"
			}

			Pass {
				Blend SrcAlpha OneMinusSrcAlpha

				CGPROGRAM
				#pragma	vertex		vert
				#pragma	fragment	frag
				#pragma multi_compile_instancing
				#pragma enable_d3d11_debug_symbols

				#include "UnityCG.cginc"


				struct INPUT {
					half4	vertex			: POSITION;

					fixed2	uv				: TEXCOORD0;
					fixed3	normal			: NORMAL;
					fixed4	color			: COLOR;

					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f {
					half4	pos 			: SV_POSITION;
					fixed2	color			: COLOR;

					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				fixed4		_CustomColor;
				fixed4		_CustomInnerColor;

				half		_Contrast;
				half		_Frensel;
				half		_FrenselInt;

				fixed		_MinLight;
				fixed		_MaxLight;

				half		_FadeStart;
				half		_FadeSize;

				v2f vert(INPUT v) {
					v2f data;

					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(data);

					half4 wpos		= mul(unity_ObjectToWorld, v.vertex);
					fixed3 normal	= UnityObjectToWorldNormal(v.normal);
					fixed  dotVec	= dot(_WorldSpaceLightPos0.xyz, normal);
					fixed3 view		= normalize(_WorldSpaceCameraPos.xyz - wpos);

					fixed  albedo	= (1.2 - dot(view, normal) * _FrenselInt) * _Frensel;
					data.color.r	= clamp(dotVec * _Contrast * albedo, _MinLight, _MaxLight);
					data.color.g	= v.color.r;

					data.pos		= mul(UNITY_MATRIX_VP, wpos);

					return data;
				}

				fixed4 frag(v2f data) : SV_Target {
					UNITY_SETUP_INSTANCE_ID(data);

					fixed4 col		= lerp(_CustomInnerColor, _CustomColor, data.color.g) * data.color.r;
					col.a			= 1;

					return col;
				}

				ENDCG
			}

		// shadow casting support
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}
