﻿Shader "Custom/Map/Battle_MapTowerDissolveShader " {
	Properties{
		[NoScaleOffset]
		_DissolveTex		("Dissolve Texture",	2D)					= "white" {}
		_DissolveEdge		("Dissolve Edge Color",	Color)				= (1, 1, 1, 1)
		_DissolveEdgePow	("Dissolve EdgePower",	Range(0, 1))		= 0.5
		[Space]
		[Space]

		_CustomColor		("Color",				Color)				= (1, 1, 1, 1)
		_CustomInnerColor	("Inner Color",			Color)				= (1, 1, 1, 1)

		_Contrast			("Contrast",			Range(0.8, 2))		= 1
		_Frensel			("Frensel",				Range(0.5, 3))		= 1.5
		_FrenselInt			("FrenselInt",			Range(0.5, 2))		= 1.5

		_MinLight			("MinLight",			Range(0.01, 0.5))	= 0.25
		_MaxLight			("MaxLight",			Range(0.5, 1))		= 1
	}

	SubShader{
		Tags {
			"LightMode"		= "ForwardBase"
		}

		Pass {
			ZWrite On
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma	vertex		vert
			#pragma	fragment	frag
			#pragma multi_compile_instancing
			#pragma enable_d3d11_debug_symbols

			#include "UnityCG.cginc"
			#include "Assets/_cg/Customs.cginc"


			struct INPUT {
				half4	vertex			: POSITION;

				fixed2	uv				: TEXCOORD0;
				fixed3	normal			: NORMAL;
				fixed4	color			: COLOR;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				half4	pos 			: SV_POSITION;

				fixed2	uv				: TEXCOORD0;
				fixed4	color			: COLOR;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

				
			sampler2D	_DissolveTex;
			fixed4		_DissolveEdge;
			fixed		_DissolveEdgePow;

			fixed4		_CustomColor;
			fixed4		_CustomInnerColor;

			half		_Contrast;
			half		_Frensel;
			half		_FrenselInt;

			fixed		_MinLight;
			fixed		_MaxLight;

			v2f vert(INPUT v) {
				v2f data;

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(data);

				float4 mData;
				float4x4 matIn	= ParseMatrix(unity_ObjectToWorld, mData);
				fixed dissolve	= 1 - mData.x;

				half4  wpos		= mul(matIn, v.vertex);
				fixed3 normal	= UnityObjectToWorldNormal(v.normal);
				fixed  dotVec	= dot(_WorldSpaceLightPos0.xyz, normal);
				fixed3 view		= normalize(_WorldSpaceCameraPos.xyz - wpos);

				fixed  albedo	= (1.2 - dot(view, normal) * _FrenselInt) * _Frensel;
				fixed  lightVal	= clamp(dotVec * _Contrast * albedo, _MinLight, _MaxLight);

				data.color.rgb	= lerp(_CustomInnerColor, _CustomColor * lightVal, v.color.r);
				data.color.a	= dissolve;

				data.uv			= float2(wpos.x * v.vertex.z * .05, wpos.z * v.vertex.x * .05);
				data.pos		= mul(UNITY_MATRIX_VP, wpos);

				return data;
			}

			fixed4 frag(v2f data) : SV_Target {
				UNITY_SETUP_INSTANCE_ID(data);
				fixed dissolve		= data.color.a;

				fixed4 mask			= tex2D(_DissolveTex,	data.uv);
				half dissolveVal	= mask.r - dissolve;
				half dissolveMul	= lerp(0, _DissolveEdgePow, dissolve);

				fixed4 col		= data.color;
				col.a			= 1;
				col				= lerp(col, _DissolveEdge, dissolveVal < dissolveMul);

				clip(dissolveVal);

				return col;
			}

			ENDCG
		}
	}
}
