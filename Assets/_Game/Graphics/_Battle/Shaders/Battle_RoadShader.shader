﻿Shader "Custom/Map/Battle_RoadShader" {
	Properties{
		_CustomColor	("Color",		Color) = (1, 1, 1, 1)

		_Contrast		("Contrast",	Range(0.8, 2)) = 1
		_Frensel		("Frensel",		Range(0.5, 3)) = 1.5
		_FrenselInt		("FrenselInt",	Range(0.5, 2)) = 1.5

		_MinLight		("MinLight",	Range(0.01, 0.5)) = 0.25
		_MaxLight		("MaxLight",	Range(0.5, 1)) = 1

		_FadeStart		("Fade Start",	Range(-10, 0)) = 1
		_FadeSize		("Fade Size",	Range(-10, 0)) = 1
	}

	SubShader{
		Tags {
			"LightMode"		= "ForwardBase"
			"Queue"			= "Transparent"
			"RenderType"	= "Transparent"
		}

		Pass {
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma	vertex		vert
			#pragma	fragment	frag
			#pragma multi_compile_instancing
			#pragma enable_d3d11_debug_symbols

			#include "UnityCG.cginc"


			struct INPUT {
				half4	vertex			: POSITION;

				fixed2	uv				: TEXCOORD0;
				fixed3	normal			: NORMAL0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				half4	pos 			: SV_POSITION;
				fixed4	color			: COLOR0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			fixed4		_CustomColor;
			half		_Contrast;
			half		_Frensel;
			half		_FrenselInt;

			fixed		_MinLight;
			fixed		_MaxLight;

			half		_FadeStart;
			half		_FadeSize;


			half4		_cameraDirection_inv;
			v2f vert(INPUT v) {
				v2f data;

				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(data);

				half4 wpos		= mul(unity_ObjectToWorld, v.vertex);
				fixed3 normal	= UnityObjectToWorldNormal(v.normal);
				fixed  dotVec	= dot(_WorldSpaceLightPos0.xyz, normal);

				fixed  albedo	= (1.2 - dot(_cameraDirection_inv, normal) * _FrenselInt) * _Frensel;
				fixed  lightVal	= clamp(dotVec * _Contrast * albedo, _MinLight, _MaxLight);
				fixed  alpha	= saturate(1 - (wpos.y - _FadeStart) / _FadeSize);

				data.color		= _CustomColor * lightVal;
				data.color.a	= alpha;

				data.pos		= mul(UNITY_MATRIX_VP, wpos);

				return data;
			}

			fixed4 frag(v2f data) : SV_Target {
				UNITY_SETUP_INSTANCE_ID(data);
				return data.color;
			}

			ENDCG
		}
	}
}
