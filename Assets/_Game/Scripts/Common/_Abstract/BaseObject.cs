﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseObject : MonoBehaviour {

    [HideInInspector]
    public              Transform           obj_transform;

    public              Quaternion          obj_rotation_q { 
        get {
            return obj_transform.rotation;
        }
        set {
            obj_transform.rotation = value;
        }
    }
    public              Vector3             obj_rotation_e { 
        get {
            return obj_transform.eulerAngles;
        }
        set {
            obj_transform.eulerAngles = value;
        }
    }


    public              Quaternion          obj_local_rotation_q { 
        get {
            return obj_transform.localRotation;
        }
        set {
            obj_transform.localRotation = value;
        }
    }
    public              Vector3             obj_local_rotation_e { 
        get {
            return obj_transform.localEulerAngles;
        }
        set {
            obj_transform.localEulerAngles = value;
        }
    }

    
    public              Vector3             obj_forward {
        get {
            return obj_transform.forward;
        }
    }
    public              Vector3             obj_position {
        get {
            return obj_transform.position;
        }
        set {
            obj_transform.position = value;
        }
    }
    public              Vector3             obj_local_position {
        get {
            return obj_transform.localPosition;
        }
        set {
            obj_transform.localPosition = value;
        }
    }
    public              Vector3             obj_scale {
        get {
            return obj_transform.localScale;
        }
        set {
            obj_transform.localScale = value;
        }
    }
    public              float               obj_scale_f {
        set {
            obj_transform.localScale = new Vector3(value, value, value);
        }
    }


    private             void                Awake                       () {
        obj_mOnAwake();
    }

    protected virtual   void                obj_mOnAwake                () {
        obj_transform = transform;
    }

}
