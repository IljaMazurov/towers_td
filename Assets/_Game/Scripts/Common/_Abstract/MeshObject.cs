﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshObject : BaseObject {
    protected       List<Renderer>          obj_renderers;
    protected       List<MeshFilter>        obj_meshFilters;
    private         MaterialPropertyBlock   propBlock;

    private         Vector3                 mesh_centerOffset;
    private         Vector3                 mesh_topOffset;
    private         Vector3                 mesh_bottomOffset;
    private         Vector3                 mesh_size;

    public          Vector3                 obj_position_center {
        get {
            return obj_transform.position + mesh_centerOffset;
        }
    }
    public          Vector3                 obj_position_top {
        get {
            return obj_transform.position + mesh_topOffset;
        }
    }
    public          Vector3                 obj_position_bottom {
        get {
            return obj_transform.position + mesh_bottomOffset;
        }
    }
    public          Vector3                 obj_size {
        get {
            return mesh_size;
        }
    }


    protected override  void                obj_mOnAwake                () {
        base.obj_mOnAwake();
        InitBasics();
    }


    private             void                InitBasics                  () {
        obj_transform       = transform;
        obj_renderers       = new List<Renderer>();
        obj_meshFilters     = new List<MeshFilter>();

        // GetComponentsInChildren ignores disabled objects
        foreach (var child in obj_transform) {
            var childTransform  = child as Transform;
            var render          = childTransform.GetComponent<Renderer>();
            var filter          = childTransform.GetComponent<MeshFilter>();

            if (render != null && filter != null) {
                obj_renderers.  Add(render);
                obj_meshFilters.Add(filter);
            }
        }

        if (obj_renderers.Count > 0) {
            var bounds          = obj_renderers[0].bounds;

            foreach (var childRender in obj_renderers) {
                bounds.Encapsulate(childRender.bounds);
            }

            mesh_centerOffset   = (bounds.center - obj_position).exZero(true, false, true);
            mesh_topOffset      = mesh_centerOffset;
            mesh_bottomOffset   = mesh_centerOffset;
            mesh_topOffset.y    = bounds.max.y;
            mesh_bottomOffset.y = bounds.min.y;
            mesh_size           = bounds.size;
        }
        
    }

    private             void                CheckPropBlock              () {
        if (propBlock == null)
            propBlock = new MaterialPropertyBlock();
    }


    private             void                SetRendererPropertyBlock    () {
        for (int r = 0; r < obj_renderers.Count; r++) {
            if (obj_renderers[r] is MeshRenderer || obj_renderers[r] is SkinnedMeshRenderer) {
                var matCount = obj_renderers[r].sharedMaterials.Length;
                for (int m = 0; m < matCount; m++){
                    obj_renderers[r].SetPropertyBlock(propBlock, m);
                }
            }
        }
    }
    protected virtual   void                obj_mRendererSetFloat       (int nameID, float value) {
        CheckPropBlock();

        propBlock.SetFloat(nameID, value);
        SetRendererPropertyBlock();
    }
    protected virtual   void                obj_mRendererSetVec3        (int nameID, Vector3 value) {
        CheckPropBlock();

        propBlock.SetVector(nameID, new Vector4(value.x, value.y, value.z));
        SetRendererPropertyBlock();
    }
    protected virtual   void                obj_mRendererSetColor       (int nameID, Color value) {
        CheckPropBlock();

        propBlock.SetColor(nameID, value);
        SetRendererPropertyBlock();
    }

    protected virtual   void                obj_mChangeMaterial         (Material newMaterial) {
        CheckPropBlock();

        propBlock.Clear();
        for (int r = 0; r < obj_renderers.Count; r++) {
            if (obj_renderers[r] is MeshRenderer || obj_renderers[r] is SkinnedMeshRenderer) {

                if (obj_renderers[r] == null) 
                    continue;

                var materials   = obj_renderers[r].sharedMaterials;
                for (int i = 0; i < materials.Length; i++) {
                    obj_renderers[r].GetPropertyBlock(propBlock, i);
                    materials[i]   = newMaterial;
                    obj_renderers[r].SetPropertyBlock (propBlock, i);
                }

                obj_renderers[r].sharedMaterials = materials;
            }
        }
    }

    public    virtual   Mesh                obj_GetMesh                 (int rendererIndex = 0) {
        var index       = rendererIndex;
        var maxIndex    = obj_meshFilters.Count - 1;
        if (index > maxIndex)
            index = maxIndex;

        if (index >= 0) {
            return obj_meshFilters[index].mesh;
        }

        index       = rendererIndex;
        maxIndex    = obj_renderers.Count - 1;
        if (index > maxIndex)
            index = maxIndex;

        if (obj_renderers[index] is SkinnedMeshRenderer skin) {
            Mesh result = new Mesh();
            skin.BakeMesh(result);
            return result;
        }

        return null;
    }
    protected virtual   bool                obj_SetMesh                 (Mesh mesh, int rendererIndex = 0) {
        var maxIndex = obj_meshFilters.Count - 1;

        if (rendererIndex <= maxIndex) {
            obj_meshFilters[rendererIndex].mesh = mesh;
            return true;
        }

        return false;
    }
    public    virtual   Mesh                obj_GetSharedMesh           (int rendererIndex = 0) {
        var maxIndex = obj_meshFilters.Count - 1;
        if (rendererIndex > maxIndex) 
            rendererIndex = maxIndex;

        if (rendererIndex >= 0) {
            return obj_meshFilters[rendererIndex].mesh;
        }

        maxIndex = obj_renderers.Count - 1;
        if (rendererIndex > maxIndex)
            rendererIndex = maxIndex;

        if (obj_renderers[maxIndex] is SkinnedMeshRenderer skin) {
            return skin.sharedMesh;
        }

        return null;
    }
    protected virtual   bool                obj_SetSharedMesh           (Mesh mesh, int rendererIndex = 0) {
        var maxIndex = obj_meshFilters.Count - 1;

        if (rendererIndex <= maxIndex) {
            obj_meshFilters[rendererIndex].sharedMesh = mesh;
            return true;
        }

        return false;
    }

}
