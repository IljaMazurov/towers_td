﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASingletonMono<T> : MonoBehaviour where T : MonoBehaviour, new() {
    private static T _instance;
    public  static T Get        () {
        if (_instance == null) {
            _instance = GameObject.FindObjectOfType<T>();
        }

        if (_instance == null) {
            var newInstance = new GameObject();
            _instance       = newInstance.AddComponent<T>();
            _instance.name  = typeof(T).Name;
        }

        return _instance;
    }

    public  static A Get<A>     () where A : T {
        if (_instance == null) {
            _instance = GameObject.FindObjectOfType<A>();
        }

        if (_instance == null) {
            var newInstance = new GameObject();
            _instance       = newInstance.AddComponent<A>();
            _instance.name  = typeof(A).Name;
        }

        return _instance as A;
    }

    protected ASingletonMono() { mngr_Reset(); }

    public virtual void mngr_Reset  () { }
    public virtual void mngr_Clear  () { }
}
