﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class ASingletonMonoConcrete<T> : MonoBehaviour where T : MonoBehaviour, new() {
    private static T _instance;
    public  static T Get        () {
        if (_instance == null) {
            _instance = GameObject.FindObjectOfType<T>();
        }

        if (_instance == null) {
            var name        = typeof(T).Name;
            var gObject     = new GameObject(name);
            _instance       = gObject.AddComponent<T>();

            DontDestroyOnLoad(gObject);
        }

        return _instance;
    }

    public  static A Get<A>     () where A : T {
        if (_instance == null) {
            _instance = GameObject.FindObjectOfType<A>();
        }

        if (_instance == null) {
            var newInstance = GameObject.Instantiate(new GameObject());
            _instance       = newInstance.AddComponent<A>();
        }

        return _instance as A;
    }

}
