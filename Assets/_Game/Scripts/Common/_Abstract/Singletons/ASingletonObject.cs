﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASingletonObject<T> : ScriptableObject where T : ScriptableObject {
    private static T _instance;
    public  static C Get<C>() where C : T {
        var TArray1 = Resources.FindObjectsOfTypeAll<C>();
        if (TArray1.Length != 0) {
            _instance = TArray1[0];
        }

        if (_instance == null) {
            throw new MissingReferenceException($"Cant find instance of {nameof(T)}");
        }

        return _instance as C;
    }

}
