﻿using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;

[System.Serializable]
public class AssetObject : AssetReferenceT<GameObject> {
    public AssetObject(string guid) : base(guid) { }

    public Task<GameObject> Get() {
        if (OperationHandle.IsValid())
            return OperationHandle.Convert<GameObject>().Task;
        else
            return LoadAssetAsync().Task;
    }


}
