﻿using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;

[System.Serializable]
public class AssetSprite : AssetReferenceT<Sprite> {
    public AssetSprite(string guid) : base(guid) { }

    public Task<Sprite> Get() {
        if (OperationHandle.IsValid())
            return OperationHandle.Convert<Sprite>().Task;
        else
            return LoadAssetAsync().Task;
    }
}
