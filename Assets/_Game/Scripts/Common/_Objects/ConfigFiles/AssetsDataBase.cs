﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Config/AssetsDataBase")]
public class AssetsDataBase : AConfigFile {
    [SerializeField, HideInInspector] private List<db_BulletAsset> Bullets = new List<db_BulletAsset>();

    public List<BulletData> mGetActiveBullets     () {
        var list = new List<BulletData>();

        foreach (var bullet in Bullets) {
            if (bullet.active)
                list.Add(bullet.asset);
        }

        return list;
    }

    public BulletData       mGetBulletByType      (Constants.Bullets.BType type) {
        var result = Bullets.FirstOrDefault(x => x.asset.bullet_Type == type);

        if (result == null)
            throw new System.MissingMemberException($"There is no bullet with type [{type}] in DataBase");

        return result.asset;
    }

}

#if UNITY_EDITOR
public static class db_Updater {
    public static List<AssetInfo> items = new List<AssetInfo> {
        new AssetInfo { db_Type = typeof (db_BulletAsset), db_VarName = "Bullets", db_Path = "_GameData/Bullets" }

    };
}
public class AssetInfo {
    public System.Type  db_Type;
    public string       db_VarName;
    public string       db_Path;
}
#endif