﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "GameData/Config/BattleConfig")]
public class BattleConfig : AConfigFile {
    [SerializeField] private List<EnemyDestroyVFX_Set> enemyDestroyVFX = new List<EnemyDestroyVFX_Set>();

    public GameObject mGetEnemyDestroyVFX(Constants.Enemies.EType enemyType) {
        var result = enemyDestroyVFX.FirstOrDefault(x => x.enemy == enemyType)?.prefab;
        if (result == null)
            throw new System.Exception($"Can't find destroy VFX for [{enemyType}]");

        return result;
    }

}

