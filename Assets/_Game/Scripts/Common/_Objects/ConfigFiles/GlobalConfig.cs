﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "GameData/Config/GlobalConfig")]
public class GlobalConfig : AConfigFile {
    [SerializeField] private float test_value;
}
