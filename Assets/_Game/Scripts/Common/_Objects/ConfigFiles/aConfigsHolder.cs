﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aConfigsHolder : MonoBehaviour {
    [HideInInspector]
    [SerializeField]
    public List<ScriptableObject> configs;

}
