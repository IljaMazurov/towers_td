﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorExtensions  {
    public static Color Alpha       (this Color col, float a) {
        col.a = a;
        return col;
    }
    public static Color Red         (this Color col, float r) {
        col.r = r;
        return col;
    }
    public static Color Green       (this Color col, float g) {
        col.g = g;
        return col;
    }
    public static Color Blue        (this Color col, float b) {
        col.b = b;
        return col;
    }
	
}
