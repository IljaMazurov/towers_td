﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DictionaryExtensions {
    public static KeyValuePair<K,V>     exFirstPair <K, V>      (this Dictionary<K, V> dictionary) {
        if (dictionary.Count == 0)
            return default(KeyValuePair<K, V>);

        var ie = dictionary.GetEnumerator();
        ie.MoveNext();


        var result = ie.Current;
        ie.Dispose();

        return result;
    }
    public static K                     exFirstKey  <K,V>       (this Dictionary<K, V> dictionary) {
        var pair = dictionary.exFirstPair();
        return pair.Key;
    }
    public static V                     exFirstValue<K, V>      (this Dictionary<K, V> dictionary) {
        var pair = dictionary.exFirstPair();
        return pair.Value;
    }

    public static KeyValuePair<K,V>     exLastPair <K, V>       (this Dictionary<K, V> dictionary) {
        if (dictionary.Count == 0)
            return default(KeyValuePair<K, V>);

        var ie      = dictionary.GetEnumerator();
        var last    = ie.Current;
        while (ie.MoveNext())
            last    = ie.Current;

        ie.Dispose();

        return last;
    }
    public static K                     exLastKey  <K,V>        (this Dictionary<K, V> dictionary) {
        var pair = dictionary.exLastPair();
        return pair.Key;
    }
    public static V                     exLastValue<K, V>       (this Dictionary<K, V> dictionary) {
        var pair = dictionary.exLastPair();
        return pair.Value;
    }


    public static KeyValuePair<K, V>    exRandomItem<K, V>      (this Dictionary<K, V> dictionary) {
        if (dictionary.Count == 0)
            return default(KeyValuePair<K, V>);

        int index   = Random.Range(0, dictionary.Count);
        var ie      = dictionary.GetEnumerator();

        ie.MoveNext();
        for (int i = 0; i < index && ie.MoveNext(); i++);
        var result = ie.Current;
        ie.Dispose();

        return result;
    }
    public static V                     exRandomValue<K, V>     (this Dictionary<K, V> dictionary) {
        if (dictionary.Count == 0)
            return default(V);

        int index   = Random.Range(0, dictionary.Count);
        var ie      = dictionary.GetEnumerator();

        ie.MoveNext();
        for (int i = 0; i < index && ie.MoveNext(); i++);
        var result = ie.Current.Value;
        ie.Dispose();
        return result;
    }
    public static K                     exRandomKey<K, V>       (this Dictionary<K, V> dictionary) {
        if (dictionary.Count == 0)
            return default(K);

        int index   = Random.Range(0, dictionary.Count);
        var ie      = dictionary.GetEnumerator();

        ie.MoveNext();
        for (int i = 0; i < index && ie.MoveNext(); i++);
        var result = ie.Current.Key;
        ie.Dispose();
        return result;
    }
}
