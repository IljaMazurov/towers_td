﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public static class FloatExtensions {

    public static int   Sign    (this float val) {
        if (val == 0)
            return 0;

        return val > 0 ? 1 : -1;
    }

    public static float Abs     (this float val) {
        return Mathf.Abs(val);
    }

}
