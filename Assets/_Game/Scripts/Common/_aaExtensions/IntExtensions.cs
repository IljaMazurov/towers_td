﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public static class IntExtensions {

    public static int Sign  (this int val) {
        if (val == 0)
            return 0;

        return val > 0 ? 1 : -1;
    }

    public static int Abs     (this int val) {
        return (int)Mathf.Abs(val);
    }
}
