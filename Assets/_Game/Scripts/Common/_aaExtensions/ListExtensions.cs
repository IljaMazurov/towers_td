﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions {
    public static T exRandomItem<T>(this List<T> list) {
        if (list.Count == 0)
            return default(T);

        return list[Random.Range(0, list.Count)];
    }
    public static int   exRandomIndex<T>(this List<T> list) {
        if (list.Count == 0)
            return 0;

        return Random.Range(0, list.Count);
    }

    public static T     exRandomItemAndRemove<T>  (this List<T> list) {
        if (list.Count == 0)
            return default(T);
        var item = list[Random.Range(0, list.Count)];
        list.Remove(item);

        return item;
    }

    public static T exLast<T>(this List<T> list) {
        if (list.Count == 0)
            return default(T);

        return list[list.Count - 1];
    }
    public static T exLastAndRemove<T>(this List<T> list) {
        if (list.Count == 0)
            return default(T);

        var item = list[list.Count - 1];
        list.Remove(item);

        return item;
    }
    public static T exFirst<T>(this List<T> list) {
        if (list.Count == 0)
            return default(T);

        return list[0];
    }
    public static T exFirstAndRemove<T>(this List<T> list) {
        if (list.Count == 0)
            return default(T);

        var item = list[0];
        list.Remove(item);

        return item;
    }

}
