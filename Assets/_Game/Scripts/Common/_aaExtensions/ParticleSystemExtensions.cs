﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ParticleSystemExtensions {
    public static bool exFullResetPlay  (this ParticleSystem mono, bool withChildren = true) {
        mono.Stop   (withChildren);
        mono.Clear  (withChildren);
        mono.Play   (withChildren);


        return false;
    }

    public static bool exFullReset      (this ParticleSystem mono, bool withChildren = true) {
        mono.Stop   (withChildren);
        mono.Clear  (withChildren);


        return false;
    }

}
