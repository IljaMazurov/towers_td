﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpriteExtensions {

    public static  Vector4 getUvOffset(this Sprite spr) {
        var texture = spr.texture;
        var texSize = new Vector2(texture.width, texture.height);

        var spr_t_rect = spr.textureRect;

        var sprSize = new Vector2(spr_t_rect.width, spr_t_rect.height);
        var sprOffset = new Vector2(spr_t_rect.x, spr_t_rect.y);

        return new Vector4(
                sprSize.x / texSize.x, sprSize.y / texSize.y,
                sprOffset.x / texSize.x, sprOffset.y / texSize.y
            );
    }
}
