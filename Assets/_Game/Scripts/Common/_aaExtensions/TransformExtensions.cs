﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {
    public static void DestroyChildren      (this Transform transform) {
        if (transform.childCount == 0)
            return;

        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    public static void DestroyChildrenNow  (this Transform transform) {
        if (transform.childCount == 0)
            return;

        foreach (Transform child in transform) {
            GameObject.DestroyImmediate(child.gameObject);
        }
    }

}
