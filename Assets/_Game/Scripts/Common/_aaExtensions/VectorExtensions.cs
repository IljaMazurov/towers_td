﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Random = UnityEngine.Random;



public static class VectorExtensions {

    public static Vector3   GetBeizerPoint(Vector3 p1, Vector3 p2, Vector3 p3, float t) {
        var spot1      = Vector3.Lerp(p1, p2, t);
        var spot2      = Vector3.Lerp(p2, p3, t);
        return           Vector3.Lerp(spot1, spot2, t);
    }

    public static float3    GetBeizerPoint(float3 p1, float3 p2, float3 p3, float t) {
        var spot1      = math.lerp(p1, p2, t);
        var spot2      = math.lerp(p2, p3, t);
        return           math.lerp(spot1, spot2, t);
    }

    public static Vector3       exToV3                  (this Vector2 v2) {
        return new Vector3(v2.x, 0, v2.y);
    }
    public static Vector2Int    exToInt                 (this Vector2 v2) {
        return new Vector2Int(Mathf.RoundToInt(v2.x), Mathf.RoundToInt(v2.y));
    }

    public static Vector3       exToV3                  (this Vector2Int v2) {
        return new Vector3(v2.x, 0, v2.y);
    }
    public static Vector2       exToV2xy                (this Vector3 v3) {
        return new Vector2(v3.x, v3.y);
    }
    public static Vector2       exToV2xz                (this Vector3 v3) {
        return new Vector2(v3.x, v3.z);
    }

    public static Vector3       exAbs                   (this Vector3 vec) {
        vec.x = vec.x < 0 ? -vec.x : vec.x;
        vec.y = vec.y < 0 ? -vec.y : vec.y;
        vec.z = vec.z < 0 ? -vec.z : vec.z;

        return vec;
    }

    public static Vector3       exAdd                   (this Vector3 vec, float x, float y, float z) {
        vec.x += x;
        vec.y += y;
        vec.z += z;

        return vec;
    }

    public static Vector2Int    exAdd                (this Vector2Int vec, int x, int y) {
        vec.x += x;
        vec.y += y;

        return vec;
    }

    public static bool      exIsZero              (this Vector3 vec) {
        return vec.x == 0 && vec.y == 0 && vec.z == 0;
    }

    public static Vector3Int exToInt         (this Vector3 vec) {
        var v3int = new Vector3Int(
                Mathf.RoundToInt(vec.x),
                Mathf.RoundToInt(vec.y),
                Mathf.RoundToInt(vec.z)
            );

        return v3int;
    }
    public static Vector2Int exToV2               (this Vector3Int vec) {
        var v3int = new Vector2Int(
                Mathf.RoundToInt(vec.x),
                Mathf.RoundToInt(vec.z)
            );

        return v3int;
    }

    public static Vector3 exDiv                   (this Vector3 vec, Vector3 sec) {
        vec.x /= sec.x;
        vec.y /= sec.y;
        vec.z /= sec.z;

        return vec;
    }

    public static Vector2 exDiv                   (this Vector2 vec, Vector2 sec) {
        vec.x /= sec.x;
        vec.y /= sec.y;

        return vec;
    }

    public static Vector3 RndDirection          (float angle = 90f, int variations = 4, float startAngleOffset = 45f) {
        var rotation = startAngleOffset + Random.Range(1, variations) * angle;
        return new Vector3( Mathf.Sin(Mathf.Deg2Rad * rotation), 
                            0f, 
                            Mathf.Cos(Mathf.Deg2Rad * rotation));
    }

    public static Vector3 RndDirection          () {
        var rotation = Random.Range(0, 360);
        return new Vector3( Mathf.Sin(Mathf.Deg2Rad * rotation), 
                            0f, 
                            Mathf.Cos(Mathf.Deg2Rad * rotation));
    }

    public static Vector3 exMirror                (this Vector3 vec, bool x = true, bool y = true, bool z = true) {
        vec.x = x ? -vec.x : vec.x;
        vec.y = y ? -vec.y : vec.y;
        vec.z = z ? -vec.z : vec.z;

        return vec;
    }

    public static Vector3 exSwapCoord             (this Vector3 vec,  Vector3 swapper, bool x, bool y, bool z) {
        vec.x = x ? swapper.x : vec.x;
        vec.y = y ? swapper.y : vec.y;
        vec.z = z ? swapper.z : vec.z;

        return vec;
    }

    public static Vector3 exSwapCoord             (this Vector3 vec, float x, float y, float z) {
        vec.x = x == 0 ? vec.x : x;
        vec.y = y == 0 ? vec.y : y;
        vec.z = z == 0 ? vec.z : z;
        
        return vec;
    }

    public static Vector3 exZero                  (this Vector3 vec, bool x, bool y, bool z) {
        vec.x = x ? 0 : vec.x;
        vec.y = y ? 0 : vec.y;
        vec.z = z ? 0 : vec.z;

        return vec;
    }


    public static float exGetAngle                (this Vector3 vec) {
        var rad     = Mathf.Atan2(vec.x, vec.z);
        var angle   = rad * Mathf.Rad2Deg;

        if (angle < 0) {
            angle = 360 + angle;
        }

        return angle;
    }
    public static float exGetAngle                (this Vector2 vec) {
        var rad     = Mathf.Atan2(vec.x, vec.y);
        var angle   = rad * Mathf.Rad2Deg;

        if (angle < 0) {
            angle = 360 + angle;
        }

        return angle;
    }
    


    public static bool exInsidePoly               (this Vector2Int p, Vector3[] polyPoints) {
        var j       = polyPoints.Length - 1;
        var inside  = false;

        for (int i = 0; i < polyPoints.Length; j = i++) {
            var pi = new Vector2(polyPoints[i].x, polyPoints[i].z);
            var pj = new Vector2(polyPoints[j].x, polyPoints[j].z);

            if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
                (p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
                inside = !inside;
        }

        return inside;
    }

}
