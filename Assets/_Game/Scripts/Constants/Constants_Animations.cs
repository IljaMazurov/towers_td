﻿namespace Constants {
    namespace Animations {

        public enum EventTrigger {
            OnMeleeFire                 = 0,
            OnMeleeFireEnd              = 10,
            OnRangeFire                 = 20,
            OnRangeFireEnd              = 30,

            OnAttackCustomTriggerEnd    = 35,

            OnReload                    = 40,
            OnReloadEnd                 = 50,

            OnDie                       = 60,

            OnStep                      = 70,
        }
    }
}
