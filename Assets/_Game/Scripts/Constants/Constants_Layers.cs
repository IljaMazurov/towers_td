﻿using System.Collections.Generic;

namespace Constants {
    public class Layers {
        public const int Default            = 0;
        public const int TransparentFX      = 1;
        public const int IgnoreRaycast      = 2;
        public const int Water              = 4;
        public const int UI                 = 5;
        public const int MapItems           = 10;
        public const int UnitItems          = 13;
        public const int TowerItems         = 16;
        public const int PostFX_Bloom       = 28;
    }
}
