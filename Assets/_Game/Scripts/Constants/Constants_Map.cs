﻿
namespace Constants {

    namespace Map {
        public enum CellType {
            Empty       = 0,

            Road_Start  = 10,
            Road        = 20,
            Road_End    = 30,

            Ground      = 100,
        }

        public enum CellNeighbors {
            Top,
            Right,
            Bottom,
            Left,
        }

    }
}
