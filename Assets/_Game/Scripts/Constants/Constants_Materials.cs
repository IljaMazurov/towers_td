﻿using System.Collections.Generic;
using Unity;
using UnityEngine;

namespace Constants {
    namespace Materials {
        public enum MaterialTypeIndex  : int {
            Enemy_Default       = 0,
            Enemy_Spawn         = 1,
            Enemy_Finish        = 2,


            Tower_Default       = 3,
            Tower_Spawn         = 4
        }

    }


}