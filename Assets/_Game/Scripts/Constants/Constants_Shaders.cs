﻿using System.Collections.Generic;
using Unity;
using UnityEngine;

namespace Constants {
    public static class Shaders {
        public static int MainTex           = Shader.PropertyToID("_MainTex");
        public static int MainTexST         = Shader.PropertyToID("_MainTex_ST");
        public static int Color             = Shader.PropertyToID("_Color");
        public static int TSize             = Shader.PropertyToID("_TSize");
        public static int TPower            = Shader.PropertyToID("_TPower");

        public static int ColorCenter       = Shader.PropertyToID("_ColorCenter");
        public static int ColorBorders      = Shader.PropertyToID("_ColorBorders");

        public static int DissolveAmount    = Shader.PropertyToID("_DissolveAmount");

        public static int Origin            = Shader.PropertyToID("_Origin");

    }

}