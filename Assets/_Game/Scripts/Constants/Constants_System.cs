﻿
namespace Constants {

    namespace System {
        public enum GameScenes {
            Loader_Main         = 100,

            Menu_Main           = 200,

            Battle_Main         = 300,
            Battle_UI_Overlay   = 310,
            Battle_UI_Windows   = 320,


            Camera_Vignette     = 900,
            Camera_PostFx       = 910,
        }

    }
}
