﻿using System.Collections.Generic;

namespace Constants {
    public class Tags {
        public const string UI_CameraTag            = "UI_MainCamera";
        public const string UI_CameraHolderTag      = "UI_MainCameraHolder";

        public const string UI_InfoCanvas           = "UI_InfoCanvas";
        public const string UI_ControllsCanvas      = "UI_ControllsCanvas";
        public const string UI_HealthCanvas         = "UI_HealthCanvas";
        public const string UI_WindowsCanvas        = "UI_WindowsCanvas";

        public const string WORLD_ControllerTag     = "World_MainController";
    }
}
