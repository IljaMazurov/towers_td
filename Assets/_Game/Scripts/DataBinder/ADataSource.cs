﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public abstract class ADataSource<T> : MonoBehaviour, DataReflection.IDataSource<T>{
    public      abstract    T       dataSource { get; protected set; }

    protected               void    OnDataUpdate    () {

    }
    public                  void    Subscribe       (string field, DataReflection.IDataBinder target) {
        var bindings    = BindingFlags.Public | BindingFlags.Instance;
        var dataType    = typeof(T);

        var fieldData   = dataType.GetProperty(field, bindings);
        if (fieldData  == null)
            return;

        var dataRef     = fieldData.GetValue(dataSource) as DataReflection.IDataSubscribable;
        dataRef.Subscribe(target);
    }

    public                  void    Unsubscribe     (string field, DataReflection.IDataBinder target) {
        var bindings    = BindingFlags.Public | BindingFlags.Instance;
        var dataType    = typeof(T);

        var fieldData   = dataType.GetProperty(field, bindings);
        if (fieldData  == null)
            return;

        var dataRef     = fieldData.GetValue(dataSource) as DataReflection.IDataSubscribable;
        dataRef.Unsubscribe(target);
    }

}

