﻿using UnityEngine;
using DataReflection;

public class DataBinder : MonoBehaviour, IDataBinder {
    [HideInInspector, SerializeField] BinderActivity    activityType = BinderActivity.Enable_Disable;

    [HideInInspector, SerializeField] Component         target;
    [HideInInspector, SerializeField] string            target_field;

    [HideInInspector, SerializeField] Component         source;
    [HideInInspector, SerializeField] string            source_field;

    [HideInInspector, SerializeField] string            global_source;
    [HideInInspector, SerializeField] string            global_source_child;

    [HideInInspector, SerializeField] string            stringFormat    = string.Empty;
    [HideInInspector, SerializeField] bool              call_ToString   = false;
    [HideInInspector, SerializeField] bool              setNullValues   = false;

    private void Awake      () {
        if (activityType == BinderActivity.Awake_Destroy)
            Subscribe();
    }
    private void OnDestroy  () {
        if (activityType == BinderActivity.Awake_Destroy)
            Unsubscribe();
    }


    private void OnEnable   () {
        if (activityType == BinderActivity.Enable_Disable)
            Subscribe();
    }
    private void OnDisable  () {
        if (activityType == BinderActivity.Enable_Disable)
            Unsubscribe();
    }


    private void Subscribe      () {
        if (!string.IsNullOrEmpty(global_source)) {
            DataReflectionCore. GetGlobal(global_source)?.
                                Subscribe(this);
        } else if (target != null && source != null) {
            var dataSource = source as IDataSourceBase;
            dataSource.Subscribe(source_field, this);
        }
    }
    private void Unsubscribe    () {
        if (!string.IsNullOrEmpty(global_source)) {
            DataReflectionCore. GetGlobal(global_source)?.
                                Unsubscribe(this);
        } else if (target != null && source != null) {
            var dataSource = source as IDataSourceBase;
            dataSource.Unsubscribe(source_field, this);
        }
    }

    public void UpdateValue(object val) {
        if (val == null && !setNullValues)
            return;

        if (!string.IsNullOrEmpty(global_source_child)) {
            val = DataReflectionCore.GetValue(val, global_source_child);
        }

        if (call_ToString) {
            val = val?.ToString();
        }

        if (stringFormat != string.Empty) {
            val = string.Format(stringFormat, val);
        }

        DataReflectionCore.SetValue(target, target_field, val);
    }


}

public enum BinderActivity {
    Awake_Destroy,
    Enable_Disable
}
