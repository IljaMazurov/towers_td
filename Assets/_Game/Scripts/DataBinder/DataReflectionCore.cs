﻿using UnityEngine;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System;

namespace DataReflection {

    public static class DataReflectionCore {
        private static bool                                     DatabaseInited          = false;
        private static Dictionary<string, IDataSubscribable>    DatabaseCahcedTypes     = new Dictionary<string, IDataSubscribable>();

        public static System.Type                               GlobalDataType          => typeof(GameRuntimeData);
        public static List<string>                              GlobalFieldsIngore      = new List<string> {
            "useGUILayout", "runInEditMode", "isActiveAndEnabled", "transform", "gameObject", "tag", 
            "rigidbody", "rigidbody2D", 
            "collider", "collider2D",
            "animation", "particleSystem", "hingeJoint","name","hideFlags",
            "camera", "light", "constantForce", "renderer", "audio", "guiText", "guiElement", "guiTexture", "networkView", 

        };
        public static Dictionary<System.Type, List<string>>     ComponentsFilterMap     = new Dictionary<System.Type, List<string>> {
            // ignore
            { typeof (RectTransform),   new List<string> {} },
            { typeof (CanvasRenderer),  new List<string> {} },
            { typeof (DataBinder),      new List<string> {} },

            // filter
            { typeof (TMPro.TextMeshProUGUI), new List<string> {
                "text", "color", "fontSize", "isRightToLeftText"
            } },
            { typeof (TMPro.TextMeshPro),   new List<string> {
                "text", "color", "fontSize", "isRightToLeftText"
            } },

            { typeof (UnityEngine.UI.Image), new List<string> {
                "sprite", "material", "color"
            } },
            { typeof (SpriteRenderer), new List<string> {
                "sprite", "material", "color"
            } },
        };

        private static void                 FillTypeValues      (object type_ref, System.Type type, string route) {
            route           = string.IsNullOrEmpty(route) ? route : $"{route}/";

            var bindings        = BindingFlags.Public | BindingFlags.Instance;
            var db_properties   = type.GetProperties(bindings);

            var dataList        = new List<PropertyInfo>();
            var holdersList     = new List<PropertyInfo>();

            // fill marked memberList
            foreach (var property in db_properties) {
                var attributes = property.GetCustomAttributes().FirstOrDefault(x => x is IRDataBinder);
                if (attributes is RDataAttribute) {
                    dataList.Add        (property);
                } else if (attributes is RHolderAttribute) {
                    holdersList.Add     (property);
                }
            }

            // fill datavars
            foreach (var data in dataList) {
                object dataRef = data.GetValue(type_ref);
                if (dataRef is IDataSubscribable sub) {
                    var curName = $"{route}{data.Name}";
                    DatabaseCahcedTypes.Add(curName, sub);
                }
            }

            // loop holders
            foreach (var holder in holdersList) {
                var deepRoute   = $"{route}{holder.Name}";
                var deepType    = holder.PropertyType;
                var dataRef     = holder.GetValue(type_ref);

                FillTypeValues(dataRef, deepType, deepRoute);
            }

        }
        public static void                  PrecacheDatabase    (object db_ref) {
            DatabaseCahcedTypes.Clear();
            FillTypeValues(db_ref, GlobalDataType, "");
            DatabaseInited = true;
        }


        public static IDataSubscribable     GetGlobal           (string sourceName) {
            if (!DatabaseInited) {
                var db = GameCore.Data.DynamicData; // load and init database
            }

            IDataSubscribable result = null;
            if (DatabaseCahcedTypes.ContainsKey(sourceName))
                result = DatabaseCahcedTypes[sourceName];

            return result;
        }
        
        
        public static void                  SetValue            (object target, string classMemberName, object value) {
            //if (value == null)
                //return;

            var bindings            = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var targetType          = target.GetType();
            var targetProperty      = targetType.GetProperty(classMemberName, bindings);

            if (targetProperty == null)
                return;

            targetProperty.SetValue(target, value);
        }
        public static object                GetValue            (object source, string classMemberName) {
            var bindings        = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var targetType      = source?.GetType();
            var targetProperty  = targetType?.GetProperty(classMemberName, bindings);

            return targetProperty?.GetValue(source);
        }

    }

}