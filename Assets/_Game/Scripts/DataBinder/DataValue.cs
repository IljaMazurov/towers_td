﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataReflection;

public class DataValue<T> : IDataSubscribable {
    private List<IDataBinder>   _subscribers = new List<IDataBinder>();
    private T                   _genericValue;

    public T Value {
        get { return _genericValue; }
        set {
            _genericValue = value;

            var cnt = _subscribers.Count;
            for (int i = 0; i < cnt; i++)
                _subscribers[i].UpdateValue(_genericValue);
        }
    }

    public void     Subscribe   (IDataBinder binder) {
        binder.UpdateValue(_genericValue);
        _subscribers.Add(binder);
    }

    public void     Unsubscribe (IDataBinder binder) {
        _subscribers.Remove(binder);
    }
}
