﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Reflection;
using DataReflection;

[CustomEditor(typeof(DataBinder), true)]
public class DataBinder_Inspector : Editor {
    private GUIStyle fontStyle_green;
    private GUIStyle fontStyle_red;
    private GUIStyle fontStyle_yellow;

    public SerializedObject         _serializedObject = null;
    private DataBinder              gui_target;

    private System.Type getMemberType(MemberInfo member) {
        System.Type targetType = null;

        if (member is FieldInfo field)
            targetType = field.FieldType;
        else if (member is PropertyInfo prop)
            targetType = prop.PropertyType;

        return targetType;
    }
    private System.Type getGenericTypeInData(System.Type dataType) {
        var bindings                = BindingFlags.Public | BindingFlags.Instance;
        System.Type targetType      = dataType?.GetProperties(bindings).
                                                FirstOrDefault(prop => prop.PropertyType.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IData)))?.PropertyType;

        return targetType;
    }

    public override void    OnInspectorGUI      () {
        if (fontStyle_green == null) {
            fontStyle_green = new GUIStyle(GUI.skin.label) {
                alignment   = TextAnchor.MiddleCenter,
                fontSize    = 11,
                fontStyle   = FontStyle.Bold
            };
            fontStyle_green.normal.textColor = Color.green;
        }

        if (fontStyle_red == null) {
            fontStyle_red = new GUIStyle(GUI.skin.label) {
                alignment   = TextAnchor.MiddleCenter,
                fontSize    = 11,
                fontStyle   = FontStyle.Bold
            };
            fontStyle_red.normal.textColor = Color.red;
        }

        if (fontStyle_yellow == null) {
            fontStyle_yellow = new GUIStyle(GUI.skin.label) {
                alignment   = TextAnchor.MiddleCenter,
                fontSize    = 11,
                fontStyle   = FontStyle.Bold
            };
            fontStyle_yellow.normal.textColor = Color.yellow;
        }

        serializedObject.Update();
        gui_target      = (DataBinder)target;


        var activity = serializedObject.FindProperty("activityType");

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Activity Type", GUILayout.Width(90f));
        EditorGUILayout.PropertyField(activity, new GUIContent(""));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        var sourceType  = DrawSourceValues();
        var targetType  = DrawTargetValues();
        DrawConfigs(sourceType, targetType);

        if (GUI.changed)
            EditorUtility.SetDirty(gui_target);

        serializedObject.ApplyModifiedProperties();
    }

    private void OnTargetChanged    (object target) {
        var data = target as Editor_Binder_TargetFieldSet;

        serializedObject.FindProperty("target").objectReferenceValue    = data.target;
        serializedObject.FindProperty("target_field").stringValue       = data.field;

        EditorUtility.SetDirty(gui_target);

        serializedObject.ApplyModifiedProperties();
    }
    private void OnSourceChanged    (object target) {
        var data = target as Editor_Binder_TargetFieldSet;

        serializedObject.FindProperty("source").objectReferenceValue        = data.target;
        serializedObject.FindProperty("source_field").stringValue           = data.field;
        serializedObject.FindProperty("global_source").stringValue          = data.globalVar;
        serializedObject.FindProperty("global_source_child").stringValue    = null;

        EditorUtility.SetDirty(gui_target);

        serializedObject.ApplyModifiedProperties();
    }
    private void OnDBSourceChanged  (object target) {
        var data = target as Editor_Binder_TargetFieldSet;

        serializedObject.FindProperty("source").objectReferenceValue        = data.target;
        serializedObject.FindProperty("source_field").stringValue           = data.field;
        serializedObject.FindProperty("global_source").stringValue          = data.globalVar;
        serializedObject.FindProperty("global_source_child").stringValue    = data.field_child;

        EditorUtility.SetDirty(gui_target);

        serializedObject.ApplyModifiedProperties();
    }

    private System.Type DrawSourceValues   () {
        var bindings            = BindingFlags.Public | BindingFlags.Instance;

        var dataSourceComponent = gui_target.GetComponentInParent<IDataSourceBase>() as MonoBehaviour;
        var dataSourceType      = dataSourceComponent?.GetType();
        var properties          = dataSourceType?.GetProperties(bindings);


        var propType            = getGenericTypeInData(dataSourceType);

        var dataProps          = propType?.GetProperties(bindings);
        if (dataProps == null)
            dataProps = new PropertyInfo[0];

        var sourceProp          = serializedObject.FindProperty("source");
        var sourceField         = serializedObject.FindProperty("source_field");
        var globalField         = serializedObject.FindProperty("global_source").stringValue;
        var globalFieldChild    = serializedObject.FindProperty("global_source_child").stringValue;

        var currentSource       = sourceProp.objectReferenceValue;
        var currentType         = currentSource?.GetType();

        var buttonName          = currentType != null ? $"{currentType?.Name}->{sourceField.stringValue}" : " - Select Source - ";
        if (!string.IsNullOrEmpty(globalField)) {
            buttonName = globalField.Replace("/", "->");
            if (!string.IsNullOrEmpty(globalFieldChild))
                buttonName += $"->[{globalFieldChild}]";

        } else {
            buttonName = currentType != null ? $"{currentType?.Name}->{sourceField.stringValue}" : " - Select Source - ";
        }

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Data Source", GUILayout.Width(90f));
        if (GUILayout.Button(buttonName)) {
            var menu = new GenericMenu();

            menu.AddItem(new GUIContent(" - Select Source - "), false, OnSourceChanged, new Editor_Binder_TargetFieldSet {
                target      = null,
                field       = null,
                globalVar   = null
            });

            if (dataProps.Length > 0) {
                menu.AddSeparator("");
                foreach (var prop in dataProps) {
                    var attributes  = prop.GetCustomAttributes().Where(x => x is IRDataBinder);
                    var skip        = true;
                    foreach (var attr in attributes) {
                        if (attr is RDataAttribute) {
                            skip = false;
                        }
                    }

                    if (skip)
                        continue;

                    var propName    = prop.Name;
                    var isSelected  = currentType?.Name == dataSourceType.Name && propName == sourceField?.stringValue;

                    menu.AddItem(new GUIContent($"Parent-{dataSourceType.Name}/{propName}"), isSelected, OnSourceChanged, new Editor_Binder_TargetFieldSet {
                        target  = dataSourceComponent,
                        field   = propName
                    });
                }
            }

            menu.AddSeparator("");
            var DbType      = DataReflectionCore.GlobalDataType;
            DrawFieldsRoute(menu, DbType, "", globalField, globalFieldChild);


            menu.ShowAsContext();
        }

        EditorGUILayout.EndHorizontal();

        System.Type sourceType = null;
        if (currentSource != null && !string.IsNullOrEmpty(sourceField.stringValue)) {
            var srcData     = getGenericTypeInData(dataSourceType);
            var property    = srcData.GetProperty(sourceField.stringValue);
            var type        = property?.PropertyType;
            if (type != null && type.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IDataSubscribable))) { // -> DataValue
                type = type.GenericTypeArguments[0];
            }

            sourceType      = type;
        }

        if (!string.IsNullOrEmpty(globalField)) {
            var result = DataReflectionCore.GlobalDataType;
            var map = new List<string>(globalField.Split('/'));

            while (map.Count > 0) {
                var step        = map.exFirstAndRemove();
                var property    = result?.GetProperty(step);
                result          = property?.PropertyType;
            }

            if (result != null && result.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IDataSubscribable))) { // -> DataValue
                result = result.GenericTypeArguments[0];
            }

            if (!string.IsNullOrEmpty(globalFieldChild)) {
                var property    = result?.GetProperty(globalFieldChild);
                result          = property?.PropertyType;
            }

            sourceType = result;
        }

        return sourceType;
    }
    private System.Type DrawTargetValues   () {
        var bindings            = BindingFlags.Public | BindingFlags.Instance;

        var targetProp          = serializedObject.FindProperty("target");
        var targeField          = serializedObject.FindProperty("target_field");

        var currentTarget       = targetProp.objectReferenceValue;
        var currentType         = currentTarget?.GetType();

        var components          = gui_target.GetComponents<Component>();
        var buttonName          = currentType != null ? $"{currentType.Name}->{targeField.stringValue}" : " - Select Target - ";

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Data Target", GUILayout.Width(90f));
        if ( GUILayout.Button(buttonName) ) {
            var menu = new GenericMenu();

            menu.AddItem(new GUIContent(" - Select Target - "), false, OnTargetChanged, new Editor_Binder_TargetFieldSet { 
                field   = null,
                target  = null
            });

            menu.AddSeparator("");

            for (int i = 0; i < components.Length; i++) {
                var component       = components[i];
                var componentType   = component.GetType();

                // getter-setter
                var properties      = componentType.GetProperties   (bindings);
                var skip            = DataReflectionCore.ComponentsFilterMap.ContainsKey(componentType);
                foreach (var prop in properties) {
                    if (skip && !DataReflectionCore.ComponentsFilterMap[componentType].Contains(prop.Name))
                        continue;

                    if (DataReflectionCore.GlobalFieldsIngore.Contains(prop.Name))
                        continue;

                    var isSelected = component == currentTarget && targeField.stringValue == prop.Name;
                    menu.AddItem(new GUIContent($"{componentType.Name}/{prop.Name}"), isSelected, OnTargetChanged, new Editor_Binder_TargetFieldSet {
                        target  = component,
                        field   = prop.Name
                    });
                }
            }

            menu.ShowAsContext();
        }

        EditorGUILayout.EndHorizontal();


        System.Type targetType = null;
        if (currentTarget != null && !string.IsNullOrEmpty(targeField.stringValue)) {
            var property    = currentTarget.GetType().GetProperty(targeField.stringValue);
            targetType      = property?.PropertyType;
        }

        return targetType;
    }
    
    
    private void        DrawFieldsRoute     (GenericMenu menu, System.Type type, string route, string global, string child) {
        var bindings                = BindingFlags.Public | BindingFlags.Instance;
        var isChild                 = false;
        route                       = string.IsNullOrEmpty(route) ? route : $"{route}/";

        if (type.GetTypeInfo().ImplementedInterfaces.Contains(typeof (IDataSubscribable))) { // -> DataValue
            type        = type.GenericTypeArguments[0];
            isChild     = true;
        }

        var db_properties   = type.GetProperties(bindings);

        var dataList        = new List<PropertyInfo>();
        var holdersList     = new List<PropertyInfo>();

        foreach (var property in db_properties) {
            var curName         = isChild ? $"{route}" : $"{route}{property.Name}";
            var attributes      = property.GetCustomAttributes().Where(x => x is IRDataBinder);

            foreach (var attr in attributes) {
                if (attr is RDataAttribute) {
                    var isSelected = string.IsNullOrEmpty(child) ?
                                                global == curName :
                                                global == curName.Remove(curName.Length - 1) && child == property.Name;

                    menu.AddItem(new GUIContent(isChild ? curName + property.Name : curName), isSelected, OnDBSourceChanged, new Editor_Binder_TargetFieldSet {
                        target = null,
                        field = null,
                        field_child = isChild ? property.Name : null,
                        globalVar = isChild ? curName.Remove(curName.Length - 1) : curName
                    });

                } else if (attr is RContainerAttribute || attr is RHolderAttribute) {
                    DrawFieldsRoute(menu, property.PropertyType, curName, global, child);
                } 
            }
        }
    }


    private void        DrawConfigs        (System.Type sourceType, System.Type targetType) {
        var stringFormat    = serializedObject.FindProperty("stringFormat");
        var toString        = serializedObject.FindProperty("call_ToString");
        var setNullValues   = serializedObject.FindProperty("setNullValues");

        var style       = sourceType == targetType ? fontStyle_green : fontStyle_red;
        if (sourceType != targetType && targetType == typeof(string) && toString.boolValue)
            style = fontStyle_yellow;

        if (targetType == typeof(object)) {
            style = fontStyle_green;
        }

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField($"SourceType :: [{sourceType?.Name}]", style, GUILayout.ExpandWidth(true));
        EditorGUILayout.LabelField($"TargetType :: [{targetType?.Name}]", style, GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Can use Null", GUILayout.Width(90f));
        EditorGUILayout.PropertyField(setNullValues, new GUIContent(""));
        EditorGUILayout.EndHorizontal();

        if (sourceType != targetType && targetType == typeof(string)) {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Call ToString()", GUILayout.Width(90f));
            EditorGUILayout.PropertyField(toString, new GUIContent(""));
            EditorGUILayout.EndHorizontal();
        } else {
            toString.boolValue = false;
        }

        if (targetType == typeof(string) && (sourceType == targetType || toString.boolValue)) {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("String Format", GUILayout.Width(90f));
            EditorGUILayout.PropertyField(stringFormat, new GUIContent(""));
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }
}

public class Editor_Binder_TargetFieldSet {
    public Object   target;
    public string   field;
    public string   field_child;
    public string   globalVar;
}
