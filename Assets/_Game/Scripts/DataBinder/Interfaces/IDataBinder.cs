﻿namespace DataReflection {
    public interface IDataBinder {
        void UpdateValue(object val);
    }
}
