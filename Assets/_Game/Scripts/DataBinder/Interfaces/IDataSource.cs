﻿namespace DataReflection { 
    public interface IDataSource<T> : IDataSourceBase {
        T dataSource { get; }
    }

    public interface IDataSourceBase {
        void Subscribe      (string field, IDataBinder target);
        void Unsubscribe    (string field, IDataBinder target);
    }
}