﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataReflection { 
    public interface IDataSubscribable  {
        void Subscribe      (IDataBinder binder);
        void Unsubscribe    (IDataBinder binder);
    }
}
