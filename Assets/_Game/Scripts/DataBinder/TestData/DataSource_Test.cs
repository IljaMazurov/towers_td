﻿using System;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataReflection;

public class DataSource_Test : ADataSource<TestData> {

    public override TestData dataSource { get; protected set; } = new TestData();

    private TestGameDataBase gameDB;

    public void Awake() {
        var loader = GameDB.db;
    }

    public void Start() {
        (dataSource as TestData).someFloatValue.Value = 0.75f;
        (dataSource as TestData).someIntValue.Value = 75;
        (dataSource as TestData).someStringValue.Value = "Test text";

        StartCoroutine(delayed());
    }

    private IEnumerator delayed() {
        int softCounter = 0;
        int hardCounter = 0;

        while(true) {
            yield return new WaitForSeconds(1f);

            GameDB.db.Menu.Inventory.hardCurrency.Value = hardCounter++;
            GameDB.db.Menu.Inventory.softCurrency.Value = softCounter--;
        }

    }



}
