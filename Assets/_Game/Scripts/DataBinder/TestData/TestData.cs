﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestData : DataReflection.IData {
    public DataValue<string>    someStringValue     = new DataValue<string>();
    public DataValue<float>     someFloatValue      = new DataValue<float>();
    public DataValue<int>       someIntValue        = new DataValue<int>();
}
