﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataReflection {
    public static class GameDB {
        private     static TestGameDataBase _db     = new TestGameDataBase();
        public      static TestGameDataBase   db     => _db;
    }


    public class TestGameDataBase {
        public TestGameDataBase() { DataReflectionCore.PrecacheDatabase(this); }

        public FarmGameData     Menu      = new FarmGameData();
        public BattleGameVars   Battle    = new BattleGameVars();
        public GlobalVars       Common    = new GlobalVars();
    }

    public class FarmGameData {
        public DataValue<float> menuFloat_1     { get; private set; }   = new DataValue<float>  ();
        public DataValue<float> menuFloat_2     { get; private set; }   = new DataValue<float>  ();
        public DataValue<int>   menuIntt_1      { get; private set; }   = new DataValue<int>    ();

        public Inventory        Inventory                               = new Inventory         ();
    }

    public class Inventory {
        public DataValue<int>   hardCurrency    { get; private set; }   = new DataValue<int>    ();
        public DataValue<int>   softCurrency    { get; private set; }   = new DataValue<int>    ();
    }

    public class BattleGameVars {
        public DataValue<int> levelIndex        { get; private set; }   = new DataValue<int>    ();
        public DataValue<int> enemiesKilled     { get; private set; }   = new DataValue<int>    ();
        public DataValue<int> totalScore        { get; private set; }   = new DataValue<int>    ();
    }

    public class GlobalVars {
        public DataValue<int> playerLevel       { get; private set; }   = new DataValue<int>    ();
    }

    
}