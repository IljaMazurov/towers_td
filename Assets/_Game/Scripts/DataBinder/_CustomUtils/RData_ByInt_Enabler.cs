﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RData_ByInt_Enabler : MonoBehaviour {
    [SerializeField] private List<ItemEnablerSet>   targets         = new List<ItemEnablerSet>();
    [SerializeField] private float                  incrementDelay  = 1.25f;

    private Coroutine   _activeIncrement;

    public int Input { 
        set {
            UpdateValue(value, false);
        }
    }

    private void            Awake               () {
        UpdateValue(0, true);
    }

    private void            UpdateValue         (int val, bool force = false) {
        if (force) {
            var cnt = targets.Count;
            for (int i = 0; i < cnt; i++) {
                targets[i]?.Enabled?.gameObject?.SetActive(val >= i);
                targets[i]?.Disabled?.gameObject?.SetActive(val < i);
                targets[i]?.OnEnable?.ResetToBeginning();
            }
            return;
        }
        _activeIncrement.End(ref _activeIncrement, this);
        _activeIncrement = StartCoroutine(incrementer(val));
    }

    private IEnumerator     incrementer         (int newValue) {
        var delay   = GameCore.Utils.Yields.mWait(incrementDelay);
        var cnt     = targets.Count;

        for (int i = 0; i < cnt; i++) {
            targets[i]?.Enabled?.gameObject?.SetActive(newValue >= i);
            targets[i]?.Disabled?.gameObject?.SetActive(newValue < i);
            targets[i]?.OnEnable?.ResetToBeginning();
            targets[i]?.OnEnable?.PlayForward();
            yield return delay;
        }
    }

}

[System.Serializable]
public class ItemEnablerSet {
    public Transform Enabled;
    public Transform Disabled;
    public UITweener OnEnable;
}
