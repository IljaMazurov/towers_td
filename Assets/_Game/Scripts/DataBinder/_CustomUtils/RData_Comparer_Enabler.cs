﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RData_Comparer_Enabler : MonoBehaviour {
    [SerializeField] private MonoBehaviour target;

    private object _item_a;
    private object _item_b;

    public object InputA { 
        set {
            _item_a = value;
            CheckEquality();
        }
    }
    public object InputB {
        set {
            _item_b = value;
            CheckEquality();
        }
    }

    private void CheckEquality() {
        if (_item_a == null || _item_b == null) {
            target.enabled = false;
            return;
        }

        target.enabled  = _item_a.Equals(_item_b);
    }


}

