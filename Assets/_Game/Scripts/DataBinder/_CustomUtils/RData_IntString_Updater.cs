﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RData_IntString_Updater : MonoBehaviour {
    [SerializeField] private TMPro.TMP_Text     targetField;
    [Space]
    [SerializeField] private string             stringFormat;
    [Space]
    [SerializeField] private EaseType           incrementType = EaseType.easeOut_x2;
    [SerializeField] private float              incrementTime = 1.25f;

    private Coroutine   _activeIncrement;
    private int         _lastValue;

    public int Input { 
        set {
            UpdateValue(value, false);
        }
    }

    private void            Awake               () {
        _lastValue = 0;
        UpdateValue(0, true);
    }

    private void            UpdateValue         (int val, bool force = false) {
        if (incrementType == EaseType.instant || force) {
            setText(val);
            _lastValue = val;
            return;
        }

        _activeIncrement.End(ref _activeIncrement, this);
        _activeIncrement = StartCoroutine(incrementer(val));
    }

    private void            setText             (int val) {
        string text = null;
        if (!string.IsNullOrEmpty(stringFormat)) {
            text = string.Format(stringFormat, val);
        } else {
            text = val.ToString();
        }

        targetField.text = text;
    }
    private IEnumerator     incrementer         (int newValue) {
        var oldValue    = _lastValue;
        var diff        = newValue - oldValue;

        var time = 0f;
        while (time < incrementTime) {
            var progress    = Ease.Do(time / incrementTime, incrementType);
            var setVal      = diff * progress;

            _lastValue = (int)(oldValue + setVal);
            setText(_lastValue);

            yield return null;
            time += Time.deltaTime;
        }

        _lastValue = newValue;
        setText(newValue);
    }

}
