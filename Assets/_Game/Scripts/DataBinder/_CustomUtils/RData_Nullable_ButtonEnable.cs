﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RData_Nullable_ButtonEnable : MonoBehaviour {
    private Button         _Button;
    private void Awake() {
        _Button = GetComponent<Button>();
    }

    public object Input { 
        set {
            if (_Button != null)
                _Button.interactable = value != null;
        }
    }

}
