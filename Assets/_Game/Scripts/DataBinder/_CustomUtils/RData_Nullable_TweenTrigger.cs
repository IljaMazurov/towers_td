﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RData_Nullable_TweenTrigger : MonoBehaviour {
    [SerializeField] private UITweener[]    _TweenList;
    [SerializeField] private bool           DisableOnStart      = true;
    [SerializeField] private bool           ReverseAnimation    = false;

    public object Input { 
        set {
            if (value != null) {
                PlayTween_Forward();
            } else {
                PlayTween_Backward();
            }
        }
    }

    private void Awake              () {
        if (_TweenList != null) {

            foreach (var tween in _TweenList) {
                if (ReverseAnimation) {
                    tween.ResetToEnding();
                } else {
                    if (DisableOnStart)
                        tween.enabled = false;
                    tween.ResetToBeginning();
                }
            }

        } 
    }

    private void PlayTween_Forward  () {
        if (_TweenList != null) {
            foreach (var tween in _TweenList) {
                if (ReverseAnimation) {
                    tween.ResetToEnding();
                    tween.PlayReverse();
                } else {
                    tween.ResetToBeginning();
                    tween.PlayForward();
                }
            }
        }
    }
    private void PlayTween_Backward () {
        if (_TweenList != null) {
            foreach (var tween in _TweenList) {
                if (!ReverseAnimation) {
                    tween.ResetToEnding();
                    tween.PlayReverse();
                } else {
                    tween.ResetToBeginning();
                    tween.PlayForward();
                }
            }
        }
    }
}
