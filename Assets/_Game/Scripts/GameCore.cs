﻿namespace GameCore {

    public static class Battle { 
        public static       IObjectPoolManager      Pool            =>      BattleObjectsPool.Get();
        public static       IBattleMapManager       Map             =>      BattleMapManager.Get();
        public static       IBattleDataManager      CacheData       =>      BattleDataManager.Get();
    }

    public static class UI { 
        public static       IWindowManager          Windows         =>      WindowManager.Get();
    }

    public static class System { 
        public static       IEventsManager          Events          =>      EventsManager.Get();
        public static       ISceneManager           Scene           =>      GameSceneManager.Get();
        public static       IUpdateManager          Updater         =>      GameUpdateManager.Get();
    }

    public static class ECS { 
        public static       IECSWorldManager        World           =>      GameECSWorldManager.Get();
        public static       IECSParticlesManager    Particles       =>      ECSParticlesManager.Get();
    }

    public static class Configs { 
        public static       AssetsDataBase          Assets          =>      AConfigFile.Get<AssetsDataBase>();
        public static       GlobalConfig            Globals         =>      AConfigFile.Get<GlobalConfig>();
        public static       BattleConfig            Battle          =>      AConfigFile.Get<BattleConfig>();
    }

    public static class Data    { 
        public static       GameRuntimeData         DynamicData     =>      GameRuntimeData.Get();

    }

    public static class Utils {
        public static       YieldsUtil              Yields          =>      YieldsUtil.Get();
    }

}
