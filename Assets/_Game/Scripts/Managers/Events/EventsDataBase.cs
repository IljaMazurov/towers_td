﻿using UnityEngine;

namespace GameEvents {
    public interface IEventInfo { }

    namespace Windows {
        public struct RequestWindow : IEventInfo {
            public System.Type  windowType;
            public object       windowData;
        }
        public struct OnWindowClosed : IEventInfo {
            public System.Type  windowType;
        }
    }

    namespace Battle {

        namespace Map {
            public struct LoadStart      : IEventInfo {
                public ABattleMap map;
            }

            public struct RoadsReady     : IEventInfo {
                public ABattleMap map;
            }

            public struct BlocksSpawned  : IEventInfo {
                public ABattleMap map;
            }

        }

        namespace Game {
            public struct StartGame             : IEventInfo { }
            public struct CantSpawnTower        : IEventInfo { }
            public struct BaseDamagged          : IEventInfo { }
            public struct SendNextWave          : IEventInfo { }
        }

        namespace Enemies {
            public struct Destroyed             : IEventInfo {
                public Constants.Enemies.EType  enemyType;
            
            }
        }

        namespace TouchControlls {
            public struct OnTouchDown : IEventInfo {
                public Vector3 touchSpot;
            }

            public struct OnTouchMove : IEventInfo {
                public Vector3 touchSpot;
                public Vector2 direction;
            }

            public struct OnTouchUp : IEventInfo {
                public Vector3 touchSpot;
                public Vector2 direction;
            }

            public struct OnTouchClick : IEventInfo {
                public Vector3 touchSpot;
            }
        }
    
    }


}
