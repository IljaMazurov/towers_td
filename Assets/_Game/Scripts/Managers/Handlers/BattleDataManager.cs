﻿using System.Collections.Generic;
using Constants.Materials;
using Unity.Collections;
using UnityEngine;

using BlobNav       = Unity.Entities.BlobAssetReference<Blob_EnemyNavigation>;
using BlobPathList  = System.Collections.Generic.List<Unity.Entities.BlobAssetReference<Blob_EnemyNavigation>>;

using BlobTowerMap  = Unity.Entities.BlobAssetReference<Blob_MapFloor>;

public class BattleDataManager : ASingleton<BattleDataManager>, IBattleDataManager {
    // DataType -> EnemyID -> ComponentData
    private NativeHashMap<int, EnemyPositionsCache>                     _enemiesPositions;

    private Dictionary<MaterialTypeIndex, Material>                     _materialsCache;

    private Dictionary<int, GameObject>                                 _towerPrefabs;
    private BlobPathList                                                _enemyPath;

    private BlobTowerMap                                                _towersMap;

    public BattleDataManager() {
        _towerPrefabs               = new Dictionary<int, GameObject>();
        _enemyPath                  = new BlobPathList();

        _enemiesPositions           = new NativeHashMap<int, EnemyPositionsCache>(512, Allocator.Persistent);
        _materialsCache             = new Dictionary<MaterialTypeIndex, Material>();
    }


    public void                                     mSaveMaterial                  (MaterialTypeIndex type, Material mat) {
        if (_materialsCache.ContainsKey(type))
            _materialsCache.Add(type, null);

        _materialsCache[type] = mat;
    }
    public Material                                 mGetMaterial                   (MaterialTypeIndex type) {
        return _materialsCache[type];
    }



    public void                                     mSaveTowerPrefab               (int level, GameObject prefab) {
        if (!_towerPrefabs.ContainsKey(level))
            _towerPrefabs.Add(level, null);

        _towerPrefabs[level] = prefab;
    }
    public GameObject                               mGetTowerPrefab                (int level) {
        if (!_towerPrefabs.ContainsKey(level))
            return null;

        return _towerPrefabs[level];
    }


    public void                                     mAddEnemyRoad                  (BlobNav road) {
        _enemyPath.Add(road);
    }
    public BlobPathList                             mGetEnemyRoads                 () {
        return new BlobPathList(_enemyPath);
    }

    public void                                     mSaveTowersMapRef              (BlobTowerMap map) {
        _towersMap = map;
    }
    public BlobTowerMap                             mGetTowersMapRef               () {
        return _towersMap;
    }

    
    public NativeHashMap<int, EnemyPositionsCache>  mGetEnemyPositionsCacheRef     () {
        return _enemiesPositions;
    }

}


