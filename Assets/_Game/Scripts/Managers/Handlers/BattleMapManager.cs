﻿using System;
using System.Linq;
using System.Collections.Generic;
using Unity.Mathematics;
using UniRx;
using UnityEngine;

public class BattleMapManager : ASingleton<BattleMapManager>, IBattleMapManager {
    private     ABattleMap                              _currentMap;
    private     List<List<AMapCell>>                    _roadPaths;
    private     List<List<float3>>                      _roadPaths_f3;

    private     List<AMapCell>                          _startCells;
    private     List<AMapCell>                          _finishCells;

    private     bool                    _roadsCalculated = false;

    private     IDisposable             _pathCalcTask;
    private     IDisposable             _mapBuildAwaiter;

    public      bool                    map_IsMapinited         => _currentMap != null;
    public      bool                    map_AreBlocksSpawned    { get; set; }   = false;
    public      ABattleMap              map_ActiveMapData       => _currentMap;
    public      List<List<AMapCell>>    map_ActiveMapRoads      => _roadPaths;
    public      List<List<float3>>      map_ActiveMapRoads_f3   => _roadPaths_f3;

    public      List<AMapCell>          map_StartCells          => _startCells;

    public      List<AMapCell>          map_FinishCells         => _finishCells;

    public void mLoad           (ABattleMap map) {
        var ecs = GameCore.ECS.World;

        if (_pathCalcTask != null) {
            _pathCalcTask.Dispose();
            _pathCalcTask = null;
        }

        if (_finishCells == null)
            _finishCells = new List<AMapCell>();
        _finishCells.Clear();

        if (_startCells == null)
            _startCells = new List<AMapCell>();
        _startCells.Clear();

        _currentMap         = map;


        ClearRoadPaths  ();
        CalcRoadCells   ();

        GameCore.System.Events.mTrigger(new GameEvents.Battle.Map.LoadStart { map = map });

        _mapBuildAwaiter = Observable.  
                                EveryUpdate ().
                                Delay       (TimeSpan.FromSeconds(2)).
                                SkipWhile   (x => ecs.mCalcEntitiesWith<Block_SpawnComponent>() > 0).
                                Subscribe   (x => {
                                    map_AreBlocksSpawned = true;
                                    GameCore.System.Events.mTrigger(new GameEvents.Battle.Map.BlocksSpawned { map = map });

                                    if (_mapBuildAwaiter != null)
                                        _mapBuildAwaiter.Dispose();
                                });
    }


    private void ClearRoadPaths     () {

        map_AreBlocksSpawned    = false;
        _roadsCalculated        = false;

        if (_roadPaths != null) {
            foreach (var path in _roadPaths) {
                if (path != null)
                    path.Clear();
            }
            _roadPaths.Clear();
        } else {
            _roadPaths = new List<List<AMapCell>>();
        }

        if (_roadPaths_f3 != null) {
            foreach (var path in _roadPaths_f3) {
                if (path != null)
                    path.Clear();
            }
            _roadPaths_f3.Clear();
        } else {
            _roadPaths_f3 = new List<List<float3>>();
        }
    }
    private void CalcRoadCells      () {
        var cells       = _currentMap.map_Cells;
        var startCells  = new List<AMapCell>();
        foreach (var cell in cells) {
            if (cell.cell_CellType == Constants.Map.CellType.Road_Start) {
                _startCells.Add(cell);
                startCells.Add(cell);
            }
        }

        var roadCalculators = new List< IObservable<List<List<AMapCell>>> >();
        foreach (var cell in startCells) {

            var o = Observable.Start(() => {
                var pathList = new List<List<AMapCell>>();
                pathList.Add(new List<AMapCell>() { cell });

                for (int i = 0; i < pathList.Count; i++) {
                    var road            = pathList[i];
                    var moveCell        = road[road.Count - 1];
                    var blocker         = false; 
                    
                    var visitedCells    = new List<AMapCell>(road);

                    if (pathList.Count > 128)
                        break; // fail-safe

                    while (!blocker) {

                        if (pathList.Count > 128) 
                            break; // fail-safe

                        int roads = 0; 
                        for (int c = 0; c < moveCell.cell_Neighbors.Length; c++) {

                            // skip empty cells
                            if (moveCell.cell_Neighbors[c] == -1)
                                continue;

                            var nextCellData = cells[moveCell.cell_Neighbors[c]];

                            // skip visited cells
                            if (visitedCells.Contains(nextCellData))
                                continue;

                            // add current to visited
                            visitedCells.Add(nextCellData);

                            // if next cell is road -> move
                            if (nextCellData.cell_CellType == Constants.Map.CellType.Road) {
                                roads++;

                                if (roads > 1) { 
                                    // it is not first neighbor Road tile -> split roads
                                    var splitPath = new List<AMapCell>(road);
                                    splitPath.Add(nextCellData);
                                    pathList.Add(splitPath);
                                } else {
                                    // first road tile -> move next
                                    moveCell = nextCellData;
                                    road.Add(moveCell);
                                }

                            } else if (nextCellData.cell_CellType == Constants.Map.CellType.Road_End) {
                                // break on road end
                                road.Add(nextCellData);
                                blocker = true;
                                break;
                            }

                        }

                        // it no road tile found -> blocker
                        if (roads == 0)
                            blocker = true;
                    }
                }

                return pathList;
            });

            roadCalculators.Add(o);
        }


        var mapRef      = _currentMap;
        _pathCalcTask   = Observable. WhenAll(roadCalculators).
                    ThrottleFrame(1, FrameCountType.Update).
                    ObserveOnMainThread().
                    Subscribe((startSplits) => {
                        var answer = startSplits;
                        foreach (var roads in startSplits) {
                            foreach (var road in roads) {
                                if (road.Count == 0) // ???
                                    continue;

                                if (road[road.Count - 1].cell_CellType != Constants.Map.CellType.Road_End)
                                    continue;

                                if (!_finishCells.Contains(road[road.Count - 1]))
                                    _finishCells.Add(road[road.Count - 1]);

                                _roadPaths.Add(road);
                                _roadPaths_f3.Add(road.Select(x => new float3(x.cell_WorldPosition.x, x.cell_WorldPosition.y, x.cell_WorldPosition.z)).ToList());
                            }
                        }

                        _roadsCalculated = true;
                        GameCore.System.Events.mTrigger(new GameEvents.Battle.Map.RoadsReady { map = mapRef });

                        if (_pathCalcTask != null) {
                            _pathCalcTask.Dispose();
                            _pathCalcTask = null;
                        }
                    });
    }

}


