﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleObjectsPool : ASingleton<BattleObjectsPool>, IObjectPoolManager {
    private Dictionary<string, Queue<BaseObject>>       pool;

    private GameObject                                  poolObjectHolder;
    private Transform                                   poolObjectHolder_transform;

    public BattleObjectsPool() {
        pool_mClear();
        pool_mReset(); 
    }

    public void         pool_mClear  () {
        if (pool != null) {
            foreach (var list in pool) {
                foreach (var item in list.Value) {
                    GameObject.Destroy(item);
                }
            }
            pool.Clear();
        }

        pool = null;
        poolObjectHolder_transform = null;

        if (poolObjectHolder != null)
            GameObject.Destroy(poolObjectHolder);
    }

    public void         pool_mReset  () {
        pool                        = new Dictionary<string, Queue<BaseObject>>();
        poolObjectHolder            = new GameObject("PoolObjectHolder");
        poolObjectHolder_transform  = poolObjectHolder.transform;
    }

    private GameObject  GetOrCreateObject               (GameObject unityObject, Transform parentTransform = null) {
        if (unityObject == null) {
            throw new MissingComponentException("Object tamplete is null");
        }

        string objectName = unityObject.name;

        if (pool.ContainsKey(objectName) && pool[objectName].Count > 0) {
            var result = pool[objectName].Dequeue();
            result.gameObject.SetActive(true);

            return result.gameObject;
        }

        var resultObject    = GameObject.Instantiate(unityObject, parentTransform == null ? poolObjectHolder_transform : parentTransform);
        resultObject.name   = unityObject.name;
        resultObject.SetActive(true);

        return resultObject;
    }

    public BaseObject   mGetOrCreateObject         (GameObject unityObject, Transform parentTransform = null) {
        var gObject         = GetOrCreateObject(unityObject, parentTransform);
        var baseObject      = gObject.GetComponent<BaseObject>();

        return baseObject;
    }
    public T            mGetOrCreateObject<T>      (GameObject unityObject, Transform parentTransform = null) {
        var gObject         = GetOrCreateObject(unityObject, parentTransform);
        var baseObject      = gObject.GetComponent<T>();

        return baseObject;
    }

    public void         mStore                     (BaseObject unityObject) {
        if (unityObject == null)
            return;

        string objectName = unityObject.name;
        if (!pool.ContainsKey(objectName))
            pool.Add(objectName, new Queue<BaseObject>());

        unityObject.transform.SetParent(poolObjectHolder_transform, false);

        unityObject.gameObject.SetActive(false);

        if (!pool[objectName].Contains(unityObject))
            pool[objectName].Enqueue(unityObject);
    }


}
