﻿using System.Collections.Generic;
using Constants.Bullets;
using Constants.Enemies;
using Unity.Mathematics;
using UnityEngine;

public class ECSParticlesManager : ASingletonMono<ECSParticlesManager>, IECSParticlesManager {
    private Dictionary<BType, ParticlesBulletsController>   _bulletControllers              = new Dictionary<BType, ParticlesBulletsController>();
    private Dictionary<EType, ParticleCommonController>     _enemyExplosionsControllers     = new Dictionary<EType, ParticleCommonController>();


    private void    InitBulletControllerByType          (BType bulletType) {
        var data        = GameCore.Configs.Assets.mGetBulletByType(bulletType);
        var controller  = GameCore.Battle.Pool.mGetOrCreateObject<ParticlesBulletsController>(data.bullet_View);

        controller.mInit();

        _bulletControllers.Add(bulletType, controller);
    }

    private void    InitEnemyExplosionControllerByType  (EType enemyType) {
        var data        = GameCore.Configs.Battle.mGetEnemyDestroyVFX(enemyType);
        var controller  = GameCore.Battle.Pool.mGetOrCreateObject<ParticleCommonController>(data);

        controller.mInit();

        _enemyExplosionsControllers.Add(enemyType, controller);
    }
    
    public void     mRegisterBullet                     (BType bulletType, int ID, float3 start, float3 velocity, float lifeTime) {
        if (!_bulletControllers.ContainsKey(bulletType))
            InitBulletControllerByType(bulletType);

        var controller = _bulletControllers[bulletType];
        controller.mEmitBullet(ID, start, velocity, lifeTime);
    }

    public void     mRegisterEnemyExplosion             (EType enemyType, float3 position) {
        if (!_enemyExplosionsControllers.ContainsKey(enemyType))
            InitEnemyExplosionControllerByType(enemyType);

        var controller = _enemyExplosionsControllers[enemyType];
        controller.mEmitParticle(position);
    }
}


