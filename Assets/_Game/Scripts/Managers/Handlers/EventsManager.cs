﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using GameEvents;

public class EventsManager : ASingleton<EventsManager>, IEventsManager {
    private Dictionary<Type, IList>     events = new Dictionary<Type, IList>();

    public  void    mSubscribe<T>         (Action<T> callback, bool clearIfExist = true) {
        var type = typeof(T);
        if (!events.ContainsKey(type))
            events.Add(type, new List<Action<T>>(16));

        if (clearIfExist)
            mUnsubscribe(callback);

        var list    = events[type] as List<Action<T>>;
        var found   = false;
        for (int i = 0; i < list.Count; i++) {
            if (list[i] == null) {
                list[i] = callback;
                found = true;
                break;
            }
        }

        if (!found)
            list.Add(callback);
    }
    public  void    mUnsubscribe<T>       (Action<T> callback) {
        var type = typeof(T);
        if (events.ContainsKey(type)) {
            var list    = events[type] as List<Action<T>>;
            var index   = list.IndexOf(callback);
            if (index != -1)
                list[index] = null;
        }
    }
    public  void    mTrigger<T>           (T e) where T : IEventInfo {
        var type    = typeof(T);
        if (events.ContainsKey(type)) {
            var list    = events[type] as List<Action<T>>;
            var count   = list.Count;
            for (int i = 0; i < count; i++) {
                list[i]?.Invoke(e);
            }
        }
    }

    public  void    mClearEventsOf<T>     () {
        var type = typeof(T);
        if (events.ContainsKey(type)) {
            var list = events[type] as List<Action<T>>;
            list.Clear();
        }
    }
    public  void    mClearAllEvents       () {
        foreach (var list in events) {
            list.Value.Clear();
        }
    }
    public  void    mClearAllEventsBut    (params Type[] except) {
        foreach (var list in events) {
            var type    = list.Key;
            var events  = list.Value;

            if (!except.Contains(type))
                events.Clear();
        }

    }

}
