﻿using UnityEngine;
using Unity.Entities;
using System.Collections.Generic;

public class GameECSWorldManager : ASingleton<GameECSWorldManager>, IECSWorldManager {
    private World                           world;
    private GameObjectConversionSettings    conversionSettings;
    private EntityManager                   em;

    private Dictionary<GameObject, Entity>  prefabMap;
    
    public              GameECSWorldManager     () {
        world               = World.DefaultGameObjectInjectionWorld;
        em                  = world.EntityManager;
        prefabMap           = new Dictionary<GameObject, Entity>();

        conversionSettings  = GameObjectConversionSettings.FromWorld(world, null);
    }


    private Entity      CacheGameObjectAsEntity         (GameObject prefab, string entityName = null) {
        var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, conversionSettings);
        em.SetEnabled(entity, false);
        prefabMap.Add(prefab, entity);
#if UNITY_EDITOR
        if (!string.IsNullOrEmpty(entityName))
            em.SetName(entity, entityName);
#endif
        return entity;
    }


    public Entity       mCacheGameObject            (GameObject prefab, string entityName = null) {
        if (!prefabMap.ContainsKey(prefab))
            return CacheGameObjectAsEntity(prefab, entityName);
        else
            return prefabMap[prefab];
    }
    public Entity       mGetFromGameObject          (GameObject prefab, string entityName = null) {
        if (!prefabMap.ContainsKey(prefab))
            CacheGameObjectAsEntity(prefab, entityName);

        var reference   = prefabMap[prefab];
        var result      = em.Instantiate(reference);
        em.SetEnabled(result, true);

#if UNITY_EDITOR
        if (!string.IsNullOrEmpty(entityName))
            em.SetName(result, entityName);
#endif

        return result;
    }
    public Entity       mGetEmptyEntity             (string entityName = null) {
        var entity = em.CreateEntity();

#if UNITY_EDITOR
        if (!string.IsNullOrEmpty(entityName))
            em.SetName(entity, entityName);
#endif

        return entity;
    }

    public void         mAddEntityData<T>           (Entity entity, T data) where T : struct, IComponentData {
        em.AddComponentData(entity, data);
    }
    public void         mAddSharedEntityData<T>     (Entity entity, T data) where T : struct, ISharedComponentData {
        em.AddSharedComponentData(entity, data);
    }
    public void         mSetEntityData<T>           (Entity entity, T data) where T : struct, IComponentData {
        em.SetComponentData(entity, data);
    }
    public void         mSetSharedEntityData<T>     (Entity entity, T data) where T : struct, ISharedComponentData {
        em.SetSharedComponentData(entity, data);
    }
    public T            mGetEntityData<T>           (Entity entity) where T : struct, IComponentData {
        return em.GetComponentData<T>(entity);
    }
    public T            mGetSharedEntityData<T>     (Entity entity) where T : struct, ISharedComponentData {
        return em.GetSharedComponentData<T>(entity);
    }

    public int          mCalcEntitiesWith<T>        () {
        var q = em.CreateEntityQuery(ComponentType.ReadOnly<T>());
        return q.CalculateEntityCount();
        
    }

}


