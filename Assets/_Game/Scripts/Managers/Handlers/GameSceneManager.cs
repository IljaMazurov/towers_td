﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Constants.System;

public class GameSceneManager : ASingleton<GameSceneManager>, ISceneManager {
    private readonly Dictionary<GameScenes, string> _sceneMap = new Dictionary<GameScenes, string> {
        { GameScenes.Loader_Main,               "Loader_Main"                   },

        { GameScenes.Menu_Main,                 "Menu_Main"                     },

        { GameScenes.Battle_Main,               "Battle_Main"                   },
        { GameScenes.Battle_UI_Overlay,         "Battle_UI_Overlay"             },
        { GameScenes.Battle_UI_Windows,         "Battle_UI_Windows"             },

        { GameScenes.Camera_Vignette,           "Camera_Vignette"               },
        { GameScenes.Camera_PostFx,             "Camera_PostFX"                 },
    };

    private string              GetSceneName            (GameScenes scene) {
        if (!_sceneMap.ContainsKey(scene))
            throw new MissingReferenceException($"Scene [{scene.ToString()}] was not found");

        return _sceneMap[scene];
    }

    public void                 mLoad             (GameScenes scene, bool additive = false) {
        if (!_sceneMap.ContainsKey(scene))
            throw new MissingReferenceException($"Scene [{scene.ToString()}] was not found");

        var sceneName = GetSceneName(scene);
        SceneManager.LoadScene(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
    }
    public AsyncOperation       mLoadAsync        (GameScenes scene, bool additive = false) {
        if (!_sceneMap.ContainsKey(scene))
            throw new MissingReferenceException($"Scene [{scene.ToString()}] was not found");

        var sceneName   = GetSceneName(scene);
        var awaiter     = SceneManager.LoadSceneAsync(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);

        return awaiter;
    }
}


