﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Constants.System;
using System;

public class GameUpdateManager : ASingletonMonoConcrete<GameUpdateManager>, IUpdateManager {
    private List<Action<float>> _unityUnscaledUpdateListeners   = new List<Action<float>>(64);
    private List<Action<float>> _unityUpdateListeners           = new List<Action<float>>(64);
    private List<Action<float>> _unityLateUpdateListeners       = new List<Action<float>>(64);

    private float   TimeScale = 1f;

    private void    Update                              () {
        var deltaTime   = Time.deltaTime;
        var updateCount = _unityUnscaledUpdateListeners.Count;
        for (int i = 0; i < updateCount; i++) {
            _unityUnscaledUpdateListeners[i]?.Invoke(deltaTime);
        }

        var scaledDeltaTime   = Time.deltaTime * TimeScale;
        var scaledUpdateCount = _unityUpdateListeners.Count;
        for (int i = 0; i < scaledUpdateCount; i++) {
            _unityUpdateListeners[i]?.Invoke(scaledDeltaTime);
        }


    }
    private void    LateUpdate                          () {
        var scaledDeltaTime     = Time.deltaTime * TimeScale;
        var updateCount         = _unityLateUpdateListeners.Count;

        for (int i = 0; i < updateCount; i++) {
            _unityLateUpdateListeners[i]?.Invoke(scaledDeltaTime);
        }
    }

    private void    SubscribeTo                         (Action<float> updateCallback, List<Action<float>> holder) {
        var updateLen       = holder.Count;
        for (int i = 0; i < updateLen; i++) {
            if (holder[i] == null) {
                holder[i] = updateCallback;
                return;
            }
        }

        holder.Add(updateCallback);
    }
    private void    UnsubscribeFrom                     (Action<float> updateCallback, List<Action<float>> holder) {
        var holderLen = holder.Count;
        for (int i = 0; i < holderLen; i++) {
            if (updateCallback == holder[i])
                holder[i] = null;
        }
    }


    public void     mSubscribe                   (Action<float> updateCallback, bool unsubscribeIfExist = false) {
        if (unsubscribeIfExist)
            UnsubscribeFrom(updateCallback, _unityUpdateListeners);

        SubscribeTo(updateCallback, _unityUpdateListeners);
    }
    public void     mUnsubscribe                 (Action<float> updateCallback) {
        UnsubscribeFrom(updateCallback, _unityUpdateListeners);
    }

    public void     mSubscribe_Late              (Action<float> updateCallback, bool unsubscribeIfExist = false) {
        if (unsubscribeIfExist)
            UnsubscribeFrom(updateCallback, _unityLateUpdateListeners);

        SubscribeTo(updateCallback, _unityLateUpdateListeners);
    }
    public void     mUnsubscribe_Late            (Action<float> updateCallback) {
        UnsubscribeFrom(updateCallback, _unityLateUpdateListeners);
    }


    public void     mSubscribe_Unscaled          (Action<float> updateCallback, bool unsubscribeIfExist = false) {
        if (unsubscribeIfExist)
            UnsubscribeFrom(updateCallback, _unityUnscaledUpdateListeners);

        SubscribeTo(updateCallback, _unityUnscaledUpdateListeners);
    }
    public void     mUnsubscribe_Unscaled        (Action<float> updateCallback) {
        UnsubscribeFrom(updateCallback, _unityUnscaledUpdateListeners);
    }
}


