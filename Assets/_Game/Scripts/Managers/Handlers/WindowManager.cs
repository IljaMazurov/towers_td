﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowManager : ASingletonMono<WindowManager>, IWindowManager {
    private Transform _windowsHolder;

    private Dictionary<System.Type, AInteractableUI>    _windowsDB = new Dictionary<System.Type, AInteractableUI>();
    private Stack<AInteractableUI>                      _activeWindows = new Stack<AInteractableUI>();

    private void Awake                  () {
        _windowsHolder = transform;
        InitReferences(_windowsHolder,  _windowsDB);

        GameCore.System.Events.mSubscribe<GameEvents.Windows.RequestWindow> (WindowRequestHandler,  true);
        GameCore.System.Events.mSubscribe<GameEvents.Windows.OnWindowClosed>(WindowClosingHandler,  true);
    }
    
    private void InitReferences         (Transform holder, Dictionary<System.Type, AInteractableUI> db) {
        foreach (var item in holder) {
            var child   = item as Transform;
            var wnd     = child.GetComponent<AInteractableUI>();

            if (wnd != null) {
                wnd.gameObject.SetActive(false);
                db.Add(wnd.GetType(), wnd);
            } else {
                Destroy(child.gameObject);
            }
        }
    }

    private void WindowRequestHandler   (GameEvents.Windows.RequestWindow e) {
        if (!_windowsDB.ContainsKey(e.windowType)) {
            Dbg.Log($"UI :: Cannop find window [{e.windowType.FullName}]", Color.red);
            return;
        }

        var wndRef = _windowsDB[e.windowType];
        if (_activeWindows.Contains(wndRef)) {
            Dbg.Log($"UI :: Window [{e.windowType.FullName}] is already opened", Color.red);
            return;
        }

        wndRef.Show(e.windowData);
        wndRef.obj_transform.SetAsLastSibling();

        _activeWindows.Push(wndRef);
    }
    private void WindowClosingHandler   (GameEvents.Windows.OnWindowClosed e) {
        if (!_windowsDB.ContainsKey(e.windowType)) {
            Dbg.Log($"UI :: Cannop find window [{e.windowType.FullName}]", Color.red);
            return;
        }

        var wndRef = _windowsDB[e.windowType];
        if (!_activeWindows.Contains(wndRef)) {
            Dbg.Log($"UI :: Window [{e.windowType.FullName}] is already closed", Color.red);
            return;
        }


        var topWindow = _activeWindows.Peek();
        if (wndRef != topWindow) {
            Dbg.Log($"UI :: Cannot close [{e.windowType.FullName}], last window is {topWindow.GetType().FullName}", Color.red);
            return;
        }

        _activeWindows.Pop();
    }

}
