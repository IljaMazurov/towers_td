﻿using Constants.Materials;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using UnityEngine;

using BlobNav       = Unity.Entities.BlobAssetReference<Blob_EnemyNavigation>;
using BlobPathList  = System.Collections.Generic.List<Unity.Entities.BlobAssetReference<Blob_EnemyNavigation>>;

using BlobTowerMap  = Unity.Entities.BlobAssetReference<Blob_MapFloor>;

public interface IBattleDataManager {
    void                                    mSaveMaterial              (MaterialTypeIndex type, Material mat);
    Material                                mGetMaterial               (MaterialTypeIndex type);


    void                                    mSaveTowerPrefab           (int level, GameObject prefab);
    GameObject                              mGetTowerPrefab            (int level);

    void                                    mAddEnemyRoad              (BlobNav road);
    BlobPathList                            mGetEnemyRoads             ();

    void                                    mSaveTowersMapRef          (BlobTowerMap map);
    BlobTowerMap                            mGetTowersMapRef           ();

    NativeHashMap<int, EnemyPositionsCache> mGetEnemyPositionsCacheRef ();  
}
