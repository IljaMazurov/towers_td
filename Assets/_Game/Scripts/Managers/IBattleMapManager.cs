﻿using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public interface IBattleMapManager {
    bool                    map_IsMapinited         { get; }
    bool                    map_AreBlocksSpawned    { get; }
    ABattleMap              map_ActiveMapData       { get; }
    List<List<AMapCell>>    map_ActiveMapRoads      { get; }
    List<List<float3>>      map_ActiveMapRoads_f3   { get; }
        
    List<AMapCell>          map_StartCells          { get; }
    List<AMapCell>          map_FinishCells         { get; }

    void                    mLoad                   (ABattleMap map);
}
