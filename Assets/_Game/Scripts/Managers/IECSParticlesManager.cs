﻿using Unity.Mathematics;
using Constants.Bullets;
using Constants.Enemies;

public interface IECSParticlesManager {
    void mRegisterBullet            (BType bulletType, int ID, float3 start, float3 velocity, float lifeTime);
    void mRegisterEnemyExplosion    (EType enemyType, float3 position);
}
