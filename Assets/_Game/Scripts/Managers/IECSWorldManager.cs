﻿using Unity.Entities;
using UnityEngine;

public interface IECSWorldManager {
    Entity      mCacheGameObject        (GameObject prefab, string entityName = null);
    Entity      mGetFromGameObject      (GameObject prefab, string entityName = null);
    Entity      mGetEmptyEntity         (string entityName = null);

    void        mAddEntityData <T>      (Entity entity, T data) where T : struct, IComponentData;
    void        mAddSharedEntityData <T>(Entity entity, T data) where T : struct, ISharedComponentData;

    void        mSetEntityData <T>      (Entity entity, T data) where T : struct, IComponentData;
    void        mSetSharedEntityData<T> (Entity entity, T data) where T : struct, ISharedComponentData;

    T           mGetEntityData<T>       (Entity entity) where T : struct, IComponentData ;
    T           mGetSharedEntityData<T> (Entity entity) where T : struct, ISharedComponentData;

    int         mCalcEntitiesWith<T>    ();
}
