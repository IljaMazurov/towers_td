﻿using System;
using GameEvents;

public interface IEventsManager {
    void    mSubscribe<T>        (Action<T> callback, bool clearIfExist = true);
    void    mUnsubscribe<T>      (Action<T> callback);
    void    mTrigger<T>          (T e) where T : IEventInfo;

    void    mClearEventsOf<T>    ();
    void    mClearAllEvents      ();
    void    mClearAllEventsBut   (params Type[] except);
}
