﻿using UnityEngine;

public interface IObjectPoolManager {
    BaseObject      mGetOrCreateObject   (GameObject unityObject, Transform parentTransform = null);
    T               mGetOrCreateObject<T>(GameObject unityObject, Transform parentTransform = null);
    void            mStore               (BaseObject unityObject);

}
