﻿using Constants.System;
using UnityEngine;

public interface ISceneManager {
    void                mLoad         (GameScenes scene, bool additive = false);
    AsyncOperation      mLoadAsync    (GameScenes scene, bool additive = false);
}
