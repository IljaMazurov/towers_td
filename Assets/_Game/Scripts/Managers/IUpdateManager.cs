﻿using System;

public interface IUpdateManager {
    void mSubscribe                 (Action<float> updateCallback, bool unsubscribeIfExist = false);
    void mUnsubscribe               (Action<float> updateCallback);

    void mSubscribe_Late            (Action<float> updateCallback, bool unsubscribeIfExist = false);
    void mUnsubscribe_Late          (Action<float> updateCallback);

    void mSubscribe_Unscaled        (Action<float> updateCallback, bool unsubscribeIfExist = false);
    void mUnsubscribe_Unscaled      (Action<float> updateCallback);
}
