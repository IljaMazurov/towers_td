﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YieldsUtil : ASingleton<YieldsUtil> {
    private Dictionary<float, WaitForSeconds>   yields          = new Dictionary<float, WaitForSeconds>();
    public WaitForSeconds       mWait     (float seconds) {
        if (yields.ContainsKey(seconds))
            return yields[seconds];

        var waiter = new WaitForSeconds(seconds);
        yields.Add(seconds, waiter);
        return waiter;
    }
}
