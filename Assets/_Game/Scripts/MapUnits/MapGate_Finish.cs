﻿using UniRx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGate_Finish : MeshObject, IInitable {
    [SerializeField] private ParticleSystem     _onDamageVfx;
    [SerializeField] private ParticleSystem     _constantPortalVfx;
    [Space]
    [SerializeField] private Material           _defaultMaterial;
    [SerializeField] private Material           _dissolveMaterial;
    [Space]
    [SerializeField] private float              _appearTime = 1f;

    public void obj_Init() {
        _onDamageVfx.exFullReset();
        _constantPortalVfx.exFullReset();

        obj_mChangeMaterial(_dissolveMaterial);
        obj_mRendererSetFloat(Constants.Shaders.DissolveAmount, 1f);

        MainThreadDispatcher.StartUpdateMicroCoroutine(SpawnSequence());

        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.BaseDamagged>(OnBaseDamagged);
    }

    private IEnumerator     SpawnSequence   () {
        obj_mChangeMaterial(_dissolveMaterial);

        var time = 0f;
        while (time < _appearTime) {
            var progress = time / _appearTime;

            obj_mRendererSetFloat(Constants.Shaders.DissolveAmount, 1f - progress);
            yield return null;
            time += Time.deltaTime;
        }

        obj_mRendererSetFloat(Constants.Shaders.DissolveAmount, 0);
        yield return null;

        _constantPortalVfx.exFullResetPlay();

        yield return null;
        obj_mChangeMaterial(_defaultMaterial);
    }

    private void OnBaseDamagged(GameEvents.Battle.Game.BaseDamagged e) {
        _onDamageVfx.Play();
    }
}
