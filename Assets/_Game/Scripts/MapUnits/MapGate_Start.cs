﻿using UniRx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGate_Start : MeshObject, IInitable {
    [SerializeField] private ParticleSystem     _onSpawnVfx;
    [SerializeField] private ParticleSystem     _constantPortalVfx;
    [Space]
    [SerializeField] private Material           _defaultMaterial;
    [SerializeField] private Material           _dissolveMaterial;
    [Space]
    [SerializeField] private float              _appearTime = 1f;

    public void             obj_Init        () {
        _onSpawnVfx.exFullReset();
        _constantPortalVfx.exFullReset();

        obj_mChangeMaterial(_dissolveMaterial);
        obj_mRendererSetFloat(Constants.Shaders.DissolveAmount, 1f);

        MainThreadDispatcher.StartUpdateMicroCoroutine(SpawnSequence());
    }


    private IEnumerator     SpawnSequence   () {
        obj_mChangeMaterial(_dissolveMaterial);

        var time = 0f;
        while (time < _appearTime) {
            var progress = time / _appearTime;

            obj_mRendererSetFloat(Constants.Shaders.DissolveAmount, 1f - progress);
            yield return null;
            time += Time.deltaTime;
        }

        obj_mRendererSetFloat(Constants.Shaders.DissolveAmount, 0);
        yield return null;

        _onSpawnVfx.exFullResetPlay();
        _constantPortalVfx.exFullResetPlay();

        yield return null;
        obj_mChangeMaterial(_defaultMaterial);
    }
}
