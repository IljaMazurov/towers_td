﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGameModule : ScriptableObject {
    public abstract void    module_Init  (System.Action OnComplete, System.Action OnProgress = null);

}
