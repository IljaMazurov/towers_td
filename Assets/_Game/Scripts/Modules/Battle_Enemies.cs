﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using System.Collections;
using Unity.Rendering;
using UniRx;
using Unity.Transforms;

[CreateAssetMenu(fileName = "Battle_Enemies", menuName = "GameData/Modules/Battle/Enemies")]
public class Battle_Enemies : AGameModule {
    [SerializeField] private GameObject             enemyPrefab;
    [SerializeField] private GameObject             enemyHealthbar;
    [SerializeField] private Material               enemyDefault;
    [SerializeField] private Material               enemyAtStart;
    [SerializeField] private Material               enemyAtFinish;

    private IBattleMapManager       _mapManager;
    private IBattleDataManager      _gameCache;


    public override void            module_Init         (System.Action OnComplete, System.Action OnProgress = null) {
        _mapManager     = GameCore.Battle.Map;
        _gameCache      = GameCore.Battle.CacheData;

        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.SendNextWave>    (OnStartEnemies, true);
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Map.BlocksSpawned>    (OnMapReady, true);

        OnComplete?.Invoke();
    }


    private void                    OnMapReady          (GameEvents.Battle.Map.BlocksSpawned e) {
        var ecs_world   = GameCore.ECS.World;

        ecs_world.mCacheGameObject(enemyPrefab,     "default_enemy");
        ecs_world.mCacheGameObject(enemyHealthbar,  "default_enemy_healthbar");

        enemyAtStart.SetVector(Constants.Shaders.Origin,    _mapManager.map_StartCells[0].cell_WorldPosition);
        enemyAtFinish.SetVector(Constants.Shaders.Origin,   _mapManager.map_FinishCells[0].cell_WorldPosition);

        _gameCache.mSaveMaterial(Constants.Materials.MaterialTypeIndex.Enemy_Default, enemyDefault);
        _gameCache.mSaveMaterial(Constants.Materials.MaterialTypeIndex.Enemy_Spawn, enemyAtStart);
        _gameCache.mSaveMaterial(Constants.Materials.MaterialTypeIndex.Enemy_Finish, enemyAtFinish);

        GeneratePathList();
    }

    private void                    OnStartEnemies      (GameEvents.Battle.Game.SendNextWave e) {
        MainThreadDispatcher.StartCoroutine(SpawnEnemies(250, .5f));
    }


    private void                    GeneratePathList    () {
        var roads       = _mapManager.map_ActiveMapRoads_f3;

        foreach (var road in roads) {
            using (BlobBuilder bb = new BlobBuilder(Allocator.Temp)) {
                ref var pathData    = ref bb.ConstructRoot<Blob_EnemyNavigation>();
                var stepsArray      = bb.Allocate(ref pathData.road, road.Count + 2);

                for (int i = 0; i < road.Count; i++)
                    stepsArray[i + 1] = road[i];
                
                // first step -> before spawn -> portal center
                stepsArray[0]           = stepsArray[1] + (stepsArray[1] - stepsArray[2]) * 0.25f;

                // last step -> after start
                stepsArray[road.Count]  = road[road.Count-1] + (road[road.Count-1] - road[road.Count-2]);

                var bbPathRef           = bb.CreateBlobAssetReference<Blob_EnemyNavigation>(Allocator.Persistent);
                _gameCache.mAddEnemyRoad(bbPathRef);
            }
        }


    }

    private IEnumerator             SpawnEnemies        (int count, float enemyDelay) {
        var ecs_world       = GameCore.ECS.World;
        var navRefList      = _gameCache.mGetEnemyRoads();
        var roads           = _mapManager.map_ActiveMapRoads_f3;

        var delay           = GameCore.Utils.Yields.mWait(enemyDelay);

        for (int i = 0; i < count; i++) {
            var index       = i % roads.Count;
            var road        = roads[index];
            var entity      = ecs_world.mGetFromGameObject(enemyPrefab, $"enemy_default_#{i}");
            var health      = ecs_world.mGetFromGameObject(enemyHealthbar, $"enemy_health_#{i}");

            var navData     = navRefList[i % navRefList.Count];
            var unitOffset  = Random.insideUnitCircle.exToV3() * 0.35f + new Vector3(0, .75f, 0);

            var navigationData = new Enemy_NavigationComponent {
                nav_PathData                = navData,

                f3CellPrevPosition        = road[0],
                f3CellCurrentPosition     = road[1],
                f3CellNextPosition        = road[2],

                iStepIndex               = -1,
                iTotalSteps              = navData.Value.road.Length - 1,

                fTimeToComplete          = 15f, 
                fCurrentRunTime          = 0f, 
                f3RandomMoveOffset        = unitOffset
            };
            var commonData      = new Enemy_CommonDataComponent {
                entityHealthBar    = health
            };
            var healthbarData   = new Enemy_HealthbarComponent {
                entityOwner     = entity
            };
            var healthPos       = new Translation {
                Value           = road[1]
            };

            ecs_world.mAddEntityData(entity, commonData);
            ecs_world.mSetEntityData(entity, navigationData);

            ecs_world.mSetEntityData(health, healthbarData);
            ecs_world.mSetEntityData(health, healthPos);

            yield return delay;
        }

    }

}
