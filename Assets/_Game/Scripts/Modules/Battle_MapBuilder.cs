﻿using System;
using System.Linq;
using UnityEngine;
using Unity.Transforms;
using Unity.Mathematics;
using System.Collections.Generic;

using Constants.Map;
using UniRx;
using Unity.Rendering;

[CreateAssetMenu(fileName = "Battle_MapBuilder", menuName = "GameData/Modules/Battle/MapBuilder")]
public class Battle_MapBuilder : AGameModule {
    [SerializeField] private GameObject             startBlock;
    [SerializeField] private GameObject             finishBlock;
    [SerializeField] private List<CellTypeDataSet>  mapBlocks;


    [SerializeField] private GameObject             mapRoadPreview;
    [SerializeField] private List<Color>            roadPreviewColors;

    private IBattleMapManager  _mapManager;
    private ABattleMap         _currentMap;

    private List<ParticlePath>  _pathViews;

    public override void            module_Init         (Action OnComplete, Action OnProgress = null) {
        _mapManager = GameCore.Battle.Map;

        CacheGameContent();

        GameCore.System.Events.mSubscribe<GameEvents.Battle.Map.LoadStart>                (OnNewMap, true);
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Map.BlocksSpawned>            (OnMapBlocksSpawned, true);
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.StartGame>   (OnEnemiesSpawn, true);

        if (_mapManager.map_IsMapinited)
            OnNewMap(new GameEvents.Battle.Map.LoadStart());

        OnComplete?.Invoke();
    }

    private void                    OnNewMap            (GameEvents.Battle.Map.LoadStart e) {
        if (_pathViews != null) {
            foreach (var path in _pathViews)
                GameCore.Battle.Pool.mStore(path);
            _pathViews.Clear();
        } else {
            _pathViews = new List<ParticlePath>();
        }

        _currentMap = _mapManager.map_ActiveMapData;
        BuildMap();
    }
    private void                    OnMapBlocksSpawned  (GameEvents.Battle.Map.BlocksSpawned e) {
        SpawnRoadHighlights();
        BuildStartGate  ();
        BuildFinishGate ();
    }
    private void                    OnEnemiesSpawn      (GameEvents.Battle.Game.StartGame e) {
        if (_pathViews == null)
            return;

        foreach (var path in _pathViews) {
            path.particles_Stop();
        }

        _pathViews.Clear();
    }

    private void                    CacheGameContent    () {
        var ecs = GameCore.ECS.World;
        foreach (var block in mapBlocks)
            ecs.mCacheGameObject(block.prefab, $"prefab_{block.type}");
    }


    private CellTypeDataSet         GetCellData         (CellType cell) {
        foreach (var block in mapBlocks)
            if (block.type == cell)
                return block;

        return mapBlocks[0];
    }
    private void                    BuildMap            () {
        var ecs_world       = GameCore.ECS.World;
        var spawnHeight     = -7f;

        foreach (var cell in _currentMap.map_Cells) {
            var data            = GetCellData(cell.cell_CellType);
            var cPos            = cell.cell_WorldPosition + data.prefabOffset;
            var cellSpawnDelay  = math.sqrt((cPos.x + 1) * (cPos.z + 1)) * .15f;

            var entity          = ecs_world.mGetFromGameObject(data.prefab, $"block_{cell.cell_CellType}_{cPos.x}x{cPos.z}");

            var translation     = new Translation                   { Value         = cPos.exAdd(0, spawnHeight, 0) };
            var rotation        = new Rotation                      { Value         = Quaternion.Euler(0, UnityEngine.Random.Range(0, 4) * 90f, 0) };
            var spawnerDelay    = new Block_SpawnerDelayComponent   { fDelayTimer    = cellSpawnDelay };
            var spawnerParams   = new Block_SpawnComponent {
                fStartHeight       = cPos.y + spawnHeight,
                fDefaultHeight     = cPos.y,
                fTotalTime         = 1.75f,
                fCurrentTime       = 0f,
            };

            ecs_world.mSetEntityData(entity, translation);
            ecs_world.mSetEntityData(entity, rotation);
            ecs_world.mAddEntityData(entity, spawnerDelay);
            ecs_world.mAddEntityData(entity, spawnerParams);
        }

    }
    private void                    SpawnRoadHighlights () {
        var colors = new List<Color>(roadPreviewColors);

        foreach (var road in _mapManager.map_ActiveMapRoads_f3) {
            var color       = colors.exRandomItem();
            var preview     = GameCore.Battle.Pool.mGetOrCreateObject<ParticlePath>(mapRoadPreview);
            preview.particles_Init(road, color, 1f);

            _pathViews.Add(preview);
        }
    }
    private void                    BuildStartGate      () {
        var pool    = GameCore.Battle.Pool;
        var cells   = _mapManager.map_ActiveMapData.map_Cells;

        foreach (var cell in _mapManager.map_StartCells) {
            var gateBase    = pool.mGetOrCreateObject<IInitable>(startBlock);
            var gateObj     = gateBase as BaseObject;

            var lookOffset  = cell.cell_WorldPosition;
            foreach (var nIdx in cell.cell_Neighbors) {
                if (nIdx == -1)
                    continue;

                if (cells[nIdx].cell_CellType == CellType.Road)
                    lookOffset += (cells[nIdx].cell_WorldPosition - cell.cell_WorldPosition);
            }

            gateObj.obj_position = cell.cell_WorldPosition;
            gateObj.obj_transform.LookAt(lookOffset);
            gateBase.obj_Init();
        }
    }
    private void                    BuildFinishGate     () {
        var pool    = GameCore.Battle.Pool;
        foreach (var cell in _mapManager.map_FinishCells) {
            var gateBase    = pool.mGetOrCreateObject<IInitable>(finishBlock);
            var gateObj     = gateBase as BaseObject;

            gateObj.obj_position = cell.cell_WorldPosition;
            gateBase.obj_Init();
        }
    }

}
