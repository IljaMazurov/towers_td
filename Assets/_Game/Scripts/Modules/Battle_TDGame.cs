﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using System.Collections;
using Unity.Rendering;
using UniRx;

[CreateAssetMenu(fileName = "Battle_TDGame", menuName = "GameData/Modules/Battle/TDGame")]
public class Battle_TDGame : AGameModule {
    [SerializeField, Range(0, 15)]
    private int                     _startSpawnDelay;
    private Coroutine               _waveSpawnerRoutine;

    private BattleGameData          _battleData;

    public override void            module_Init         (System.Action OnComplete, System.Action OnProgress = null) {
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.StartGame>       (OnGameStart, true);

        GameCore.System.Events.mSubscribe<GameEvents.Battle.Enemies.Destroyed>    (OnEnemyKilled, true);
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.BaseDamagged>    (OnBaseDamaged, true);

        _battleData = GameCore.Data.DynamicData.Battle;

        InitBattleDataValues();

        OnComplete?.Invoke();
    }

    private void                    InitBattleDataValues    () {
        _battleData.WavesInfo.iWaveCurrent.Value        = 0;
        _battleData.WavesInfo.iWavesTotal.Value         = 50;
        _battleData.WavesInfo.iNextWaveDelaySec.Value   = _startSpawnDelay;

        _battleData.BaseInfo.iHealthCurrent.Value       = 5;
        _battleData.BaseInfo.iHealthTotal.Value         = 5;
        _battleData.BaseInfo.iShieldCurrent.Value       = 4;
        _battleData.BaseInfo.iShieldTotal.Value         = 4;

        _battleData.ResourcesInfo.iCoins.Value          = 0;
        _battleData.ResourcesInfo.iGems.Value           = 0;
    }

    private void                    OnGameStart         (GameEvents.Battle.Game.StartGame e) {
        _waveSpawnerRoutine = MainThreadDispatcher.StartCoroutine(WaveSpawner(_startSpawnDelay));
    }

    private void                    OnEnemyKilled       (GameEvents.Battle.Enemies.Destroyed e) {
        _battleData.ResourcesInfo.iCoins.Value += 2;
    }
    
    private void                    OnBaseDamaged       (GameEvents.Battle.Game.BaseDamagged e) {
        var shield  = _battleData.BaseInfo.iShieldCurrent;
        var health  = _battleData.BaseInfo.iHealthCurrent;

        if (shield.Value > 0) {
            shield.Value--;
        } else if (health.Value > 0) {
            health.Value--;
        } else {
            // game over
        }

    }

    private IEnumerator             WaveSpawner         (int timer) {
        _battleData.WavesInfo.iWaveCurrent.Value++;

        var delay   = GameCore.Utils.Yields.mWait(1);
        var wTimer  = _battleData.WavesInfo.iNextWaveDelaySec;
        
        while (timer > 0) {
            wTimer.Value = timer;
            yield return delay;
            timer--;
        }

        wTimer.Value = timer;
        GameCore.System.Events.mTrigger(new GameEvents.Battle.Game.SendNextWave());
    }

}
