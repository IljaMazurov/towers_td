﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using System.Collections;
using System.Collections.Generic;
using UniRx;

[CreateAssetMenu(fileName = "Battle_TouchControlls", menuName = "GameData/Modules/Battle/TouchControlls")]
public class Battle_TouchControlls : AGameModule {
    [SerializeField] private GameObject mapSwipeQuad;

    private BaseObject _currentControllsQuad;

    public override void            module_Init         (System.Action OnComplete, System.Action OnProgress = null) {
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Map.RoadsReady>        (OnMapReady, true);

        OnComplete?.Invoke();
    }

    private void                    OnMapReady          (GameEvents.Battle.Map.RoadsReady e) {
        var swipeQuad       = Instantiate(mapSwipeQuad);
        var quadController  = swipeQuad.GetComponent<ModuleItem_TouchControlls>();

        quadController.name = "Battle Touch Controller";
        quadController.controlls_Init();

        _currentControllsQuad = quadController;
    }


}
