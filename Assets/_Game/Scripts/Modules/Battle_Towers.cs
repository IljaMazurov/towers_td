﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Unity.Transforms;
using Constants.Towers;

[CreateAssetMenu(fileName = "Battle_Towers", menuName = "GameData/Modules/Battle/Towers")]
public class Battle_Towers : AGameModule {
    [SerializeField] private GameObject             towerBase;
    [SerializeField] private GameObject             towerGun;

    [SerializeField] private Material               towerDefault;
    [SerializeField] private Material               towerSpawn;


    private Dictionary<Vector2Int, TowerLevelSet> _towers;

    private IBattleMapManager       _mapManager;
    private IBattleDataManager      _gameCache;
    private IECSWorldManager        _ecsManager;


    public override void            module_Init         (System.Action OnComplete, System.Action OnProgress = null) {
        _mapManager     = GameCore.Battle.Map;
        _gameCache      = GameCore.Battle.CacheData;
        _ecsManager     = GameCore.ECS.World;

        _towers         = new Dictionary<Vector2Int, TowerLevelSet>();

        GameCore.System.Events.mSubscribe<GameEvents.Battle.Map.BlocksSpawned>(OnMapReady, true);
        OnComplete?.Invoke();
    }

    private void                    OnMapReady          (GameEvents.Battle.Map.BlocksSpawned e) {
        _ecsManager.mCacheGameObject(towerBase,   "default_base");
        _ecsManager.mCacheGameObject(towerGun,    "default_gun");

        _gameCache.mSaveMaterial(Constants.Materials.MaterialTypeIndex.Tower_Default,  towerDefault);
        _gameCache.mSaveMaterial(Constants.Materials.MaterialTypeIndex.Tower_Spawn,    towerSpawn);


        GameCore.System.Events.mSubscribe<GameEvents.Battle.TouchControlls.OnTouchClick>(OnMapClicked, true);


        Test_SpawnTowers();
    }

    private void                    Test_SpawnTowers    () {
        var randomCell = _mapManager.map_ActiveMapData.map_Cells.
                                        Where(x => x.cell_CellType == Constants.Map.CellType.Ground).
                                        ToList();

        for (int i = 0; i < 2; i++) {
            var cell = randomCell.exRandomItemAndRemove();
            SpawnTower(cell.cell_WorldPosition.exToV2xz().exToInt(), TType.Laser);
        }
    }

    private void                    OnMapClicked        (GameEvents.Battle.TouchControlls.OnTouchClick e) {
        var cell        = e.touchSpot.exToV2xz().exToInt();
        var mapData     = _mapManager.map_ActiveMapData.map_Cells.FirstOrDefault(x => x.cell_WorldPosition.exToV2xz().exToInt() == cell);

        if (!_towers.ContainsKey(cell) && mapData.cell_CellType == Constants.Map.CellType.Ground)
            SpawnTower(cell, TType.Laser);
    }

    private void                    SpawnTower          (Vector2Int cell, TType type) {
        _towers.Add(cell, new TowerLevelSet { 
            type    = type,
            level   = 1
        });

        var position    = new Vector3(cell.x, 0.25f, cell.y);
        var tower_base  = _ecsManager.mGetFromGameObject(towerBase, "tower_base");
        var tower_gun   = _ecsManager.mGetFromGameObject(towerGun,  "tower_gun");

        var common_SpawnData = new Tower_SpawnComponent { 
            fTimeCurrent   = 0,
            fTimeTotal     = 0.75f
        };

        var tower_BaseData = new Tower_BaseComponent {
            iLevel          = 1,
            entityGun       = tower_gun
        };
        var tower_PositionData = new Translation { 
            Value = position 
        };

        var gun_PositionData = new Translation { 
            Value = position + new Vector3(0, 1f, 0)
        };

        _ecsManager.mAddEntityData(tower_base,  common_SpawnData);
        _ecsManager.mSetEntityData(tower_base,  tower_BaseData);
        _ecsManager.mSetEntityData(tower_base,  tower_PositionData);

        _ecsManager.mAddEntityData(tower_gun,   common_SpawnData);
        _ecsManager.mSetEntityData(tower_gun,   gun_PositionData);
    }

}
