﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleItem_BgVignette : MonoBehaviour {
    [SerializeField] private Image      screenBackground;
    [Space]
    [SerializeField] private Material   backgroundGeneratorMaterial;
    [Space]
    [SerializeField] private float      screenSizeDivider   = 5f;
    [SerializeField] private Color      centerColor         = Color.white;
    [SerializeField] private Color      borderColor         = Color.black;

    private         TextureMaker        generator;

    private void    OnValidate          () {
        if (screenBackground == null)
            screenBackground = GetComponent<Image>();

        GenerateTexture();
    }

    private void    Start               () {
        GenerateTexture();
    }

    private void    GenerateTexture     () {
        if (backgroundGeneratorMaterial == null) {
            Dbg.Log("ERROR :: VignetteTextureGenerator :: [backgroundGeneratorMaterial] is not set");
            return;
        }

        if (screenBackground == null) {
            Dbg.Log("ERROR :: VignetteTextureGenerator :: [screenBackground] is not set");
            return;
        }

        if (generator == null)
            generator = new TextureMaker();

        backgroundGeneratorMaterial.SetColor(Constants.Shaders.ColorCenter,     centerColor);
        backgroundGeneratorMaterial.SetColor(Constants.Shaders.ColorBorders,    borderColor);

        generator.  Setup       ((int)(Screen.width / screenSizeDivider), (int)(Screen.height / screenSizeDivider), Color.clear).
                    SetMaterial (backgroundGeneratorMaterial).
                    Begin       ().
                    UsePostFX   ().
                    End         ();

        var texture             = generator.Result;
        screenBackground.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }
}
