﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class ModuleItem_PostFX_Bloom : Graphic {
	[SerializeField, Range(1, 256)] private int			ScreenWidthCells			= 1;
	[SerializeField, Range(1, 256)] private int			ScreenHeightCells			= 1;
	[Space]
	[SerializeField, Range(1, 8)]	private int			RT_TextureScale				= 1;
	[SerializeField, Range(1, 8)]	private int			RT_BlurPass					= 1;
	[Space]
	[SerializeField]				private Shader		CameraSnap_ReplaceShader;
	[SerializeField]				private Material	CameraSnap_BlurMaterial;

	private RenderTexture	_rt_a;
	private RenderTexture	_rt_b;


	private RenderTexture	_nextRt {
		get {
			_active = _active == _rt_a ? _rt_b : _rt_a;
			return _active;
		}
	}
	public  RenderTexture	_active;

	public override Texture mainTexture  { get { return _active; } }

	private RectTransform	_ct;
	public	RectTransform	cTransform  {
        get { if (_ct == null) _ct = transform as RectTransform; return _ct; }
    }

	private Camera			_mainCamera;
	private Camera			cCamera => _mainCamera == null ? Camera.main : _mainCamera;

	private TextureMaker	_tm;
	private float			_screenResScale;

	private void			ResetMesh		() {
		System.IDisposable dispose = null;
		dispose = Observable.	NextFrame().
								Subscribe( x=> {
									SetAllDirty();
									Rebuild(CanvasUpdate.Layout);
									dispose.Dispose();
								});
	}
	protected override void OnEnable		() {
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		var scr			= new Vector2Int(Screen.width, Screen.height);
		var refScr		= new Vector2Int(1080, 2160);
		_screenResScale	= scr.magnitude / refScr.magnitude;

		var sx = scr.x / RT_TextureScale;
		var sy = scr.y / RT_TextureScale;

		_mainCamera		= GameCore.Data.DynamicData.System.camera_Main.Value;

		_rt_a			= RenderTexture.GetTemporary(sx, sy, 16);
		_rt_b			= RenderTexture.GetTemporary(sx, sy, 16);

		GameCore.System.Updater.mSubscribe_Late(onGameUpdate, true);

		_tm = new TextureMaker();
		_tm.Setup(sx, sy, Color.black, 1, 1);
		_tm.SetMaterial(CameraSnap_BlurMaterial);
		_tm.SetCamera(_mainCamera);

		ResetMesh();
	}
	
    protected override void OnDisable		() {
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		GameCore.System.Updater.mUnsubscribe_Late(onGameUpdate);
    }

	private void			onGameUpdate	(float dt) {
		var oldLayers	= cCamera.cullingMask;
		var targetMask	= 1 << Constants.Layers.PostFX_Bloom;

		cCamera.cullingMask		= targetMask;
		cCamera.targetTexture	= _nextRt;
		cCamera.Render();
		cCamera.targetTexture	= null;
		cCamera.cullingMask		= oldLayers;

        for (int i = 0; i < RT_BlurPass; i++) {
			_tm.Begin(_nextRt, i == 0);
			var offset = (i + 1) * _screenResScale;
			
			CameraSnap_BlurMaterial.SetTexture	(Constants.Shaders.MainTex, _nextRt);
			CameraSnap_BlurMaterial.SetFloat	(Constants.Shaders.TSize,	offset);
			CameraSnap_BlurMaterial.SetFloat	(Constants.Shaders.TPower, _screenResScale);
			_tm.UsePostFX();
			_tm.SoftEnd();

			var aa = _nextRt;
		}

		if (RT_BlurPass % 2 == 0) {
			var bb = _nextRt;
		}
	}

    protected override void OnPopulateMesh	(VertexHelper vh) {
		float minX	= (0f - cTransform.pivot.x) * cTransform.rect.width;
		float minY	= (0f - cTransform.pivot.y) * cTransform.rect.height;
		float maxX	= (1f - cTransform.pivot.x) * cTransform.rect.width;
		float maxY	= (1f - cTransform.pivot.y) * cTransform.rect.height;

		vh.Clear();
		var _quads		= ScreenWidthCells * ScreenHeightCells * 4;

		var _width		= minX.Abs() + maxX.Abs();
		var _height		= minY.Abs() + maxY.Abs();
		var _width_x	= _width / ScreenWidthCells;
		var _height_x	= _height / ScreenHeightCells;

		int x = 0;
		int y = 0;
		for (int i = 0; i < _quads; i+=4) {
			var x1 = minX + _width_x * (x + 0);
			var x2 = minX + _width_x * (x + 1);

			var y1 = minY + _height_x * (y + 0);
			var y2 = minY + _height_x * (y + 1);

			var centerQuad_x    = (x * _width_x / _width + (x + 1) * _width_x / _width) / 2f;
            var centerQuad_y    = (y * _height_x / _height + (y + 1) * _height_x / _height) / 2f;
			var sideDistance_x  = (x + 1) * _width_x / _width - centerQuad_x;
            var sideDistance_y  = (y + 1) * _height_x / _height - centerQuad_y;
			var side_mul		= .75f;
			vh.AddVert(new UIVertex {
				position	= new Vector3(x1, y1),
				uv0			= new Vector2(x * _width_x / _width, y * _height_x / _height),
				uv1			= new Vector2(centerQuad_x, centerQuad_y),
				uv2			= new Vector2(sideDistance_x * side_mul, sideDistance_y * side_mul)
			});

			vh.AddVert(new UIVertex {
				position	= new Vector3(x2, y1),
				uv0			= new Vector2((x + 1) * _width_x / _width, y * _height_x / _height),
				uv1			= new Vector2(centerQuad_x, centerQuad_y),
				uv2			= new Vector2(sideDistance_x * side_mul, sideDistance_y * side_mul)
			});

			vh.AddVert(new UIVertex {
				position	= new Vector3(x2, y2),
				uv0			= new Vector2((x + 1) * _width_x / _width, (y + 1) * _height_x / _height),
				uv1			= new Vector2(centerQuad_x, centerQuad_y),
				uv2			= new Vector2(sideDistance_x * side_mul, sideDistance_y * side_mul)
			});

			vh.AddVert(new UIVertex {
				position	= new Vector3(x1, y2),
				uv0			= new Vector2(x * _width_x / _width, (y + 1) * _height_x / _height),
				uv1			= new Vector2(centerQuad_x, centerQuad_y),
				uv2			= new Vector2(sideDistance_x * side_mul, sideDistance_y * side_mul)
			});

			vh.AddTriangle(i, i+2, i+1);
			vh.AddTriangle(i, i+3, i+2);


			x++;
			if (x >= ScreenWidthCells) {
				x = 0;
				y++;
            }
        }
	}

}
