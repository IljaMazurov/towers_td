﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleItem_TouchControlls : BaseObject {
    [SerializeField] private float      _clickDistanceDelta     = 0.1f;
    [SerializeField] private float      _clickTimeDelta         = 0.5f;
    [Space]
    [SerializeField] private BoxCollider _collider;

    private Camera      _cam     => GameCore.Data.DynamicData.System.camera_Main.Value;
    private Vector3     _mPos {
        get {
            var result  = new Vector3(); 
            var ray     = _cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
                result = hit.point;
            return result;
        }
    }
    private Vector3     _lastPos;
    private Vector3     _startPos;
    private Vector3     _startScrPos;

    private float       _touchTime;

    private void                OnValidate      () {
        _collider = GetComponent<BoxCollider>();
    }

    protected override void     obj_mOnAwake    () {
        base.obj_mOnAwake();

        if (_collider == null)
            _collider = GetComponent<BoxCollider>();
    }

    public void                 controlls_Init  () {
        var map         = GameCore.Battle.Map.map_ActiveMapData;
        var nSize       = new Vector3(map.map_Size, 0.25f, map.map_Size);
         obj_position   = new Vector3(nSize.x / 2f - 0.5f, 0, nSize.z / 2f - 0.5f);
        _collider.size  = nSize;
    }

#region MOUSE EVENTS
    private float GetScrDelta   (Vector2 start, Vector2 end) {
        var move = end - start;
        var perc = new Vector2(move.x / Screen.width, move.y / Screen.width);

        return perc.magnitude;
    }

    private void OnMouseDown    () {
        _startPos       = _mPos;
        _startScrPos    = Input.mousePosition;
        _lastPos        = _startPos;
        _touchTime      = Time.time;

        GameCore.System.Events.mTrigger(new GameEvents.Battle.TouchControlls.OnTouchDown {
            touchSpot = _startPos
        });

    }
    private void OnMouseUp      () {
        var pickTime    = Time.time;
        var clickDelta  = pickTime - _touchTime;

        if (_mPos.magnitude > 0.25f)
            _lastPos = _mPos;

        var moveVector  = _lastPos - _startPos;
        var direction   = (moveVector).normalized;

        GameCore.System.Events.mTrigger(new GameEvents.Battle.TouchControlls.OnTouchUp {
            touchSpot = _lastPos, 
            direction = direction
        });
        
        if (clickDelta < _clickTimeDelta) {
            var screen  = Input.mousePosition;
            var dst     = GetScrDelta(_startScrPos, screen);

            if (dst < _clickDistanceDelta) {
                GameCore.System.Events.mTrigger(new GameEvents.Battle.TouchControlls.OnTouchClick {
                    touchSpot = _lastPos
                });
            }
        }

    }
    private void OnMouseDrag    () {
        _lastPos = _mPos;
        var direction = (_lastPos - _startPos).normalized;

        GameCore.System.Events.mTrigger(new GameEvents.Battle.TouchControlls.OnTouchMove {
            touchSpot = _lastPos,
            direction = direction
        });
    }
#endregion

}
