﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Collections.Generic;

public class ModuleItem_UI_Controlls : BaseObject {
    [SerializeField] Button         startButton;
    [SerializeField] TextByLetter   startLabel;

    private float letterDelay   = 0.15f;
    private float letterSpawn   = 0.5f;

    protected override void obj_mOnAwake() {
        base.obj_mOnAwake();

        startLabel.gameObject.SetActive(true);
        startLabel.txt_mInit();
        startLabel.gameObject.SetActive(false);

        GameCore.System.Events.mSubscribe<GameEvents.Battle.Map.BlocksSpawned>(OnMapBlocksSpawned);
    }

    private void OnMapBlocksSpawned(GameEvents.Battle.Map.BlocksSpawned e) {
        startLabel.gameObject.SetActive(true);

        var nextDelay   = 0f;
        foreach (var letter in startLabel.txt_Letters) {
            letter.trans.localPosition = new Vector3(startLabel.txt_TextHolderSize + startLabel.txt_BiggestLetter, 0, 0);
            MainThreadDispatcher.StartCoroutine(ShowLetter(letter, nextDelay));
            nextDelay  += letterDelay;
        }

        System.IDisposable onSpawn = null;
        onSpawn = startButton.  OnClickAsObservable().
                                DelaySubscription(System.TimeSpan.FromSeconds((int)(nextDelay + letterSpawn))).
                                Subscribe(x => {
                                    StartGameRun();
                                    onSpawn?.Dispose();
                                });
    }

    private void StartGameRun() {
        var nextDelay = 0f;

        foreach (var letter in startLabel.txt_Letters) {
            MainThreadDispatcher.StartCoroutine(HideLetter(letter, nextDelay));
            nextDelay += letterDelay;
        }

        GameCore.System.Events.mTrigger(new GameEvents.Battle.Game.StartGame());
    }

    private IEnumerator ShowLetter  (LetterPosInfo letterInfo, float delay) {
        yield return GameCore.Utils.Yields.mWait(delay);

        var start       = letterInfo.trans.localPosition;
        var showTime    = letterSpawn;
        var time        = 0f;

        while (time < showTime) {
            var progress    = time / showTime;
            var eased       = Ease.Do(progress, EaseType.easeInOut_x2);

            letterInfo.letter.color         = new Color(1, 1, 1, progress);
            letterInfo.trans.localPosition  = Vector3.Lerp(start, letterInfo.target, eased);

            yield return null;
            time += Time.deltaTime;
        }

        letterInfo.trans.localPosition = letterInfo.target;
        letterInfo.letter.color = new Color(1, 1, 1, 1);
    }

    private IEnumerator HideLetter  (LetterPosInfo letterInfo, float delay) {
        yield return GameCore.Utils.Yields.mWait(delay);

        var start   = letterInfo.trans.localPosition;
        var fin     = new Vector3(-startLabel.txt_TextHolderSize - startLabel.txt_BiggestLetter, 0, 0);

        var showTime    = letterSpawn;
        var time    = 0f;
        while (time < showTime) {
            var progress    = time / showTime;
            var eased       = Ease.Do(progress, EaseType.easeIn_x2);

            letterInfo.letter.color         = new Color(1, 1, 1, 1 - progress);
            letterInfo.trans.localPosition  = Vector3.Lerp(start, fin, eased);

            yield return null;
            time += Time.deltaTime;
        }

        letterInfo.trans.localPosition = fin;
        letterInfo.letter.color = new Color(1, 1, 1, 0);
    }

}
