﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

[CreateAssetMenu(fileName = "Scene_LoadAdditive", menuName = "GameData/Modules/Scenes/Load Additive")]
public class Scene_LoadAdditive : AGameModule {
    [SerializeField] private Constants.System.GameScenes SceneToLoad;

    public override         void    module_Init    (Action OnComplete, Action OnProgress = null) {
        GameCore.System.Scene.
            mLoadAsync(SceneToLoad, true).
            AsObservable().
            Subscribe((a) => {
                OnComplete?.Invoke();
            });
    }


}
