﻿using UnityEngine;
using DataReflection;

public class BattleGameData {
    [RHolder] public Battle_BaseInfo        BaseInfo        { get; private set; }   = new Battle_BaseInfo();
    [RHolder] public Battle_ResourcesInfo   ResourcesInfo   { get; private set; }   = new Battle_ResourcesInfo();
    [RHolder] public Battle_WavesInfo       WavesInfo       { get; private set; }   = new Battle_WavesInfo();

}
