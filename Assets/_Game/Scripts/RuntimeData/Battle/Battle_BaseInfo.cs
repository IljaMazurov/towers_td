﻿using UnityEngine;
using DataReflection;
using System.Collections.Generic;

public class Battle_BaseInfo {
    [RData] public DataValue<int> iHealthTotal          { get; private set; }   = new DataValue<int>();
    [RData] public DataValue<int> iHealthCurrent        { get; private set; }   = new DataValue<int>();

    [RData] public DataValue<int> iShieldTotal          { get; private set; }   = new DataValue<int>();
    [RData] public DataValue<int> iShieldCurrent        { get; private set; }   = new DataValue<int>();
}