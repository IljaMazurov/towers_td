﻿using UnityEngine;
using DataReflection;
using System.Collections.Generic;

public class Battle_WavesInfo {
    [RData] public DataValue<int> iWavesTotal           { get; private set; }   = new DataValue<int>();
    [RData] public DataValue<int> iWaveCurrent          { get; private set; }   = new DataValue<int>();
    [RData] public DataValue<int> iNextWaveDelaySec     { get; private set; }   = new DataValue<int>();
}