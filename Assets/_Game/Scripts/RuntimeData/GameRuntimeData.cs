﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataReflection;

public class GameRuntimeData : ASingletonMonoConcrete<GameRuntimeData> {
    public GameRuntimeData() {
        DataReflectionCore.PrecacheDatabase(this);
    }

    [RHolder]   public BattleGameData       Battle          { get; private set; }   = new BattleGameData    ();
    [RHolder]   public StorageGameData      Storage         { get; private set; }   = new StorageGameData   ();
    [RHolder]   public SystemGameData       System          { get; private set; }   = new SystemGameData    ();
}


