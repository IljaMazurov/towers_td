﻿using UnityEngine;
using DataReflection;
using System.Collections.Generic;

public class Storage_Currency {
    [RData] public DataValue<int>   iCoins      { get; private set; }    = new DataValue<int>();
    [RData] public DataValue<int>   iGems       { get; private set; }    = new DataValue<int>();
}