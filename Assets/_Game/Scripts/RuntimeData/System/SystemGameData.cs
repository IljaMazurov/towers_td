﻿using UnityEngine;
using DataReflection;

public class SystemGameData {
    [RData] public DataValue<Camera>    camera_Main { get; private set; } = new DataValue<Camera>();
}
