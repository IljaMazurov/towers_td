﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataProcessor_Int_FluidSet : MonoBehaviour {
    [SerializeField] private TMP_Text   TextField;
    [Space]
    [SerializeField] private EaseType   ValueUpdateEase = EaseType.linear;
    [Range(0, 2f)]
    [SerializeField] private float      ValueUpdateTime = 0.25f;

    private Coroutine   _updateRoutine;
    private int         _lastValue;

    public int Input {
        set {
            _updateRoutine.End(ref _updateRoutine, this);
            _updateRoutine = StartCoroutine(UpdateCoroutine(value));
        }
    }

    private IEnumerator UpdateCoroutine(int newValue) {
        var time        = 0f;
        var startVal    = _lastValue;

        while (time < ValueUpdateTime) {
            time += Time.deltaTime;
            var progress    = Mathf.Clamp01(time / ValueUpdateTime);
            var eased       = Ease.Do(progress, ValueUpdateEase);
            
            _lastValue      = (int)Mathf.Lerp(startVal, newValue, eased);
            TextField.text  = _lastValue.ToString();

            yield return null;
        }
    }

    
}
