﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DataProcessor_Int_ObjectDuplicator : MonoBehaviour {
    private List<GameObject>    _object = new List<GameObject>();
    private int                 _lastValue;
    private Transform           _spawnTarget;

    private GameObject          _reference;
    private bool                _inited = false;

    public int Input {
        set {
            if (value == _lastValue)
                return;

            UpdateImages(value);
            _lastValue = value;
        }
    }
    
    private void Init() {
        _object.Clear();

        _spawnTarget    = transform;
        _reference      = transform.GetChild(0).gameObject;

        _object.Add(_reference);
        _lastValue      = 1; // count

        _inited = true;
    }

    private void UpdateImages(int newValue) {
        if (!_inited)
            Init();

        var spawnedTotal = _object.Count;
        for (int i = spawnedTotal; i < newValue; i++) {
            var go = Instantiate(_reference, _spawnTarget);
            _object.Add(go);
        }

        var offset = (newValue - _lastValue).Sign();
        for (int i = _lastValue; i != newValue && i != 0; i += offset)
            _object[i - 1].SetActive(offset > 0);

    }
    
}
