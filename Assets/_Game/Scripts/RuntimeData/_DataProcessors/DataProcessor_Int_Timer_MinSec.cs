﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataProcessor_Int_Timer_MinSec : MonoBehaviour {
    [SerializeField] private TMP_Text   TextField;
    [SerializeField] private string     Format = "{0:00}:{1:00}";
    public int Input {
        set {
            UpdateTime(value);
        }
    }

    private void UpdateTime(int seconds) {
        var min = seconds / 60;
        var sec = seconds % 60;

        TextField.text = string.Format(Format, min, sec);
    }

    
}
