﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataProcessor_String_2Set : MonoBehaviour {
    [SerializeField] private TMP_Text   TextField;
    [Space]
    [SerializeField] private string     StringFormat = "{0}{1}";

    private string _val_1_last = string.Empty;
    private string _val_2_last = string.Empty;

    public string Input_1 {
        set {
            _val_1_last = value;
            UpdateTextField();
        }
    }
    public string Input_2 {
        set {
            _val_2_last = value;
            UpdateTextField();
        }
    }

    private void Awake() {
        if (!StringFormat.Contains("{0}") || 
            !StringFormat.Contains("{1}"))
            StringFormat = "{0}{1}";
    }

    private void UpdateTextField() {
        TextField.text = string.Format(StringFormat, _val_1_last, _val_2_last);
    }


    
}
