﻿using DataReflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RDataSource_ItemInfo : IData {
    [RData] public DataValue<int>       item_PricePerUnit       { get; private set; }    = new DataValue<int>();
    [RData] public DataValue<int>       item_Stars              { get; private set; }    = new DataValue<int>();
    [RData] public DataValue<int>       item_HarvestableFrom    { get; private set; }    = new DataValue<int>();
}