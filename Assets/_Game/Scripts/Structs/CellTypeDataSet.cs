﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constants.Map;

[System.Serializable]
public class CellTypeDataSet {
    public CellType             type;

    public Vector3              prefabOffset;
    public GameObject           prefab;
}
