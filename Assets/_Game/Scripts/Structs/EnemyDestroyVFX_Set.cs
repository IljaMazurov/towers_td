﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyDestroyVFX_Set {
    public Constants.Enemies.EType  enemy;
    public GameObject               prefab;
}
