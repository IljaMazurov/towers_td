﻿using Unity.Mathematics;
public struct EnemyPositionsCache {
    public float3   enemy_Position;
    public float    enemy_DestroyDelay;
}
