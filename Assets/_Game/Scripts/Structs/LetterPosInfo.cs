﻿using TMPro;
using UnityEngine;

public class LetterPosInfo{
    public TMP_Text letter;
    public RectTransform trans;
    public Vector3 target;
}
