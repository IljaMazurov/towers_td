﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class db_BulletAsset {
    public BulletData   asset;
    public bool         active = false;
}