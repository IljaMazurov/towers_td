﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AInteractableUI : BaseObject {
    [SerializeField] protected UITweener[] effectTweens;

    protected System.Type typeUIType { get; private set; }

    private         void    Awake                   () {
        typeUIType = GetType();
    }

    public virtual void     Show                    (object data = null) {
        gameObject.SetActive(true);

        ClearEventsOnFinish(true);
        PlayTweens();
    }

    public virtual void     Close                   () {
        if (effectTweens != null && effectTweens.Length > 0) {
            EventDelegate del = new EventDelegate(() => {
                gameObject.SetActive(false);
            });

            ClearEventsOnFinish(true);
            ReverseTweens(true, del);
        } else {
            gameObject.SetActive(false);
        }
    }

    protected virtual void  PlayTweens              (bool reset = true, EventDelegate onFinish = null) {
        if (effectTweens == null || effectTweens.Length == 0)
            return;

        UITweener longest = null;
        float duration = 0f;
        for (int i = 0; i < effectTweens.Length; i++) {
            var twe = effectTweens[i];
            if (reset)
                twe.ResetToBeginning();
            twe.PlayForward();

            if (twe.duration > duration) {
                longest = twe;
                duration = twe.duration;
            }
        }

        if (onFinish != null)
            longest.AddOnFinished(onFinish);
    }

    protected virtual void  ReverseTweens           (bool reset = true, EventDelegate onFinish = null) {
        if (effectTweens == null || effectTweens.Length == 0)
            return;

        UITweener longest = null;
        float duration = 0f;
        for (int i = 0; i < effectTweens.Length; i++) {
            var twe = effectTweens[i];
            if (reset)
                twe.ResetToEnding();
            twe.PlayReverse();

            if (twe.duration > duration) {
                longest = twe;
                duration = twe.duration;
            }
        }

        if (onFinish != null)
            longest.AddOnFinished(onFinish);
    }

    protected void          ClearEventsOnFinish     (bool remove) {
        if (effectTweens == null || effectTweens.Length == 0)
            return;

        for (int i = 0; i < effectTweens.Length; i++) {
            effectTweens[i].ClearEventsOnFinish = remove;
        }
    }


}
