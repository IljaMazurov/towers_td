﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AWindow : AInteractableUI {
    public override void Close() {
        base.Close();

        GameCore.System.Events.mTrigger(new GameEvents.Windows.OnWindowClosed {
            windowType = typeUIType
        });
    }

}
