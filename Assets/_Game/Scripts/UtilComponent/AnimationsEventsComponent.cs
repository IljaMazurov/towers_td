﻿using UnityEngine;
using Constants.Animations;
using System.Collections.Generic;

public class AnimationsEventsComponent : MonoBehaviour {
    private Dictionary<int, List<System.Action<EventTrigger>>> animEvents = new Dictionary<int, List<System.Action<EventTrigger>>>();



    public  void animEvents_mSubscribe      (EventTrigger   eventType, System.Action<EventTrigger> callback) {
        var eventIndex = (int)eventType;
        if (!animEvents.ContainsKey(eventIndex))
            animEvents.Add(eventIndex, new List<System.Action<EventTrigger>>());

        animEvents[eventIndex].Add(callback);
    }

    public  void animEvents_mUnSubscribe    (EventTrigger   eventType, System.Action<EventTrigger> callback) {
        var eventIndex = (int)eventType;
        if (!animEvents.ContainsKey(eventIndex))
            return;

        var eventsList  = animEvents[eventIndex];
        eventsList.Remove(callback);
    }

    private void animEvents_mOnKeyEvent     (EventTrigger   eventType) {
        var eventIndex = (int)eventType;

        if (animEvents.ContainsKey(eventIndex)) {
            var eventsList  = animEvents[eventIndex];
            var eventsCount = eventsList.Count;

            for (int i = 0; i < eventsCount; i++) {
                eventsList[i]?.Invoke(eventType);
            }
        }
    }

}


