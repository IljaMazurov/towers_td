﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMain_UpdateCanvas : MonoBehaviour {
    private void Awake() {
        var mainCamera  = GameCore.Data.DynamicData.System.camera_Main.Value;
        var canvas      = GetComponent<Canvas>();

        if (mainCamera != null && canvas != null) {
            if (canvas?.worldCamera != null)
                Destroy(canvas.worldCamera.gameObject);

            canvas.worldCamera = mainCamera;
        }
    }
}
