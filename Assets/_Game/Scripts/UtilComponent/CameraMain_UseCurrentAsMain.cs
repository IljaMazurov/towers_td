﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMain_UseCurrentAsMain : MonoBehaviour {
    void Awake() {
        GameCore.Data.DynamicData.System.camera_Main.Value = GetComponent<Camera>();
    }

}
