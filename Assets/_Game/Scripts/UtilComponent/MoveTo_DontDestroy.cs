﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTo_DontDestroy : MonoBehaviour {
    void Awake() {
        transform.parent = null;
        DontDestroyOnLoad(this.gameObject);
    }

}
