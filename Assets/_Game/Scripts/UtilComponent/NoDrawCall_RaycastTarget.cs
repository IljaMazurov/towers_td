﻿using UnityEngine;
using UnityEngine.UI;

public class NoDrawCall_RaycastTarget : Graphic {
    public      override void SetMaterialDirty() {}
    public      override void SetVerticesDirty() {}
    protected   override void OnPopulateMesh(VertexHelper vh) {vh.Clear();}

    public void test() {
        Dbg.Log(1);
    }
}