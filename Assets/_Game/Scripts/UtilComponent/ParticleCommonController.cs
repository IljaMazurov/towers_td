﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

using Unity.Mathematics;
using Unity.Collections;

public class ParticleCommonController : MonoBehaviour {
    private ParticleSystem      ps;

    public void     mInit          () {
        ps  = GetComponent<ParticleSystem>();
    }

    public void     mEmitParticle  (float3 position) {
        var emitParams      = new ParticleSystem.EmitParams();
        emitParams.position = position;

        ps.Emit(emitParams, 1);
    }

}
