﻿using UniRx;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.Collections;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

using mat = Unity.Mathematics.math;

public class ParticlePath : BaseObject {
    [SerializeField] 
    private ParticleSystem      ps;

    private UpdateParticlesJob          job         = new UpdateParticlesJob();
    private NativeArray<float3>         pathArray;
    private bool                        inited      = false;
    private float                       y_offset    = 0.5f;

    private void            OnValidate                      () {
        ps      = GetComponent<ParticleSystem>();
    }

    public void             particles_Init                  (List<float3> path, Color flowColor, float delay = 0f) {
        var myPath      = new List<float3>(path);
        obj_position    = myPath[0] + new float3(0, y_offset, 0);
        myPath.RemoveAt(0);

        var distance = Vector3.Distance(myPath[0], transform.position);
        for (int i = 1; i < myPath.Count; i++)
            distance += Vector3.Distance(myPath[i - 1], myPath[i]);

        ps.Stop();
        ps.Clear();

        var mBlock              = ps.main;
        mBlock.duration         = distance;
        mBlock.startLifetime    = distance;
        mBlock.startColor       = flowColor;

        pathArray       = new NativeArray<float3>(myPath.Count, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

        for (int i = 0; i < myPath.Count; i++) {
            var p    = myPath[i];
            p.y     += y_offset;
            pathArray[i] = p;
        }

        job.path    = pathArray;
        job.pathLen = myPath.Count;

        MainThreadDispatcher.StartCoroutine(StartSystem(delay));
    }
    public void             particles_Stop                  () {
        ps.Stop();
    }

    private IEnumerator     StartSystem(float delay) {
        yield return GameCore.Utils.Yields.mWait(delay);
        inited = true;
        ps.Play();
    }

    private void            OnParticleUpdateJobScheduled    () {
        if (inited)
            job.Schedule(ps);
    }

    private void            OnDisable                       () {
        if (inited)
            pathArray.Dispose();
    }

    struct UpdateParticlesJob : IJobParticleSystem {
        public int                  pathLen;
        public NativeArray<float3>  path;

        public void Execute (ParticleSystemJobData jobData) {
            var velX  = jobData.velocities.x;
            var velY  = jobData.velocities.y;
            var velZ  = jobData.velocities.z;

            var posX  = jobData.positions.x;
            var posY  = jobData.positions.y;
            var posZ  = jobData.positions.z;

            var progress    = jobData.aliveTimePercent;
            var sliceSize   = 1f / pathLen;

            for (int i = 0; i < jobData.count; i++) {
                var perc        = (progress[i] / 100f);
                var index       = (int)mat.floor(perc / sliceSize);
                var target      = path[index];

                var direction   = new Vector3(target.x - posX[i], target.y - posY[i], target.z - posZ[i]).normalized;

                velX[i]         = mat.lerp(velX[i], direction.x, .05f);
                velY[i]         = mat.lerp(velY[i], direction.y, .05f);
                velZ[i]         = mat.lerp(velZ[i], direction.z, .05f);
            }
        }
    }
}
