﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

using Unity.Mathematics;
using Unity.Collections;

public class ParticlesBulletsController : MonoBehaviour {
    [Range(0, 1)]
    [SerializeField] private float TimeToLiveOverride = 1f;

    private UpdateParticlesJob  job = new UpdateParticlesJob();
    private ParticleSystem      ps;

    public void     mInit          () {
        ps                  = GetComponent<ParticleSystem>();
        job.targets         = GameCore.Battle.CacheData.mGetEnemyPositionsCacheRef();
        job.timeOverride    = TimeToLiveOverride;
    }

    public void     mEmitBullet    (int ID, float3 start, float3 velocityVector, float lifeTime) {
        var emitParams  = new ParticleSystem.EmitParams();

        emitParams.randomSeed       = (uint)ID;
        emitParams.position         = start;
        emitParams.velocity         = velocityVector;
        emitParams.startLifetime    = lifeTime / TimeToLiveOverride;

        ps.Emit(emitParams, 1);
    }

    private void    OnParticleUpdateJobScheduled    () {
        job.Schedule(ps);
    }


    struct UpdateParticlesJob : IJobParticleSystem {
        public float                                    timeOverride;
        public NativeHashMap<int, EnemyPositionsCache>  targets;

        public void Execute (ParticleSystemJobData jobData) {
            var idxs        = jobData.randomSeeds;

            var posX        = jobData.positions.x;
            var posY        = jobData.positions.y;
            var posZ        = jobData.positions.z;

            var progress    = jobData.aliveTimePercent;

            for (int i = 0; i < jobData.count; i++) {
                var idx     = idxs[i];
                var perc    = (progress[i] / 100f) / timeOverride;
                perc        = math.clamp(perc, 0f, 1f);

                var lerpVal = Ease.Do(perc, EaseType.easeIn_x3);

                var finish  = targets[(int)idx].enemy_Position;

                posX[i]     = math.lerp(posX[i], finish.x, lerpVal);
                posY[i]     = math.lerp(posY[i], finish.y, lerpVal);
                posZ[i]     = math.lerp(posZ[i], finish.z, lerpVal);
            }
        }

    }
}
