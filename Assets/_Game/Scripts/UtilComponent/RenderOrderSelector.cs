﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RenderOrderSelector : MonoBehaviour {
    [SerializeField] Renderer renderer;

    public int RenderOrder {
        get {
            return renderer == null ? 0 : renderer.sortingOrder;
        }

        set {
            if (renderer != null)
                renderer.sortingOrder = value;
        }
    }

    public int RenderLayer {
        get {
            return renderer == null ? 0 : renderer.sortingLayerID;
        }

        set {
            if (renderer != null)
                renderer.sortingLayerID = value;
        }
    }

}
