﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorComponent : MonoBehaviour {
    [SerializeField] private bool localRotation;
    [Space]
    [SerializeField] private bool x;
    [SerializeField] private bool y;
    [SerializeField] private bool z;
    [Space]
    [SerializeField, Range(-25, 25)] private float speedX = 1f;
    [SerializeField, Range(-25, 25)] private float speedY = 1f;
    [SerializeField, Range(-25, 25)] private float speedZ = 1f;

    private Vector3     _basicRotation;
    private Transform   _cacheTrans;

    private void    Awake      () {
        _cacheTrans     = transform;
        _basicRotation  = _cacheTrans.localRotation.eulerAngles;
    }

    private void    OnEnable   () {
#if UNITY_EDITOR
        if (Application.isPlaying)
#endif
            GameCore.System.Updater.mSubscribe(Rotate);
    }

    private void    OnDisable  () {
#if UNITY_EDITOR
        if (Application.isPlaying)
#endif
            GameCore.System.Updater.mUnsubscribe(Rotate);
    }


    private void    Rotate      (float deltaTime) {
        _basicRotation.x = x ? _basicRotation.x += speedX * deltaTime * 10f : _basicRotation.x;
        _basicRotation.y = y ? _basicRotation.y += speedY * deltaTime * 10f : _basicRotation.y;
        _basicRotation.z = z ? _basicRotation.z += speedZ * deltaTime * 10f : _basicRotation.z;

        if (localRotation) {
            _cacheTrans.localRotation   = Quaternion.Euler(_basicRotation);
        } else {
            _cacheTrans.rotation        = Quaternion.Euler(_basicRotation);
        }
    }
}
