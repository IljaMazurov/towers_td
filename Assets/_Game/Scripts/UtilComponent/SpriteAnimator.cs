﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteAnimator : MonoBehaviour {
    [SerializeField]                protected Sprite[]  List;
    [SerializeField, Range(1,60)]   protected int       Fps             = 30;
    [SerializeField]                protected AnimType  Type            = AnimType.Default;
    [SerializeField]                protected bool      RandStartIndex  = false;
    [SerializeField]                protected bool      LoopAnimation   = true;

    protected   SpriteRenderer      sRenderer;
    protected   Coroutine           animator;

    private             void        OnEnable        () {
        IEnumerator routine = null;
        switch (Type) {
            default:
                routine = DefaultAnimator();
                break;
        }

        sRenderer   = GetComponent<SpriteRenderer>();
        animator    = StartCoroutine(routine);
    }

    private             IEnumerator DefaultAnimator () {
        var mexLength   = List.Length;
        var index       = 0;

        if (RandStartIndex) {
            index = Random.Range(0, mexLength);
        }

        yield return null;

        var time    = 0f;
        while (animator != null) {
            yield return null;

            time += Time.deltaTime;
            if (time > (1f / Fps)) {
                time                = 0;
                index               = ++index % mexLength;
                if (!LoopAnimation && index == 0) 
                    break;

                sRenderer.sprite    = List[index];
            }
        }
    }

    public enum         AnimType : int {
        Default         = 0
    }

}