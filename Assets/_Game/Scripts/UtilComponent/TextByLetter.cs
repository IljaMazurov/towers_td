﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextByLetter : BaseObject {
    [SerializeField]    private     RectTransform       holder;
    [SerializeField]    private     TMP_Text            tmpSample;
    [Space]
    [SerializeField]    private     string              textLabel;
    [Range(0.5f, 1.5f)]
    [SerializeField]    private     float               letterWidthMul = 0.7f;


    public float                txt_TextHolderSize  => _holderSize;
    public float                txt_BiggestLetter   => _biggestLetter;
    public List<LetterPosInfo>  txt_Letters         => _letters;

    private float               _biggestLetter      = 0f;
    private float               _holderSize         = 0f;
    private List<LetterPosInfo> _letters            = new List<LetterPosInfo>();

    public void txt_mInit   (string label = null, float letterWidth = 0f) {
        if (!string.IsNullOrEmpty(label))
            textLabel = label;

        if (letterWidth != 0)
            letterWidthMul = letterWidth;

        Setup();
    }

    private void Setup() {
        _letters.Clear();

        MakeAString();
        UpdateRects();
    }

    private void MakeAString() {
        var tmpLetter   = tmpSample;
        var txt         = textLabel.ToCharArray();

        for (int i = 0; i < txt.Length; i++) {
            var letter      = txt[i].ToString();
            tmpLetter.text  = letter;
            tmpLetter.ForceMeshUpdate();
            _letters.Add(new LetterPosInfo { 
                letter  = tmpLetter,
                trans   = tmpLetter.transform as RectTransform
            });

            if (i < txt.Length - 1)
                tmpLetter = Instantiate(tmpLetter, holder);
        }
    }

    private void UpdateRects() {
        var totalSize   = 0f;
        var offset      = 0f;

        var holderSize  = holder.rect.width;
        _holderSize     = holderSize;
        for (int i = 0; i < _letters.Count; i++) {
            var lTrans = _letters[i].trans;
            totalSize               = offset;

            lTrans.sizeDelta        = _letters[i].letter.bounds.size;
            lTrans.localPosition    = new Vector3(holderSize + _letters[i].letter.bounds.size.x, 0, 0);

            _letters[i].target      = new Vector3(offset, 0, 0);
            offset                  = totalSize + lTrans.sizeDelta.x * letterWidthMul;

            if (_biggestLetter < lTrans.sizeDelta.x)
                _biggestLetter = lTrans.sizeDelta.x;
        }

        for (int i = 0; i < _letters.Count; i++) {
            _letters[i].target -= new Vector3(totalSize / 2f, 0, 0);
            _letters[i].trans.localPosition -= new Vector3(totalSize / 2f, 0, 0);
        }

    }

}
