﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureMaker {

    RenderTexture   canvas;
    Texture2D       result;

    Material        drawMaterial;
    Color           bgColor;

    Mesh            quadMesh;

    public  Texture2D       Result          => result;
    public  RenderTexture   RT_Result       => canvas;

    public  int             ResultWidth     { get; private set; }
    public  int             ResultHeight    { get; private set; }

    public Camera           drawingCamera;

    #region Init/Release
    public TextureMaker     Setup               (int textureWidth, int textureHeight, Color background, int quadWidth = 1, int quadHeight = 1) {
        drawingCamera   = Camera.main;
        ResultWidth     = textureWidth;
        ResultHeight    = textureHeight;

        if (canvas != null && canvas.IsCreated()) {
            RenderTexture.ReleaseTemporary(canvas);
            canvas = null;
        }

        if (quadMesh == null) {
            GeneratePlane(quadWidth, quadHeight);
        }

        canvas          = RenderTexture.GetTemporary(ResultWidth, ResultHeight, 0, RenderTextureFormat.ARGB32);
        bgColor         = background;

        return this;
    }

    public void             SetCamera           (Camera toUse) {
        drawingCamera = toUse;
    }

    private void            GeneratePlane       (int w, int h) {
        var _quads      = w * h * 4;

        Vector3[]   vertices    = new Vector3[_quads];
        Vector2[]   uv          = new Vector2[_quads];
        Vector2[]   uv1         = new Vector2[_quads];
        Vector2[]   uv2         = new Vector2[_quads];
        int[]       tris        = new int[w * h * 6];

        var _width_x	= 1f / w;
		var _height_x	= 1f / h;

        int x           = 0;
		int y           = 0;

        var pp = 0;
        var uu = 0;
        var tt = 0;

        var cc1 = 0;
        var cc2 = 0;

		for (int i = 0; i < _quads; i+=4) {
			var px1 = _width_x * (x);
			var px2 = _width_x * (x + 1);
			var py1 = _height_x * (y);
			var py2 = _height_x * (y + 1);

            vertices[pp++] = new Vector3(px1, py1, 0);
            vertices[pp++] = new Vector3(px2, py1, 0);
            vertices[pp++] = new Vector3(px2, py2, 0);
            vertices[pp++] = new Vector3(px1, py2, 0);

            var centerQuad_x    = (px1 + px2) / 2f;
            var centerQuad_y    = (py1 + py2) / 2f;
            var sideDistance_x  = px2 - centerQuad_x;
            var sideDistance_y  = py2 - centerQuad_y;

            var c1 = new Vector2(centerQuad_x, centerQuad_y);
            var c2 = new Vector2(sideDistance_x, sideDistance_y);
            uv1[cc1++] = c1;
            uv1[cc1++] = c1;
            uv1[cc1++] = c1;
            uv1[cc1++] = c1;

            uv2[cc2++] = c2;
            uv2[cc2++] = c2;
            uv2[cc2++] = c2;
            uv2[cc2++] = c2;

            
			var ux1 = x * _width_x / 1f;
			var ux2 = (x + 1) * _width_x / 1f;
			var uy1 = y * _height_x / 1f;
			var uy2 = (y + 1) * _height_x / 1f;

            uv[uu++] = new Vector3(ux1, uy1, 0);
            uv[uu++] = new Vector3(ux2, uy1, 0);
            uv[uu++] = new Vector3(ux2, uy2, 0);
            uv[uu++] = new Vector3(ux1, uy2, 0);


            tris[tt++] = i;
            tris[tt++] = i+2;
            tris[tt++] = i+1;

            tris[tt++] = i;
            tris[tt++] = i+3;
            tris[tt++] = i+2;

			x++;
			if (x >= w) {
				x = 0;
				y++;
            }
        }

        quadMesh = new Mesh();

        quadMesh.vertices   = vertices;
        quadMesh.triangles  = tris;
        quadMesh.uv         = uv;
        quadMesh.uv2        = uv1;
        quadMesh.uv3        = uv2;

        quadMesh.RecalculateNormals();
    }

    public  TextureMaker    SetMaterial         (Material pencilMaterial) {
        drawMaterial = pencilMaterial;

        return this;
    }

    public  TextureMaker    Begin               (RenderTexture _rt = null, bool clear = true) {
        if (canvas == null)
            throw new System.Exception("RenderTExture is not Initialized, call \"Setup\" first");

        if (canvas == null)
            throw new System.Exception("Draw Material is null, call \"SetMaterial\" first");

        var trgt = _rt == null ? canvas : _rt;

        Camera.main.targetTexture = trgt;
        Graphics.SetRenderTarget(trgt);

        ClearCurrentTarget(clear);

        return this;
    }

    public TextureMaker     ClearCurrentTarget  (bool clear) {
        if (clear)
            GL.Clear(true, true, bgColor);

        GL.PushMatrix();
        GL.LoadOrtho();

        return this;
    }

    public  TextureMaker    SoftEnd             () {
        GL.PopMatrix();
        Graphics.SetRenderTarget(null);
        Camera.main.targetTexture = null;

        return this;
    }

    public  TextureMaker    End                 () {
        result          = new Texture2D(canvas.width, canvas.height);
        result.name     = $"floor_shadowmap_{canvas.width}x{canvas.height}";
        result.wrapMode = TextureWrapMode.Clamp;

        result.ReadPixels(new Rect(0, 0, canvas.width, canvas.height), 0, 0);
        result.Apply();

        GL.PopMatrix();
        Graphics.SetRenderTarget(null);
        Camera.main.targetTexture = null;

        drawMaterial?.SetTexture(Constants.Shaders.MainTex,   null);
        drawMaterial?.SetVector (Constants.Shaders.MainTexST, new Vector4(1, 1, 0, 0));

        RenderTexture.ReleaseTemporary(canvas);

        return this;
    }

    #endregion

    #region Drawing

    public TextureMaker     DrawSprite          (Sprite src, Vector2 uv, Vector2 scale, Color col) {
        //matrix struct = m[Y][X]
        var drawMatrix = Matrix4x4.identity;

        // translate
        drawMatrix.m03 = uv.x;
        drawMatrix.m13 = uv.y;

        // scale
        drawMatrix.m00 = scale.x;
        drawMatrix.m11 = scale.y;

        drawMaterial.SetVector  (Constants.Shaders.Color,     col);
        drawMaterial.SetTexture (Constants.Shaders.MainTex,   src.texture);
        drawMaterial.SetVector  (Constants.Shaders.MainTexST, src.getUvOffset());

        drawMaterial.SetPass    (0);

        Graphics.DrawMeshNow    (quadMesh, drawMatrix);

        return this;
    }
    public TextureMaker     DrawSprite          (Sprite src, int x, int y, Vector2 scale, Color col) {
        var uv = new Vector2((float)x / ResultWidth, (float)y / ResultHeight);
        return DrawSprite(src, uv, scale, col);
    }
    public TextureMaker     DrawSprite          (Sprite src, int x, int y, int width, int height, Color col) {
        var uv      = new Vector2((float)x      / ResultWidth,  (float)y        / ResultHeight);
        var scale   = new Vector2((float)width  / ResultWidth,  (float)height   / ResultHeight);
        return DrawSprite(src, uv, scale, col);
    }


    public TextureMaker     DrawTexture         (Texture src, Vector2 uv,   Vector2 scale, Color col) {
        //matrix struct = m[Y][X]
        var drawMatrix = Matrix4x4.identity;

        // translate
        drawMatrix.m03 = uv.x;
        drawMatrix.m13 = uv.y;

        // scale
        drawMatrix.m00 = scale.x;
        drawMatrix.m11 = scale.y;

        drawMaterial.SetVector  (Constants.Shaders.Color, col);
        drawMaterial.SetTexture (Constants.Shaders.MainTex, src);
        drawMaterial.SetPass    (0);

        Graphics.DrawMeshNow    (quadMesh, drawMatrix);

        return this;
    }
    public TextureMaker     DrawTexture         (Texture src, int x, int y, Vector2 scale, Color col) {
        var uv = new Vector2((float)x / ResultWidth, (float)y / ResultHeight);
        return DrawTexture(src, uv, scale, col);
    }
    public TextureMaker     DrawTexture         (Texture src, int x, int y, int width, int height, Color col) {
        var uv      = new Vector2((float)x      / ResultWidth,  (float)y        / ResultHeight);
        var scale   = new Vector2((float)width  / ResultWidth,  (float)height   / ResultHeight);
        return DrawTexture(src, uv, scale, col);
    }

    public TextureMaker     UsePostFX           () {
        var drawMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);

        drawMaterial.SetPass    (0);
        Graphics.DrawMeshNow    (quadMesh, drawMatrix);

        return this;
    }

    #endregion
}

