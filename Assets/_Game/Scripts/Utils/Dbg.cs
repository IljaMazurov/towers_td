﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Dbg  {

#if UNITY_EDITOR
    public static void      Log     (string message) {
        Debug.Log(message);
    }

    public static void      Log<T>     (T message) {
        Debug.Log(message.ToString());
    }

    public static void      Log     (string message, Color col) {
        Debug.Log($"<color=#{ColorUtility.ToHtmlStringRGB(col)}>{message}</color>");
    }

    public static void      Log<T>  (T message, Color col) {
        Debug.Log($"<color=#{ColorUtility.ToHtmlStringRGB(col)}>{message.ToString()}</color>");
    }

    public static void      Log     (string message, params Color[] col) {
        message = message.Replace("}", "</color>");

        var index   = message.IndexOf("{");
        var colNum  = 0;
        while (index != -1) {
            var color = colNum < col.Length ? col[colNum] : Color.white;

            message = message.Remove(index, 1);
            message = message.Insert(index, $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>");


            index   = message.IndexOf("{");
            colNum++;
        }

        Debug.Log(message);
    }


#else
    [System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
    public static void      Log     (string message) {
        Debug.Log(message);
    }

    [System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
    public static void      Log<T>     (T message) {
        Debug.Log(message.ToString());
    }

    [System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
    public static void      Log     (string message, Color col) {
        Debug.Log($"<color=#{ColorUtility.ToHtmlStringRGB(col)}>{message}</color>");
    }

    [System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
    public static void      Log<T>  (T message, Color col) {
        Debug.Log($"<color=#{ColorUtility.ToHtmlStringRGB(col)}>{message.ToString()}</color>");
    }

    [System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
    public static void      Log     (string message, params Color[] col) {
        message = message.Replace("}", "</color>");

        var index   = message.IndexOf("{");
        var colNum  = 0;
        while (index != -1) {
            var color = colNum < col.Length ? col[colNum] : Color.white;

            message = message.Remove(index, 1);
            message = message.Insert(index, $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>");


            index   = message.IndexOf("{");
            colNum++;
        }

        Debug.Log(message);
    }
#endif

}
