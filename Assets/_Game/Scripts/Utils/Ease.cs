﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Ease {
    public static float Do (float t, EaseType type, int mult = 1) {
        t = Mathf.Clamp01(t);

        switch (type) {
            case EaseType.instant:
                t = 1;
                break;

            case EaseType.easeIn_x2:
                for (int i = 0; i < mult; i++)
                    t = t * t;
                break;
            case EaseType.easeOut_x2:
                for (int i = 0; i < mult; i++)
                    t = t * (2 - t);
                break;
            case EaseType.easeInOut_x2:
                for (int i = 0; i < mult; i++)
                    t = t < .5f ? 2 * t * t : -1 + (4 - 2 * t) * t;
                break;
            case EaseType.easeIn_x3:
                for (int i = 0; i < mult; i++)
                    t = t * t * t;
                break;
            case EaseType.easeOut_x3:
                for (int i = 0; i < mult; i++)
                    t = (--t) * t * t + 1;
                break;
            case EaseType.easeInOut_x3:
                for (int i = 0; i < mult; i++)
                    t = t < .5f ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
                break;
            case EaseType.easeIn_x4:
                for (int i = 0; i < mult; i++)
                    t = t * t * t * t;
                break;
            case EaseType.easeOut_x4:
                for (int i = 0; i < mult; i++)
                    t = 1 - (--t) * t * t * t;
                break;
            case EaseType.easeInOut_x4:
                for (int i = 0; i < mult; i++)
                    t = t < .5f ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
                break;
            case EaseType.easeIn_x5:
                for (int i = 0; i < mult; i++)
                    t = t * t * t * t * t;
                break;
            case EaseType.easeOut_x5:
                for (int i = 0; i < mult; i++)
                    t = 1 + (--t) * t * t * t * t;
                break;
            case EaseType.easeInOut_x5:
                for (int i = 0; i < mult; i++)
                    t = t < .5f ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
                break;

            case EaseType.custom_easeInOverSize:
                for (int i = 0; i < mult; i++)
                    t = (1.2f * (t * t * t) - (t * t * t * t)) * 5;
                break;
        }

        return t;
    }

}

public enum EaseType {
    instant,

    linear,

    easeIn_x2,
    easeOut_x2,
    easeInOut_x2,

    easeIn_x3,
    easeOut_x3,
    easeInOut_x3,

    easeIn_x4,
    easeOut_x4,
    easeInOut_x4,

    easeIn_x5,
    easeOut_x5,
    easeInOut_x5,


    custom_easeInOverSize, // 0 - 1.1(90%) - 1
}
