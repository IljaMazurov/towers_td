﻿using System.Collections;
using System.Collections.Generic;
using UniRx.Triggers;
using UnityEngine;


public class ShaderVarsEx : MonoBehaviour {
    private int _fastSinTimeID          = Shader.PropertyToID("_fastSinTime");
    private int _fastSinTimePlusID      = Shader.PropertyToID("_fastSinTimePlus");
    private int _fastSinTimeMinusID     = Shader.PropertyToID("_fastSinTimeMinus");

    private int _fastCosTimeID          = Shader.PropertyToID("_fastCosTime");
    private int _fastCosTimePlusID      = Shader.PropertyToID("_fastCosTimePlus");
    private int _fastCosTimeMinusID     = Shader.PropertyToID("_fastCosTimeMinus");

    private int _noizeVecticalID        = Shader.PropertyToID("_noizeVectical");
    private int _noizeHorizontalID      = Shader.PropertyToID("_noizeHorizontal");
    private int _randomVarsID           = Shader.PropertyToID("_randomVars");

    private int _cameraDirectionID      = Shader.PropertyToID("_cameraDirection");
    private int _cameraDirection_invID  = Shader.PropertyToID("_cameraDirection_inv");


    [GlobalVarDescription("_fastSinTime",               "_SinTime analog (-1 : 1). xyzw = 2x 4x 8x 16x (speed)")]
    [SerializeField] private bool addFastSinTime        = false;
    [GlobalVarDescription("_fastSinTimePlus",           "_SinTime analog ( 0 : 1). xyzw = 2x 4x 8x 16x (speed)")]
    [SerializeField] private bool addFastSinTimePlus    = false;
    [GlobalVarDescription("_fastSinTimeMinus",          "_SinTime analog (-1 : 0). xyzw = 2x 4x 8x 16x (speed)")]
    [SerializeField] private bool addFastSinTimeMinus   = false;

    [GlobalVarDescription("_fastCosTime",               "_CosTime analog (-1 : 1). xyzw = 2x 4x 8x 16x (speed)")]
    [SerializeField] private bool addFastCosTime        = false;
    [GlobalVarDescription("_fastCosTimePlus",           "_CosTime analog ( 0 : 1). xyzw = 2x 4x 8x 16x (speed)")]
    [SerializeField] private bool addFastCosTimePlus    = false;
    [GlobalVarDescription("_fastCosTimeMinus",          "_CosTime analog (-1 : 0). xyzw = 2x 4x 8x 16x (speed)")]
    [SerializeField] private bool addFastCosTimeMinus   = false;


    [GlobalVarDescription("_noizeHorizontal",           "Perlin Noize X Coord (0 : 1). xyzw = 0.5x 1x 2x 4x (speed)")]
    [SerializeField] private bool addNoizeHorizontal    = false;
    [GlobalVarDescription("_noizeVectical",             "Perlin Noize Y Coord (0 : 1). xyzw = 0.5x 1x 2x 4x (speed)")]
    [SerializeField] private bool addNoizeVertical      = false;
    [GlobalVarDescription("_randomVars",                "Random values (0 : 1). xyzw")]
    [SerializeField] private bool addRandomVars         = false;

    [GlobalVarDescription("_cameraDirection | _cameraDirection_inv",    "Camera direction")]
    [SerializeField] private bool addCameraDirection         = false;

    private void Update() {
        var t   = Time.time;

        if (addCameraDirection) {
            var direction = Camera.main.transform.forward;

            Shader.SetGlobalVector(_cameraDirectionID,      direction);
            Shader.SetGlobalVector(_cameraDirection_invID,  direction.exMirror());
        }

        if (addFastSinTime || addFastSinTimePlus || addFastSinTimeMinus) {
            var sin2x   = Mathf.Sin(t * 2f);
            var sin4x   = Mathf.Sin(t * 4f);
            var sin8x   = Mathf.Sin(t * 8f);
            var sin16x  = Mathf.Sin(t * 16f);

            if (addFastSinTime) {
                var _fastSinTime         = new Vector4(sin2x, sin4x, sin8x, sin16x);
                Shader.SetGlobalVector(_fastSinTimeID, _fastSinTime);
            }

            if (addFastSinTimePlus) {
                var _fastSinTimePlus     = new Vector4((sin2x + 1) * 0.5f, (sin4x + 1) * 0.5f, (sin8x + 1) * 0.5f, (sin16x + 1) * 0.5f);
                Shader.SetGlobalVector(_fastSinTimePlusID, _fastSinTimePlus);
            }

            if (addFastSinTimeMinus) {
                var _fastSinTimeMinus    = new Vector4((sin2x - 1) * 0.5f, (sin4x - 1) * 0.5f, (sin8x - 1) * 0.5f, (sin16x - 1) * 0.5f);
                Shader.SetGlobalVector(_fastSinTimeMinusID, _fastSinTimeMinus);
            }
        }

        if (addFastCosTime || addFastCosTimePlus || addFastCosTimeMinus) {
            var cos2x   = Mathf.Cos(t * 2f);
            var cos4x   = Mathf.Cos(t * 4f);
            var cos8x   = Mathf.Cos(t * 8f);
            var cos16x  = Mathf.Cos(t * 16f);

            if (addFastCosTime) {
                var fastCosTime         = new Vector4(cos2x, cos4x, cos8x, cos16x);
                Shader.SetGlobalVector(_fastCosTimeID, fastCosTime);
            }

            if (addFastCosTimePlus) {
                var fastCosTimePlus     = new Vector4((cos2x + 1) * 0.5f, (cos4x + 1) * 0.5f, (cos8x + 1) * 0.5f, (cos16x + 1) * 0.5f);
                Shader.SetGlobalVector(_fastCosTimePlusID, fastCosTimePlus);
            }

            if (addFastCosTimeMinus) {
                var fastCosTimeMinus    = new Vector4((cos2x - 1) * 0.5f, (cos4x - 1) * 0.5f, (cos8x - 1) * 0.5f, (cos16x - 1) * 0.5f);
                Shader.SetGlobalVector(_fastCosTimeMinusID, fastCosTimeMinus);
            }
        }
        
        
        if (addNoizeVertical) {
            var _noizeVectical  = new Vector4(  Mathf.PerlinNoise(0, t * .5f), 
                                                Mathf.PerlinNoise(0, t * 1f),
                                                Mathf.PerlinNoise(0, t * 2f),
                                                Mathf.PerlinNoise(0, t * 4f));

            Shader.SetGlobalVector(_noizeVecticalID, _noizeVectical);
        }

        if (addNoizeHorizontal) { 
            var _noizeHorizontal = new Vector4( Mathf.PerlinNoise(t * .5f, 0), 
                                                Mathf.PerlinNoise(t * 1f, 0),
                                                Mathf.PerlinNoise(t * 2f, 0),
                                                Mathf.PerlinNoise(t * 4f, 0));
            Shader.SetGlobalVector(_noizeHorizontalID, _noizeHorizontal);
        }

        if (addRandomVars ) {
            var _randomVars  = new Vector4(Random.value, Random.value, Random.value, Random.value);
            Shader.SetGlobalVector(_randomVarsID, _randomVars);
        }

    }

}

public class GlobalVarDescriptionAttribute : System.Attribute {
    public readonly string nameID;
    public readonly string description;
    public GlobalVarDescriptionAttribute(string name, string descr) { nameID = name; description = descr; }
}