﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background_ColorOnDamage : MonoBehaviour {
    [SerializeField] private Color _color;
    [SerializeField] private float _colorTime;

    private Image       _targetImage;
    private Coroutine   _colorRoutine;

    void Awake() {
        _targetImage = GetComponent<Image>();
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.BaseDamagged>(OnBaseDamaged, true);
    }

    private void OnBaseDamaged(GameEvents.Battle.Game.BaseDamagged obj) {
        _colorRoutine.End(ref _colorRoutine, this);
        _colorRoutine = StartCoroutine(CameraShake());
    }

    private IEnumerator CameraShake() {
        var time = 0f;

        var defaultColor = Color.white;

        while (time < _colorTime) {
            yield return null;
            time += Time.deltaTime;

            var progress        = 1f - (0.5f - time / _colorTime).Abs() * 2f;
            var eased           = Ease.Do(Mathf.Clamp01(progress), EaseType.easeInOut_x2);

            _targetImage.color  = Color.Lerp(defaultColor, _color, eased);
        }

        _colorRoutine = null;
    }



}
