﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleLauncher : MonoBehaviour {
    
    [SerializeField] private ABattleMap            mapToBuild;

    [SerializeField] private List<AGameModule>      farm_GameModules        = new List<AGameModule>();

    private void                Start               () {
        Application.targetFrameRate     = 60;
        Camera.main.depthTextureMode    = DepthTextureMode.Depth;
        Launcher();
    }

    private void                Launcher            () {
        StartCoroutine(LaunchRoutine());
    }

    private IEnumerator         LaunchRoutine       () {
        yield return null;

        int modulesToLoad = 0;
        foreach (var mod in farm_GameModules) {
            modulesToLoad++;
            mod.module_Init( () => { modulesToLoad--; });
        }

        var delay = GameCore.Utils.Yields.mWait(0.1f);
        while (modulesToLoad > 0)
            yield return delay;

        GameCore.Battle.Map.mLoad(mapToBuild);

    }

}
