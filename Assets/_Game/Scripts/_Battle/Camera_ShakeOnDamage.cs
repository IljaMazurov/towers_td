﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_ShakeOnDamage : MonoBehaviour {
    [SerializeField] private float _shakeTime;
    [SerializeField] private float _shakeAmplitude;
    [SerializeField] private float _shakeFrequency;

    private Transform   _trans;

    private Coroutine   _shakeRoutine;

    private Vector3     _shakeOrigin;

    void Awake() {
        _trans = transform;
        GameCore.System.Events.mSubscribe<GameEvents.Battle.Game.BaseDamagged>(OnBaseDamaged, true);
    }

    private void            OnBaseDamaged   (GameEvents.Battle.Game.BaseDamagged obj) {
        if (_shakeRoutine != null)
            _trans.eulerAngles = _shakeOrigin;

        _shakeRoutine.End(ref _shakeRoutine, this);
        _shakeRoutine = StartCoroutine(CameraShake());
    }

    private IEnumerator     CameraShake     () {
        var time = 0f;

        var rx = Random.Range(100f, 5000f);
        var ry = Random.Range(100f, 5000f);
        var rz = Random.Range(100f, 5000f);

        _shakeOrigin = _trans.eulerAngles;
        while (time < _shakeTime) {
            yield return null;
            time += Time.deltaTime;
            var gTime       = Time.time;

            var progress    = 1f - (0.5f - time / _shakeTime).Abs() * 2f;
            var eased       = Ease.Do(Mathf.Clamp01(progress), EaseType.easeInOut_x2);

            var offset_x    = Mathf.PerlinNoise(gTime + _shakeFrequency * eased + rx, gTime + _shakeFrequency * eased) - 0.5f;
            var offset_y    = Mathf.PerlinNoise(gTime + _shakeFrequency * eased + ry, gTime + _shakeFrequency * eased) - 0.5f;
            var offset_z    = Mathf.PerlinNoise(gTime + _shakeFrequency * eased + rz, gTime + _shakeFrequency * eased) - 0.5f;

            _trans.eulerAngles = _shakeOrigin + new Vector3(offset_x, offset_y, offset_z * 2f) * _shakeAmplitude;
        }

        _trans.eulerAngles = _shakeOrigin;
        _shakeRoutine = null;
    }



}
