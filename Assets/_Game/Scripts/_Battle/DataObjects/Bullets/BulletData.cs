﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DefaultBulletData", menuName = "GameData/Battle/BulletData")]
public class BulletData : ScriptableObject {
    [SerializeField] private Constants.Bullets.BType        BulletType;
    [SerializeField] private GameObject                     BulletView;

    public Constants.Bullets.BType  bullet_Type    => BulletType;
    public GameObject               bullet_View    => BulletView;

}
