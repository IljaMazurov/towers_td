﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABattleMap : ScriptableObject {
    [SerializeField, HideInInspector] protected int                 _Size       = 5;
    [SerializeField, HideInInspector] protected List<AMapCell>      _Cells       = new List<AMapCell>();

    public int                  map_Size        => _Size;
    public List<AMapCell>       map_Cells       => _Cells;
}
