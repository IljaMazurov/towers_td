﻿using Constants.Map;
using UnityEngine;

[System.Serializable]
public class AMapCell {
    [SerializeField, HideInInspector] protected Vector3     _WorldPosition  = Vector3.zero;
    [SerializeField, HideInInspector] protected CellType    _CellType       = CellType.Ground;
    [SerializeField, HideInInspector] protected int[]       _Neighbors;

    public CellType     cell_CellType       => _CellType;
    public int[]        cell_Neighbors      => _Neighbors;
    public Vector3      cell_WorldPosition  => _WorldPosition;

    public override string ToString() => $"{_CellType} - {_WorldPosition.x}x{_WorldPosition.z}";
}
