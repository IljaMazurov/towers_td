﻿using Unity.Entities;
using Unity.Mathematics;

public struct Blob_MapFloor  {
    public BlobArray<FloorData> floor;
}

public struct FloorData {
    public float3   position;
    public bool     isGround;

    public int      towerLevel;
    public Entity   towerReference;
}
