﻿using Unity.Entities;
using Unity.Mathematics;

public struct Block_SpawnComponent : IComponentData {
    public float    fTotalTime;
    public float    fCurrentTime;

    public float    fStartHeight;
    public float    fDefaultHeight;
}
