﻿using Unity.Entities;
using Unity.Mathematics;

public struct Block_SpawnerDelayComponent : IComponentData {
    public float    fDelayTimer;
}
