﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct Bullet_DamageConponent : IComponentData {
    public Entity   entityTarget;
    public float    fDamage;
    public float    fDamageDelay;
}
