﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct Common_RotationComponent : IComponentData {
    public float3 f3CurrentAngle;
    public float3 f3AngleSpeed;
}
