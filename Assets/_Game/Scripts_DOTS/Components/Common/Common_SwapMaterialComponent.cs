﻿using Constants.Materials;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Common_SwapMaterialComponent : IComponentData {
    public MaterialTypeIndex eMaterialType;
}
