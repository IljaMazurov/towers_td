﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Enemy_CommonDataComponent : IComponentData {
    [HideInInspector] public Entity entityHealthBar;
}
