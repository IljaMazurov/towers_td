﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Enemy_CommonInfoComponent : IComponentData {
    public Constants.Enemies.EType  type;
}
