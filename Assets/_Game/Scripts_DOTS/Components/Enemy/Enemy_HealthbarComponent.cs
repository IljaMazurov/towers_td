﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Enemy_HealthbarComponent : IComponentData {
    [HideInInspector] public Entity entityOwner;
}
