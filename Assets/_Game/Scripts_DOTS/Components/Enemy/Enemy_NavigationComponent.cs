﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct Enemy_NavigationComponent : IComponentData {
    [HideInInspector]public BlobAssetReference<Blob_EnemyNavigation>     nav_PathData;

    [HideInInspector]public int          iStepIndex;
    [HideInInspector]public int          iTotalSteps;

    [HideInInspector]public float        fTimeToComplete;
    [HideInInspector]public float        fCurrentRunTime;

    [HideInInspector]public float3       f3CellPrevPosition;
    [HideInInspector]public float3       f3CellCurrentPosition;
    [HideInInspector]public float3       f3CellNextPosition;

    [HideInInspector]public float3       f3RandomMoveOffset;
}
