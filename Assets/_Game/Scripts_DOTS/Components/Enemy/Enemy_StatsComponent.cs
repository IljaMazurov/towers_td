﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct Enemy_StatsComponent : IComponentData {
    public float fHealthMax;
    public float fHealthCurrent;
}
