﻿using Unity.Entities;
public struct Enemy_Tag_Destroyed : IComponentData {
    public bool HasFinished;
}
