﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Tower_BaseComponent : IComponentData {
    public int      iLevel;
    public Entity   entityGun;
}
