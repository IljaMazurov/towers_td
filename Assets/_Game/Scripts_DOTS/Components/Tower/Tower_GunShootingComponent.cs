﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Tower_GunShootingComponent : IComponentData {
    public Constants.Bullets.BType      eBulletType;
    [HideInInspector]
    public float                        fReloadTimer;
}
