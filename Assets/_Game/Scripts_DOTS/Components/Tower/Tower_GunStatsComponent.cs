﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct Tower_GunStatsComponent : IComponentData {
    public float    fDamage;
    public float    fTargetSearchRadius;
    public float    fReloadTime;
    public float    fBulletFlightTime;
    // damage type
}
