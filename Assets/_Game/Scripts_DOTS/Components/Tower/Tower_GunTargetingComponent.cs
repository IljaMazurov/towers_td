﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct Tower_GunTargetingComponent : IComponentData {
    [HideInInspector]
    public Entity   entityActiveTarget;
}
