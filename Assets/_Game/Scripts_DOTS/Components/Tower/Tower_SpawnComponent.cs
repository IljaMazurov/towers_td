﻿using Unity.Entities;
using Unity.Mathematics;

public struct Tower_SpawnComponent : IComponentData {
    public float    fTimeTotal;
    public float    fTimeCurrent;
}
