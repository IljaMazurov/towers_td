﻿using Unity.Entities;
using Unity.Mathematics;

public struct MapFloorMergeData {
    public bool     isGround;

    public bool     towerMoved;

    public float3   position;

    public int      towerLevel;

    public Entity   towerCurrent;
    public Entity   towerMergingIn;
}
