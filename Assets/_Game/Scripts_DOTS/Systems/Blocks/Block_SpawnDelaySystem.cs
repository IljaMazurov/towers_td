﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class Block_SpawnDelaySystem : ComponentSystem {
    protected override void OnUpdate() {
        var delta   = Time.DeltaTime;
        Entities.ForEach((Entity ent, ref Block_SpawnerDelayComponent spawnDelayData) => {
            spawnDelayData.fDelayTimer -= delta;
            if (spawnDelayData.fDelayTimer <= 0)
                World.EntityManager.RemoveComponent(ent, typeof(Block_SpawnerDelayComponent));
        });
    }

}
