﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class Block_SpawnSystem : SystemBase {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
    protected override void OnCreate() {
        base.OnCreate();
        m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate() {
        var ecb     = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        var delta   = Time.DeltaTime;

        Entities.
            WithNone<Block_SpawnerDelayComponent>().
            ForEach((Entity ent, int entityInQueryIndex, ref Block_SpawnComponent spawnData, ref Translation position) => {
                spawnData.fCurrentTime += delta;

                var progress    = spawnData.fCurrentTime / spawnData.fTotalTime;
                var eased       = Ease.Do(Ease.Do(progress, EaseType.easeInOut_x2), EaseType.custom_easeInOverSize);

                if (progress > 1) {
                    position.Value.y = spawnData.fDefaultHeight;
                    ecb.RemoveComponent<Block_SpawnComponent>(entityInQueryIndex, ent);
                } else {
                    position.Value.y = math.lerp(spawnData.fStartHeight, spawnData.fDefaultHeight, eased);
                }

            }).ScheduleParallel();

        m_EndSimulationEcbSystem.AddJobHandleForProducer(this.Dependency);
    }

}
