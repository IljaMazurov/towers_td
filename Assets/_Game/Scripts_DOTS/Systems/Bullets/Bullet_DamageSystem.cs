﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class Bullet_DamageSystem : SystemBase {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

    protected override void OnCreate() {
        base.OnCreate();
        m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate() {
        var ecb         = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        var delta       = Time.DeltaTime;

        var enemies     = GetComponentDataFromEntity<Enemy_StatsComponent>(true);
        var destroyed   = GetComponentDataFromEntity<Enemy_Tag_Destroyed>(true);

        Entities.
            WithReadOnly(destroyed).
            WithNativeDisableContainerSafetyRestriction(enemies).
            ForEach((Entity entity, int entityInQueryIndex, ref Bullet_DamageConponent bulletData) => {
                if (bulletData.fDamageDelay > 0) {
                    bulletData.fDamageDelay -= delta;
                    return;
                }

                ecb.DestroyEntity(entityInQueryIndex, entity);

                if (!enemies.HasComponent(bulletData.entityTarget) || 
                    destroyed.HasComponent(bulletData.entityTarget))
                    return;

                var enemyStats  = enemies[bulletData.entityTarget];
                var leftHp      = enemyStats.fHealthCurrent - bulletData.fDamage;

                if (leftHp <= 0) {
                    ecb.AddComponent(0, bulletData.entityTarget, new Enemy_Tag_Destroyed { 
                        HasFinished = false
                    });
                } else {
                    enemyStats.fHealthCurrent       = leftHp;
                    enemies[bulletData.entityTarget]   = enemyStats;
                }

        }).ScheduleParallel();

        m_EndSimulationEcbSystem.AddJobHandleForProducer(this.Dependency);
    }
}
