﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class Common_RotationSystem : JobComponentSystem {
    protected override JobHandle OnUpdate(JobHandle inputDeps) {
        var delta   = Time.DeltaTime;
        var deps    = Entities.ForEach((ref Common_RotationComponent rotatorData, ref Rotation rotation) => {
            rotatorData.f3CurrentAngle += rotatorData.f3AngleSpeed * delta;
            rotation.Value = quaternion.EulerXYZ(rotatorData.f3CurrentAngle);
        }).Schedule(inputDeps);

        return deps;
    }

}
