﻿using Unity.Entities;
using Unity.Rendering;

[UpdateBefore(typeof(BeginSimulationEntityCommandBufferSystem))]
public class Common_SwapMaterialSystem : ComponentSystem {
    protected override void OnUpdate() {
        var _gameCache  = GameCore.Battle.CacheData;

        Entities.ForEach((Entity entity, ref Common_SwapMaterialComponent materialData) => {
            World.EntityManager.RemoveComponent<Common_SwapMaterialComponent>(entity);

            var renderMesh      = World.EntityManager.GetSharedComponentData<RenderMesh>(entity);
            var newMaterial     = _gameCache.mGetMaterial(materialData.eMaterialType);

            renderMesh.material = newMaterial;

            World.EntityManager.SetSharedComponentData(entity, renderMesh);
        });

    }

}
