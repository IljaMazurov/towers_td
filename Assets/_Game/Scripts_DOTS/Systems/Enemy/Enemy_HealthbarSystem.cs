﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameTRSToLocalToWorldSystem))]
public class Enemy_HealthbarSystem : ComponentSystem {

    protected override void OnUpdate() {
        var positions   = GetComponentDataFromEntity<Translation>();
        var entityStats = GetComponentDataFromEntity<Enemy_StatsComponent>(true);

        Entities.
            WithAllReadOnly<Translation>().
            ForEach((Entity entity, ref Enemy_HealthbarComponent healthbarData, ref LocalToWorld matrix) => {
            if (!positions.HasComponent(healthbarData.entityOwner))
                return;

            var ownerStats      = entityStats[healthbarData.entityOwner];
            var ownerPos        = positions[healthbarData.entityOwner];

            matrix.Value.c0.w   = math.clamp(ownerStats.fHealthCurrent / ownerStats.fHealthMax, 0, 1);
            matrix.Value.c3     = new float4(ownerPos.Value, 1);
        });

    }
}
