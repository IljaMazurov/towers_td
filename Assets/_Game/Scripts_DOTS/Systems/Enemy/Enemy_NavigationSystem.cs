﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class Enemy_NavigationSystem : SystemBase {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

    protected override void OnCreate() {
        base.OnCreate();
        m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    private static float3   GetProgressPoint(float3 p1, float3 p2, float3 p3, float t) {
        var nSliceProgress  = 0.25f + t * 0.5f;
        var new_p2          = p2 + ((p2 - p1) + (p2 - p3)) * 0.165f;
        return VectorExtensions.GetBeizerPoint(p1, new_p2, p3, nSliceProgress);
    }

    protected override void OnUpdate() {
        var ecb     = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        var delta   = Time.DeltaTime;

        Entities.WithNone<Enemy_Tag_RunFinish>().ForEach((Entity entity, int entityInQueryIndex, ref Enemy_NavigationComponent entityNavData, ref Translation translation, ref Rotation rotation) => {
            var progress        = entityNavData.fCurrentRunTime / entityNavData.fTimeToComplete;

            var sliceLen        = 1f / entityNavData.iTotalSteps;
            var currentIndex    = (int)math.floor(progress / sliceLen);
            var sliceProgress   = (progress - sliceLen * currentIndex) / sliceLen;

            var endStep         = currentIndex + 1;

            if (entityNavData.iStepIndex != currentIndex && endStep < entityNavData.iTotalSteps) {
                entityNavData.f3CellPrevPosition      = entityNavData.nav_PathData.Value.road[currentIndex];
                entityNavData.f3CellCurrentPosition   = entityNavData.nav_PathData.Value.road[currentIndex + 1];
                entityNavData.f3CellNextPosition      = entityNavData.nav_PathData.Value.road[currentIndex + 2];
                entityNavData.iStepIndex             = currentIndex;


                if (currentIndex == 0)
                    ecb.AddComponent(entityInQueryIndex, entity, new Common_SwapMaterialComponent {
                        eMaterialType = Constants.Materials.MaterialTypeIndex.Enemy_Spawn
                    });

                if (currentIndex == 2)
                    ecb.AddComponent(entityInQueryIndex, entity, new Common_SwapMaterialComponent { 
                        eMaterialType = Constants.Materials.MaterialTypeIndex.Enemy_Default
                    });

                if (currentIndex == entityNavData.iTotalSteps - 4)
                    ecb.AddComponent(entityInQueryIndex, entity, new Common_SwapMaterialComponent { 
                        eMaterialType = Constants.Materials.MaterialTypeIndex.Enemy_Finish
                    });
            }

            if (endStep + 1 >= entityNavData.iTotalSteps) {
                ecb.AddComponent(entityInQueryIndex, entity, new Enemy_Tag_RunFinish());
                return;
            }

            var offset          = math.lerp(new float3(0, .75f, 0), entityNavData.f3RandomMoveOffset, math.clamp(currentIndex + sliceProgress, 0, 1) );
            var newTarget       = GetProgressPoint(entityNavData.f3CellPrevPosition, entityNavData.f3CellCurrentPosition, entityNavData.f3CellNextPosition, sliceProgress);
            var flowOffset      = noise.snoise(new float2(newTarget.x, newTarget.z)) * 0.1f;
            var newPosition     = newTarget + new float3(0, flowOffset, 0) + offset;

            rotation.Value      = quaternion.LookRotation(newPosition - translation.Value, new float3(0, 1, 0));
            translation.Value   = newPosition;

            entityNavData.fCurrentRunTime += delta;
        }).ScheduleParallel();

        m_EndSimulationEcbSystem.AddJobHandleForProducer(this.Dependency);
    }
}
