﻿using Unity.Entities;
using Unity.Transforms;

public class Enemy_OnDestroySystem : ComponentSystem {
    protected override void OnUpdate() {
        var particles = GameCore.ECS.Particles;

        Entities.ForEach((Entity entity, ref Enemy_Tag_Destroyed destroy, ref Enemy_CommonDataComponent common, ref Enemy_CommonInfoComponent info, ref Translation pos) => {

            World.EntityManager.DestroyEntity(entity);
            World.EntityManager.DestroyEntity(common.entityHealthBar);

            if (!destroy.HasFinished) {
                GameCore.System.Events.mTrigger(new GameEvents.Battle.Enemies.Destroyed {
                    enemyType = info.type
                });

                particles.mRegisterEnemyExplosion(info.type, pos.Value);
            }

        });
    }
}
