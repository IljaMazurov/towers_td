﻿using Unity.Entities;

public class Enemy_OnRunFinishTriggersSystem : ComponentSystem {
    protected override void OnUpdate() {
        Entities.
            WithNone(typeof(Enemy_Tag_Destroyed)).
            ForEach((Entity entity, ref Enemy_Tag_RunFinish materialData) => {

                World.EntityManager.AddComponentData(entity, new Enemy_Tag_Destroyed {
                    HasFinished = true
                });

                GameCore.System.Events.mTrigger(new GameEvents.Battle.Game.BaseDamagged());
        });
    }
}
