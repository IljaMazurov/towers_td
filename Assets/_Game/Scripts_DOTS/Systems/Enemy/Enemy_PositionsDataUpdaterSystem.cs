﻿using Unity.Jobs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class Enemy_PositionsDataUpdaterSystem : ComponentSystem {

    protected override void OnUpdate() {
        var delta           = Time.DeltaTime;
        var cachedPositions = GameCore.Battle.CacheData.mGetEnemyPositionsCacheRef();

        Entities.
            WithNone<Enemy_Tag_RunFinish>().
            WithAll<Enemy_Tag_Unit>().
            ForEach((Entity entity, ref Translation positionData) => {
                var id = entity.Index;

                if (!cachedPositions.ContainsKey(entity.Index)) {
                    cachedPositions.Add(entity.Index, new EnemyPositionsCache {
                        enemy_Position      = positionData.Value,
                        enemy_DestroyDelay  = 10f
                    });
                } else {
                    var oldData = cachedPositions[entity.Index];

                    oldData.enemy_Position         = positionData.Value;
                    oldData.enemy_DestroyDelay     = 10f;

                    cachedPositions[entity.Index]         = oldData;
                }
        });

        var keys = cachedPositions.GetKeyArray(Unity.Collections.Allocator.Temp);
        for (int i = 0; i < keys.Length; i++) {
            var index           = keys[i];
            var data            = cachedPositions[index];
            var destroyDelay    = data.enemy_DestroyDelay - delta;

            if (destroyDelay > 0) {
                data.enemy_DestroyDelay -= delta;
                cachedPositions[index] = data;
            } else {
                cachedPositions.Remove(index);
            }
        }
        keys.Dispose();

    }

}
