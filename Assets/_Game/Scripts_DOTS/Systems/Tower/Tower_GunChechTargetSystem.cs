﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;

public class Tower_GunChechTargetSystem : SystemBase {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
    protected override void OnCreate() {
        base.OnCreate();

        m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate() {
        var ecb             = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        var translations    = GetComponentDataFromEntity<Translation>(true);

        Entities.
        WithNone<Tower_Tag_GunSearchTarget>().
        ForEach((Entity entity, int entityInQueryIndex, ref Tower_GunTargetingComponent target, ref Tower_GunStatsComponent stats,  ref Rotation rotation) => {

            if (!translations.HasComponent(target.entityActiveTarget)) {
                target.entityActiveTarget = Entity.Null;
                ecb.AddComponent(entityInQueryIndex, entity, new Tower_Tag_GunSearchTarget());
                return;
            }

            if (target.entityActiveTarget == Entity.Null) {
                ecb.AddComponent(entityInQueryIndex, entity, new Tower_Tag_GunSearchTarget());
                return;
            }

            if (translations.HasComponent(target.entityActiveTarget) &&
                translations.HasComponent(entity)) {
                var entityPosition  = translations[entity].Value;
                var targetPosition  = translations[target.entityActiveTarget].Value;
                var distance        = math.distance(entityPosition, targetPosition);

                if (stats.fTargetSearchRadius < distance) {
                    target.entityActiveTarget = Entity.Null;
                    ecb.AddComponent(entityInQueryIndex, entity, new Tower_Tag_GunSearchTarget());
                    return;
                }

                var direction       = math.normalize(targetPosition - entityPosition);
                var angle           = quaternion.LookRotation(direction, math.up());
                rotation.Value      = angle;
            }
            
        }).WithReadOnly(translations).ScheduleParallel();

        m_EndSimulationEcbSystem.AddJobHandleForProducer(this.Dependency);
    }

}
