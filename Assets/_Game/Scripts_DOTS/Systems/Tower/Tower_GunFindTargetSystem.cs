﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;

public class Tower_GunFindTargetSystem : JobComponentSystem {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
    protected override void OnCreate() {
        base.OnCreate();
        m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    protected override JobHandle OnUpdate(JobHandle inputDeps) {
        var ecb         = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        var query       = World.EntityManager.CreateEntityQuery(    ComponentType.ReadOnly<Enemy_Tag_Unit>(), 
                                                                    ComponentType.ReadOnly<Enemy_NavigationComponent>());
        
        var translations    = GetComponentDataFromEntity<Translation>(true);
        var entitiesData    = query.ToEntityArray(Allocator.TempJob);
        var navigationData  = query.ToComponentDataArray<Enemy_NavigationComponent>(Allocator.TempJob);

        var dep     = Entities.
            WithAll<Tower_Tag_GunSearchTarget>().
            ForEach((Entity entity, int entityInQueryIndex, ref Tower_GunTargetingComponent target, ref Tower_GunStatsComponent stats) => {

                float maxDist       = 0;
                Entity lastTarget   = Entity.Null;

                var myPosition      = translations[entity].Value;

                for (int i = 0; i < entitiesData.Length; i++) {
                    var eData           = entitiesData[i];
                    var nData           = navigationData[i];
                    var enemyPosition   = translations[eData].Value;

                    var distance = math.distance(myPosition, enemyPosition);
                    if (distance > stats.fTargetSearchRadius)
                        continue;

                    var pathCompletion = nData.fCurrentRunTime / nData.fTimeToComplete;
                    if (pathCompletion > maxDist) {
                        maxDist     = pathCompletion;
                        lastTarget  = eData;
                    }
                }

                if (lastTarget != Entity.Null) {
                    target.entityActiveTarget = lastTarget;
                    ecb.RemoveComponent<Tower_Tag_GunSearchTarget>(entityInQueryIndex, entity);
                }

        }). WithReadOnly(translations).
            WithReadOnly(entitiesData).
            WithReadOnly(navigationData).
            Schedule(inputDeps);

        dep.Complete();

        navigationData.Dispose();
        entitiesData.Dispose();

        return dep;
    }


}
