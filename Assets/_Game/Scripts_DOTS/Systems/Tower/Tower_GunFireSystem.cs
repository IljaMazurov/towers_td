﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;

public class Tower_GunFireSystem : ComponentSystem {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

    protected override void OnUpdate() {
        var delta       = Time.DeltaTime;
        var particles   = GameCore.ECS.Particles;
        var positions   = GetComponentDataFromEntity<Translation>(true);

        var buffered    = new EntityCommandBuffer(Allocator.Temp);

        Entities.
            WithNone<Tower_Tag_GunSearchTarget>().
            ForEach((Entity entity, ref Tower_GunTargetingComponent target, ref Tower_GunStatsComponent stats, ref Tower_GunShootingComponent shoot) => {
                var enemy               = target.entityActiveTarget;
                var reloadTimer         = shoot.fReloadTimer + delta;
                shoot.fReloadTimer = reloadTimer;

                if (enemy == Entity.Null || !positions.HasComponent(enemy))
                    return;

                if (reloadTimer > stats.fReloadTime) {
                    var bulletDamageData = new Bullet_DamageConponent {
                        fDamage = stats.fDamage,
                        fDamageDelay = stats.fBulletFlightTime,
                        entityTarget = target.entityActiveTarget,
                    };
                    var bullet = buffered.CreateEntity();
                    buffered.AddComponent(bullet, bulletDamageData);


                    var targetPos   = positions[enemy];
                    var unitPos     = positions[entity];

                    particles.mRegisterBullet(shoot.eBulletType, 
                                                target.entityActiveTarget.Index,
                                                unitPos.Value,
                                                math.normalize(targetPos.Value - unitPos.Value),
                                                stats.fBulletFlightTime);

                    shoot.fReloadTimer = 0;
                }

        });

        buffered.Playback(World.EntityManager);
        buffered.Dispose();

    }


}
