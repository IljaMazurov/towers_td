﻿using Unity.Entities;
using Unity.Transforms;

public class Tower_SpawnSystem : SystemBase {
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
    protected override void OnCreate() {
        base.OnCreate();

        m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate() {
        var ecb     = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        var delta   = Time.DeltaTime;

        Entities.ForEach((Entity entity, int entityInQueryIndex, ref Tower_SpawnComponent spawnData, ref LocalToWorld matrix) => {
            var progress = spawnData.fTimeCurrent / spawnData.fTimeTotal;

            if (progress >= 1f) {
                matrix.Value.c0.w = 1f; 
                ecb.RemoveComponent<Tower_SpawnComponent>(entityInQueryIndex, entity);
                ecb.AddComponent(entityInQueryIndex, entity, new Common_SwapMaterialComponent { 
                    eMaterialType = Constants.Materials.MaterialTypeIndex.Tower_Default
                });
            } else {
                matrix.Value.c0.w = progress;
            }

            spawnData.fTimeCurrent += delta;
        }).ScheduleParallel();

        m_EndSimulationEcbSystem.AddJobHandleForProducer(this.Dependency);
    }

}
