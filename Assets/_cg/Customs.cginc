﻿
inline float4x4 ParseMatrix(float4x4 in_objectToWorld, out float4 data){
	data = float4(
			in_objectToWorld._41, 
			in_objectToWorld._42, 
			in_objectToWorld._43, 
			in_objectToWorld._44
			);
	in_objectToWorld._41 = 0;
	in_objectToWorld._42 = 0;
	in_objectToWorld._43 = 0;
	in_objectToWorld._44 = 1;

	return in_objectToWorld;
}

inline float4x4 ParseMatrix(float4x4 in_objectToWorld){
	in_objectToWorld._41 = 0;
	in_objectToWorld._42 = 0;
	in_objectToWorld._43 = 0;
	in_objectToWorld._44 = 1;
	return in_objectToWorld;
}